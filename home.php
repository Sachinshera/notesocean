<?php
require_once("includes/db.php");
include("includes/ago_time.php");
include("includes/clean.php");
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Find The Perfect Notes For You</title>

    <meta name="description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money">
    <meta name="keyword" content="handwritten notes to text,highway engineering notes,made easy handwritten notes,irrigation engineering notes,business management notes,lacture notes,handwritten notes,pdf notes,engineering notes,management notes,notesocean, notes ocean,notes ocean bba notes">
    <meta name="robots" content="index,follow">
    <!-- og start  -->
    <meta property="og:site_name" content="Notes Ocean" />
    <meta property="og:title" content="Find The Perfect Notes For You" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://notesocean.com" />
    <meta name="og:description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <!-- og end  -->
    <!-- ads script  -->
    <?php echo $ads_script; ?>

    <?php include("includes/cdn.php"); ?>
    <!-- homepage styles start -->
    <style type="text/css">
        .mt-100 {
            margin-top: 100px;
        }

        .mb-100 {
            margin-bottom: 100px;
        }

        .banner-search {
            background-image: url('assest/img/notebook.svg');
            height: 70vh;
            background-repeat: no-repeat;
            background-size: cover;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        form.example input[type=text] {
            padding: 10px;
            font-size: 17px;
            border: 1px solid grey;
            border-radius: 10px 0px 0px 10px;
            float: left;
            width: 90%;
            background: #ffff;
            border-right: none;
            text-align: center;
        }

        form.example input[type=text]:focus {
            outline: none;
        }

        form.example button {
            float: left;
            width: 10%;
            padding: 10px;
            background: #ffff;
            color: red;
            font-size: 17px;
            border: 1px solid grey;

            border-radius: 0px 10px 10px 0;
            border-left: none;
            cursor: pointer;
        }

        form.example button:hover {
            background: red;
            color: #ffff;
        }

        form.example::after {
            content: "";
            clear: both;
            display: table;
        }

        .notes-section {
            margin-top: -130px;

        }

        .notes {

            border-radius: 10px;
            /* box-shadow: 0 0 2px 0.5px #ccc; */
            border: 1px solid #ccc;
            background-color: #fff;
            width: 80%;
            margin-left: 10%;
            height: 100%;
            margin-bottom: 20px !important;
        }

        .notes h3,
        h2,
        h4,
        h5 {
            text-align: center;
            margin-bottom: 20px;

        }

        .notes ul {
            list-style: none;
            text-align: left;
        }

        .notes ul li {
            font-size: 12 px;
            line-height: 35px;

            padding: 2px 5px;

        }

        .notes ul li a {
            text-decoration: none;
            color: #323232;

        }

        .btn-danger {
            border-radius: 25px;
            padding: 10px 30px;
        }

        .headings {
            text-align: center;
            color: whitesmoke;

        }

        .headings h1 {
            font-weight: bold;
            padding-top: 25px;
            color: white;
        }

        .public {
            color: red;
        }

        .headings {
            width: 50% !important;
        }

        @media (max-width:768px) {
            .banner-search {
                padding: 5%;
                background-size: cover;
                min-height: 50vh !important;
                margin-top: 18%;
                /* display: inline-block */
                display: block;
                justify-content: top;
                align-items: top;

            }

            .headings h2 {
                font-size: 27px;
                text-align: center;
            }

            .headings p {
                font-size: 13px;
            }

            .headings {
                width: 100% !important;
            }

            .notes {
                /* padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 2px 0.5px #ccc;
            background-color: #fff; */
                width: 100%;
                margin-left: 0%;

            }

            form.example button {

                padding: 10px 0px;
            }

        }
    </style>

    <!-- homepage styles end -->

</head>

<body>
    <?php include("includes/header.php") ?>
    <!-- <section class="latest_event jumbotron bg-white p-0">
        <a href="<?php echo $home_page; ?>/account"> <img src="assest/img/events/start.jpg" alt="notesocean-latest-event"></a>
    </section> -->

    <section class="banner-search">
        <div class="headings text-center ">
            <h1>Find the Perfect <span class="public">Notes</span></h1>
            <p>Notesocean is the best destination for students<br> to find perfect notes for every subject & course.</p>
            <br>

            <form class="example" action="<?php echo $home_page; ?>/search2">
                <input type="text" placeholder="search your notes" name="q">
                <button type="submit"><i class="fa fa-search"></i></button>

            </form>
            <br><br>
            <br>

        </div>

    </section>
    <section class="notes-section mb-100">
        <div class="container col-12">
            <div class="row">
                <?php include("keyword.php"); ?>
            </div>

        </div>

    </section>

    <!-- search secton end  -->




    <?php
    include("includes/footer.php");
    ?>

    <script>
        $(document).ready(() => {
            let url = location.href.replace(/\/$/, "");

            if (location.hash) {
                const hash = url.split("#");
                $('.nav-item a[href="#' + hash[1] + '"]').tab("show");
                url = location.href.replace(/\/#/, "#");
                history.replaceState(null, null, url);
                setTimeout(() => {
                    $(window).scrollTop(0);
                }, 400);
            }

            $('a[data-toggle="tab"]').on("click", function() {
                let newUrl;
                const hash = $(this).attr("href");
                if (hash == "#all") {
                    newUrl = url.split("#")[0];
                } else {
                    newUrl = url.split("#")[0] + hash;
                }
                newUrl += "/";
                history.replaceState(null, null, newUrl);
            });
        });
    </script>
</body>

</html>