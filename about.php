<?php require_once("includes/db.php"); ?>
<!DOCTYPE html>
<html lang=" en-US" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="utf-8">
    <title>About us - Notesocean</title>
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <meta name="keyword" content="basic computer notes for students,ms word notes pdf,note to pdf,lecture notes in,,mechanical engineering,engineering mechanics notes,lecture notes in electrical engineering,total quality management notes,basic electrical engineering notes,handwritten notes to text,highway engineering notes,made easy handwritten notes,irrigation engineering notes,business management notes,lacture notes,handwritten notes,pdf notes,engineering notes,management notes,notesocean, notes ocean,notes ocean bba notes">
    <meta name="robots" content="index,follow">
    <!-- og start  -->
    <meta property="og:site_name" content="Notes Ocean" />
    <meta property="og:title" content="About us - Notes Ocean " />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://notesocean.com/about-us" />
    <meta name="og:description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- meta end  -->
    <?php include("includes/cdn.php"); ?>

    <!-- styles start here -->
    <style type="text/css">
        .mt-100{
            margin-top: 100px;
        }
        .mb-100{
            margin-bottom: 100px;
        }
        .banner_main{
            height: 50vh;
            background-image:linear-gradient(rgba(0,0,0,0.5),
                rgba(0,0,0,0.5)), url('assest/img/about.jpg');
            background-size: cover;
            display:flex;
            justify-content: center;
            align-items: center;

        }
        .banner_child h1{
            color: #fff;
            font-weight: 900;
            font-size: 4rem;
        }
        .about h3{
            font-size: 1.2rem;
            color: #FF0606;
            font-weight: bold;
        }
        .about h2{
            font-size: 4rem;
            font-weight: 900;
        }
        .about_h2{
            color: #E0E0E0;
        }
        .about hr{
            width: 150px;
            height: 10px;

        }
        .about_section h2{
            font-size: 2rem;
        }
        .about_section{
            font-size: 1.5rem;
        }
        .about_section ul{
            list-style-type: none;
        }
        .fa-hand-o-right{
            color: #FF0606;
        }
        .service_section h3{
            font-size: 1.2rem;
            color: #FF0606;
            font-weight: bold;
        }
        .service_section h2{
            font-size: 4rem;
            font-weight: 900;
        }
        .service_section hr{
            width: 150px;
            height: 10px;

        }
        .card img{
            height: 300px;
        }
        .earn{
            padding-bottom: 25px;
        }
        .team_section h2{
            font-size: 3rem;
            font-weight: 900;
        }
        .team_section p{
            font-size: 1.4rem;
        }
        .team h5{
            margin-top: 20px;
            font-size: 1.5rem;
        }
        .team h6{
            font-size: 1.1rem;
        }
        .team{
            border-radius: 10px;
            box-shadow: 0 0 3px rgba(0,0,0,0.3);
            padding: 20px 10px;
        }
        .fa-facebook{
            color: red;
        }

        .social_ul {
        display: table;
        margin: 15px auto 0 auto;
        list-style-type: none;
         }

        .social_ul li {
        padding-left: 20px;
        padding-top: 10px;
        float: left;
        }

        .social_ul li a {
        color: #CCC;
        border: 1px solid #CCC;
        padding: 8px;
        border-radius: 50%;
        }

        .social_ul li i {
        width: 20px;
        height: 20px;
        text-align: center;
        }
        @media (max-width: 767px){
            .about_section p, ul li{
            font-size: 1.2rem;
        }
        }
       .social_icons{
        font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;padding: 0 5px;
       }
       .social_icons_image{
        display: inline-block;
        height: 30px;
        width: 30px;
       }

    </style>
</head>

<body>
    <!-- Header -->

    <?php include("includes/header.php") ?>
   <!-- header end  -->

   <!-- content start here -->

   <div class="banner_main">
        <div class="banner_child">
            <h1> About Us</h1>
        </div>
   </div>

    <div class="about mt-5">
        <div class="text-center">
            <h3>A LITTLE BIT MORE</h3>
            <h2><span class="about_h2"> About</span> Notesocean</h2>
            <hr>
        </div>
    </div>


    <div class="container mt-5 mb-5">
          <div class="row">
            <div class="col-md-5">
                <img src="assest/img/about-vector.png" height="400" width="100%">
            </div>
            <div class="col-md-7 about_section">
                <h2>Our Mission</h2>
                <p>Our mission is to support students mentally an dfinancially. Since education is the light of life, we want everyone to learn and earn and build their career</p>
               
                <ul>
                    <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> To organize the world's information and make in universally accessible and useful.</li>
                    <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> To make education easier.</li>
                    <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> To support studens financially to get their education.</li>
                    <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> To provide students finacially to get higher educatoiin to provide unlimited data storage to save their documents.</li>
              

                </ul>
            </div>
              
          </div>  
    </div>
       
   <div class="service_section mt-100">
        <div class="text-center">
            <h3>WHAT WE SERVE</h3>
            <h2>Services We Provide</h2>
            <hr>  
        </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 mb-5">
                <div class="service_item">
                    <div class="card">
                        <img class="card-img-top" src="assest/img/note.png" alt="SUBJECT WISE NOTES" he>
                        <div class="card-body">
                            <h5 class="card-title">SUBJECT WISE NOTES</h5>
                            <p class="card-text">Subject and course wise perfect Notes, Questions, Casse Studies, Syllabus, etc.</p>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-md-4 mb-5">
                <div class="service_item">
                    <div class="card">
                        <img class="card-img-top" src="assest/img/read.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">READ & DOWNLOAD</h5>
                            <p class="card-text">Online visible and downloading option to view the notes offline</p>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-md-4 mb-5">
                <div class="service_item">
                    <div class="card">
                        <img class="card-img-top" src="assest/img/storage.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">UNLIMITED STORAGE</h5>
                            <p class="card-text">Unlimited data storage and backup facilities to store their documentss.</p>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>

        <div class="row">
            <div class="col-md-4 mb-5">
                <div class="service_item">
                    <div class="card">
                        <img class="card-img-top" src="assest/img/earn.png" alt="EARN MONEY">
                        <div class="card-body">
                            <h5 class="card-title">EARN MONEY</h5>
                            <p class="card-text earn">Sharing notes with Notesacean and earn money.</p>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-md-4 mb-5">
                <div class="service_item">
                    <div class="card">
                        <img class="card-img-top" src="assest/img/demand.jpg" alt="NOTES ON DEMAND">
                        <div class="card-body">
                            <h5 class="card-title">NOTES ON DEMAND</h5>
                            <p class="card-text">As us what you want. Your demand notes will be available to you within 24 hours of your question asked.</p>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="col-md-4 mb-5">
                <div class="service_item">
                    <div class="card">
                        <img class="card-img-top" src="assest/img/sub.png" alt="DOCUMENT">
                        <div class="card-body">
                            <h5 class="card-title">DOCUMENT</h5>
                            <p class="card-text earn">Notesocean provides notes on every subject.</p>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>


    </div>
   </div>

   <div class="vision">
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-md-6 about_section mt-100">
                    <h2>Our Vision</h2>
                    <p>There's always more information out there. Our vision is to empower students around the world to grouw with knowledge and build their career with our suppport.</p>
                </div>
                <div class="col-md-6">
                    <img src="assest/img/vision.png" height="350" width="100%">
                </div>
                  
              </div>  
        </div>
       
   </div>

   <div class="team_section mt-5">
        
        <div class="container">
            <div class="text-center mt-2 mb-5">
            <h2>MEET OUR TEAM</h2>
            <p class="mt-2">We are all very different. We were born in different cities, at different times, we love different music, food, movies. But we have something that unites us all. It is our Goal. We are its heart. We are not just a team, we are a family.</p>
        </div>
            <div class="row mb-3">
            <div class="col-md-4 mt-4">
                    <div class="team text-center">
                        <img src="assest/img/team/female.png" alt="Aakash Sharma" class=" rounded-circle" height="230" width="220" />
                        <h5> Bindiya Kushwaha </h5>
                        <h6> Management </h6>
                        <!-- <p>You can relay on our amazing features list and also our customer services will be great experience.</p> -->
                        
                            <div class="social mt-1">
                            <table width="20" align="center" border="0" cellpadding="0" cellspacing="5">
                                <tr>
                                                                          
                                    <td class="social_icons">
                                        <a href="#">
                                        <img src="assest/img/social/linkdin.png" alt="Linkdin" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="">
                                        <img src="assest/img/social/github.png" alt="Github"border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="#">
                                        <img src="assest/img/social/facebook.png" alt="Facebook" border="0" class="social_icons_image" />
                                        
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="">
                                        <img src="assest/img/social/instagram.png" alt="Instagram" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                     
                                </tr>
                            </table>
                        </div>
                             
                         
                    </div>
                    <div class="col-md-2 mt-4"></div>
                </div>
                <div class="col-md-4 mt-4">
                    <div class="team text-center">
                        <img src="assest/img/team/male.jpg" alt="Team_1" class=" rounded-circle" height="230" width="220" />
                        <h5> Sachin Kumar </h5>
                        <h6> Back-end Developer </h6>
                        <!-- <p>You can relay on our amazing features list and also our customer services will be great experience.</p> -->
                        

                        <div class="social mt-1">
                            <table width="20" align="center" border="0" cellpadding="0" cellspacing="5">
                                <tr>
                                                                          
                                    <td class="social_icons">
                                        <a href="https://in.linkedin.com/in/sachinsheraa">
                                        <img src="assest/img/social/linkdin.png" alt="Linkdin" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="#">
                                        <img src="assest/img/social/github.png" alt="Github"border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="#">
                                        <img src="assest/img/social/facebook.png" alt="Facebook" border="0" class="social_icons_image" />
                                        
                                        </a>
                                    </td> 
                                    <td class="social_icons">
                                        <a href="#">
                                        <img src="assest/img/social/instagram.png" alt="Instagram" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    
                                </tr>
                            </table>
                        </div>


                             
                        
                        
                    </div>
                    
                </div>
                
                 <div class="col-md-4 mt-4">
                    <div class="team text-center">
                        <img src="assest/img/team/akash.jpg" alt="Aakash Sharma" class=" rounded-circle" height="230" width="220" />
                        <h5> Aakash Sharma </h5>
                        <h6> Android Developer </h6>
                        <!-- <p>You can relay on our amazing features list and also our customer services will be great experience.</p> -->
                        
                            <div class="social mt-1">
                            <table width="20" align="center" border="0" cellpadding="0" cellspacing="5">
                                <tr>
                                                                          
                                    <td class="social_icons">
                                        <a href="https://www.linkedin.com/in/aakashvats2910/">
                                        <img src="assest/img/social/linkdin.png" alt="Linkdin" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="https://github.com/aakashvats2910">
                                        <img src="assest/img/social/github.png" alt="Github"border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="#">
                                        <img src="assest/img/social/facebook.png" alt="Facebook" border="0" class="social_icons_image" />
                                        
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="https://www.instagram.com/aakashvats2910/">
                                        <img src="assest/img/social/instagram.png" alt="Instagram" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                     
                                </tr>
                            </table>
                        </div>
                             
                         
                    </div>
                    
                </div>

                 <div class="col-md-4 mt-4">
                    <div class="team text-center">
                        <img src="assest/img/team/sajib.jpg" alt="Sajib Biswas" class="rounded-circle" height="230"  />
                        <h5> Sajib Biswas </h5>
                        <h6> Front-end Developer </h6>
                        <!-- <p>You can relay on our amazing features list and also our customer services will be great experience.</p> -->
                        <div class="social mt-1">
                            <table width="20" align="center" border="0" cellpadding="0" cellspacing="5">
                                <tr>
                                                                           
                                    <td class="social_icons">
                                        <a href="https://www.linkedin.com/in/sajib-biswas-719036176/">
                                        <img src="assest/img/social/linkdin.png" alt="Linkdin" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="https://github.com/sajib0028">
                                        <img src="assest/img/social/github.png" alt="Github"border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="https://www.facebook.com/sajib00028">
                                        <img src="assest/img/social/facebook.png" alt="Facebook" border="0" class="social_icons_image" />
                                        
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="https://www.instagram.com/sajib0028/">
                                        <img src="assest/img/social/instagram.png" alt="Instagram" border="0" class="social_icons_image" />
                                        </a>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-4 mt-4">
                    <div class="team text-center">
                        <img src="assest/img/team/majeetakushwaha.jpeg" alt="Majeeta Kushwaha" class=" rounded-circle" height="230" width="220" />
                        <h5> Majeeta Kushwaha </h5>
                        <h6> Management </h6>
                        <!-- <p>You can relay on our amazing features list and also our customer services will be great experience.</p> -->
                        

                        <div class="social mt-1">
                            <table width="20" align="center" border="0" cellpadding="0" cellspacing="5">
                                <tr>
                                                                          
                                    <td class="social_icons">
                                        <a href="https://instagram.com/manjeetakushwaha?utm_medium=copy_link">
                                        <img src="assest/img/social/linkdin.png" alt="Linkdin" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="#">
                                        <img src="assest/img/social/github.png" alt="Github"border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    <td class="social_icons">
                                        <a href="https://www.facebook.com/manjeeta.kushwaha">
                                        <img src="assest/img/social/facebook.png" alt="Facebook" border="0" class="social_icons_image" />
                                        
                                        </a>
                                    </td> 
                                    <td class="social_icons">
                                        <a href="#">
                                        <img src="assest/img/social/instagram.png" alt="Instagram" border="0" class="social_icons_image" />
                                        </a>
                                    </td>
                                    
                                </tr>
                            </table>
                        </div>


                             
                        
                        
                    </div>
                    
                </div>
            </div>

            <div class="row mb-100">
                
                
                
                 

               
            </div>
            
        </div>      
   </div>

   <!-- content end  here -->

<!-- footer start here -->
    <?php include("includes/footer.php"); ?>
</body>

</html>