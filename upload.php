<?php
require_once("includes/db.php");
if (isset($_GET["api_key"]) && isset($_GET["login_key"])) {
    $api_key = $_GET["api_key"];
    $login_key = $_GET["login_key"];
    $api_status = validate_api($api_key, $user_db);
    if ($api_status == 1) {
        $user_id = validate_login_key($user_db, $login_key);
        if ($user_id !== 0) {
            session_start();
            $_SESSION["active_upload_android_user_id"] = $user_id;
        } else {
            die("<h1 style='text-align:center'> failed to validete  login key </h1>");
        }
    } else {
        die("<h1 style='text-align:center'> failed to validete  api key </h1>");
    }
} else {
    die("<h1 style='text-align:center'> Invalid request </h1>");
}


function validate_api($api_key, $db)
{
    $check_api_key = $db->query("SELECT status FROM api_user WHERE api_key = '$api_key'");
    if ($check_api_key->num_rows == 1) {
        $api_status = $check_api_key->fetch_assoc();
        $api_status = $api_status["status"];
    } else {
        $api_status = 0;
    }
    return $api_status;
}

function validate_login_key($user_db, $login_key)
{
    $get_login_info = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$login_key' ORDER BY id DESC LIMIT 1");
    if ($get_login_info->num_rows !== 0) {
        $login_info = $get_login_info->fetch_assoc();
        $userid = $login_info["userid"];
    } else {
        $userid = 0;
    }

    return $userid;
};
?>


<!DOCTYPE html>
<html lang=" en-US" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="utf-8">
    <title>Upload Files - Notesocean</title>
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- meta end  -->
    <?php include("includes/cdn.php"); ?>
    <style>
        .dropzone {
            border: 0px solid #ccc !important;
            border-radius: 20px;
            box-shadow: 0px 1px 5px #ccc;
            padding: 10px 0px;
            height: 50vh;
            overflow-y: scroll;

        }

        ::-webkit-scrollbar {
            display: none;
        }

        .dz-success-mark {
            display: none;
        }

        .dz-error-mark {
            display: none;
        }

        .dz-image {
            display: none;
        }

        .dz-file-preview {
            border: 1px solid #ccc !important;
            padding: 10px 0px;
            margin: 5px;
            border-radius: 10px;
        }

        .dz-remove {
            background-color: #EF5350;
            color: white;
            border-radius: 5px;
            padding: 2px 2px;
        }

        .dz-remove:hover {
            text-decoration: none;
            color: white;
        }
    </style>
</head>

<body>


    <!-- content start here -->

    <section>
        <div class="container text-center mt-3">
            <img src="assest/img/notesocean.png" height="45" width="70%">
        </div>
    </section>

    <div class="container text-center">
        <div class="panel-content mt-2">
            <b>
                <h5 class="text-center" style="color:#EF5350">Supported Files <span class="text-dark" style="font-size: 15px;"> <br> pdf txt ppt xls xlsx doc docx pptx gsheet xltx </span> </h5>
            </b>
            <form action="php/upload_android_file.php" class="dropzone mt-5" id="file-upload">
                <div>
                    <div class="dz-message" data-dz-message>
                        <i class="fa fa-cloud-upload-alt" style="text-align: center;font-size:70px;color:#EF5350"></i>
                        <h5>Click to Select your files</h5>
                    </div>
                </div>

            </form>

            <div class="progress mt-2 d-none">
                <div class="progress-bar bg-danger progress-bar-striped" style="width:50%;color:white"></div>
            </div>
            <center>
                <button id="uploadFile" style="background-color: #EF5350;color:white" class=" mt-2 btn text-center mx-auto my-3">Start Upload</button>
            </center>
        </div>

    </div>
    <div class="container text-center">
        <div class="fixed-bottom">
            <p>Secure & Fast Uploads<br> Supported by Notesocean.com</p>
        </div>
        </>


        <!-- content end  here -->

        <script src="<?php echo $home_page ?>/account/dashboard/assets/js/dropzone.min.js"></script>
        <script>
            //Disabling autoDiscover
            Dropzone.autoDiscover = false;
            $(function() {
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone(".dropzone", {
                    autoProcessQueue: false,
                    maxFilesize: 30,
                    addRemoveLinks: true,
                    parallelUploads: 5,
                    maxFiles: 5,
                    uploadMultiple: true,
                    createImageThumbnails: false,
                    acceptedFiles: ".pdf,.txt,.ppt,.xls,.xlsx,.doc,.docx,.pptx,.gsheet,.xltx",

                    success: function(data) {
                        // console.log(data);
                        let status = data.xhr.response;
                        // console.log(data);
                        $("#uploadFile").html("Start Upload");
                        $("#uploadFile").prop("disabled", false);
                        if (status.trim() == "success") {
                            swal("success", "your files are uploaded successfully", "success");
                            myDropzone.removeAllFiles();
                        } else {
                            swal("error", "somthing sent wrong , please try again", "error");
                        }
                    },
                    canceled: function(event) {
                        if (event.status == "canceled") {
                            $("#uploadFile").html("Start Upload");
                            $("#uploadFile").prop("disabled", false);
                            myDropzone.removeAllFiles();
                        }
                    },
                    uploadprogress: function(file) {
                        let filename = file.name;
                        let total = file.upload.total;
                        let progres = file.upload.bytesSent;
                        let percent = Math.floor(total * progres, 2) / 100;
                        // console.log(progres);
                    }
                });
                $('#uploadFile').click(function() {
                    myDropzone.processQueue();
                });
                myDropzone.on("sending", function(file, xhr, formData) {
                    $("#uploadFile").html("Please wait....");
                    $("#uploadFile").prop("disabled", true);
                    console.log(file.upload.progress);
                });
            });
        </script>

</body>

</html>