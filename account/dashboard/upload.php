<?php
require("../check_valid_user.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Upload New File -<?php echo $full_name; ?></title>
  <link rel="icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="16x16">
  <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="48x48">
  <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
  <link rel="shortcut icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
  <?php include("includes/css.php"); ?>
  <link rel="stylesheet" href="<?php echo $home_page ?>//account/dashboard/assets/css/dropzone.min.css">
  <?php echo $ads_script; ?>


</head>

<body class="sidebar-light">
  <!-- Wrapper Start -->
  <div class="wrapper">
    <!-- Navbar Start -->
    <?php include("includes/header.php"); ?>
    <!-- Navbar End -->
    <!-- aside start  -->
    <?php include("includes/aside.php");  ?>
    <!-- aside end  -->

    <!-- Main Container Start -->

    <main class="main--container">
      <!-- Main Content Start -->
      <section class="main--content">
        <div class="panel">
            <!-- tutorail notice -->
            <div class="alert alert-info" role="alert" id="alert"> <strong>Wecome <?php echo $full_name; ?>  </strong> <br> If you don't know how to use this website , we recommend to you  please watch this tutorial <br> <button class="btn btn-sm btn-primary" id="show_tutorial"> Watch </button>  <i class="close fa fa-times" data-dismiss="alert"> </i>  </div>

<!-- tutorail notice -->
          <div class="panel-content">
            <b>
              <h3 class="text-center text-danger">Supported Files - <span class="text-dark"> pdf txt ppt xls xlsx doc docx pptx gsheet xltx </span> </h3>
            </b>
            <form action="php/upload_doc.php" class="dropzone" id="file-upload">
              <div>



              </div>

            </form>
            <center>
              <button id="uploadFile" class="btn btn-primary text-center mx-auto my-3">Start Upload</button>
            </center>
          </div>
        </div>
      </section>
      <!-- Main Content End -->
    </main>

    <!-- Main Container End -->
  </div>
  <!-- Wrapper End -->


  <?php include("includes/js.php"); ?>
  <script src="<?php echo $home_page ?>/account/dashboard/assets/js/dropzone.min.js"></script>
  <script>
    //Disabling autoDiscover
    Dropzone.autoDiscover = false;
    $(function() {
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone(".dropzone", {
        autoProcessQueue: false,
        maxFilesize: 30,
        addRemoveLinks: true,
        parallelUploads: 5,
        maxFiles: 5,
        uploadMultiple: true,
        acceptedFiles: ".pdf,.txt,.ppt,.xls,.xlsx,.doc,.docx,.pptx,.gsheet,.xltx",

        success: function(data) {
          let status = data.status;
          $("#uploadFile").html("Start Upload");
          $("#uploadFile").prop("disabled", false);
          if (status == "success") {
            swal("success", "your files are uploaded successfully", "success");
          } else {
            swal("error", "somthing sent wrong , please try again", "success");
          }
          myDropzone.removeAllFiles();

        }
      });
      $('#uploadFile').click(function() {
        myDropzone.processQueue();
      });
      myDropzone.on("sending", function(file, xhr, formData) {
        $("#uploadFile").html("Please wait....");
        $("#uploadFile").prop("disabled", true);

      });
    });
  </script>
</body>

</html>