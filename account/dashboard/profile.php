<?php 
require("../check_valid_user.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Profile - <?php echo $full_name; ?></title>
    <link rel="icon" href="<?php  echo $home_page;?>/assest/img/title_icon.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="<?php  echo $home_page;?>/assest/img/title_icon.png" sizes="48x48">
    <link rel="apple-touch-icon-precomposed" href="<?php  echo $home_page;?>/assest/img/title_icon.png">
    <link rel="shortcut icon" href="<?php  echo $home_page;?>/assest/img/title_icon.png">
    <?php   include("includes/css.php"); ?>
    <?php echo $ads_script; ?>
    <style>
.profile-info h4{
    color:red;
    font-weight:bold;
    font-size:15px;
}
.profile-info span{
    color:black;
    font-size:15px;
}

    </style>
</head>
<body class="sidebar-light">
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php  include("includes/header.php"); ?>
        <!-- Navbar End -->

        <!-- Sidebar Start -->
        <?php  include("includes/aside.php");  ?>
        <!-- Sidebar End -->

        <!-- Main Container Start -->
        <main class="main--container">
            <!-- Main Content Start -->
            <section class="main--content">
                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card bg-white border-0" style="box-shadow:0px 0px 8px -1px #ccc;">
                                    <div class="card-content bg-white border-0">
                                    <img class="card-img-top" src=" <?php echo $picture; ?>">
                                        <div class="card-footer bg-white">
                                            <!-- <button class="btn btn-danger btn-sm float-right">Change picture</button> -->
                                        </div>
                                      
                                        <div class="card-footer bg-white border-0">
                                       <center>
                                       <button class="btn btn-dark btn-sm border-0 text-center">Basic information</button>
                                       </center>
                                        
                                        <div class="profile-info mt-4 text-center">
                                        <h4 > Full Name : <span id="fullname_basic_info">  <?php echo $full_name; ?>  </span> </h4>
                                        <h4> Email : <span>  <?php echo $email; ?>  </span> </h4>
                                        <h4> Joining Date : <span>  <?php echo $registerdDate; ?>  </span> </h4>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card bg-white" style="box-shadow:0px 0px 8px -1px #ccc;">
                                    <div class="card-content bg-white">
                                        <div class="card-body bg-white">
                                        <button class=" btn btn-dark btn-sm border-0 ">All  information</button> 
                                        <form class="user-form mt-2" method="post" action="profile.php">
                                        <div class="form-group my-2">
                                                <label for="fullname">Fullname</label>
                                                <input type="text" class="form-control" autocomplete="off" name="fullname" maxlength="50" value="<?php echo $full_name;?>">
                                            </div>
                                            <div class="form-group my-2">
                                                <label for="Mobile">Mobile</label>
                                                <input type="tel" class="form-control" autocomplete="off" name="Mobile" maxlength="15" value="<?php echo $mobile;?>">
                                            </div>

                                            <div class="form-group my-2">
                                                <label for="country">country</label>
                                                <input type="text" class="form-control" autocomplete="off" name="country" maxlength="25" value="<?php echo $country;?>">
                                            </div>

                                            <div class="form-group my-2">
                                                <label for="gender">Gender</label>
                                                <select name="Gender" id="Gender" class="form-control">
                                                <option value="Male" <?php if($gender=="Male"){echo "selected";}; ?>>Male</option>
                                                <option value="Female"  <?php if($gender=="Female"){echo "selected";}; ?>>Female</option>
                                                <option value="Other" <?php if($gender=="Other"){echo "selected";}; ?>>Other</option>
                                                </select>
                                            </div>

                                            <div class="form-group my-2">
                                                <label for="Address">Address</label>
                                                <input type="text" class="form-control" autocomplete="off" name="Address" maxlength="200" value="<?php echo $address;?>">
                                            </div>

                                            <div class="form-group my-2">
                                                <label for="Account Status">Account Status</label>
                                                <input type="text" class="form-control" autocomplete="off" name="Account Status" maxlength="200" value="<?php echo $status;?>" disabled >
                                            </div>

                                            <div class="form-group my-2">
                                                <label for="userid">User id</label>
                                                <input type="text" class="form-control" autocomplete="off" name="Account Status" maxlength="200" value="<?php echo $user_id;?>" disabled>
                                            </div>
                                            <div class="update_notice"></div>
                                            <button class="btn btn-primary" type="submit" name="update" id="update_btn">Save</button>
                                        </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Main Content End -->
        </main>
        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->


    <?php   include("includes/js.php"); ?>
    <script>
    $(document).ready(function(){
        $("form").submit(function(event){
            event.preventDefault();
            $.ajax({
                type:"POST",
                url:"php/update_info.php",
                data:new FormData(this),
                processData:false,
                contentType:false,
                beforeSend:function(){
                    $("#update_btn").html("Please wait...");
                },
                success:function(response){
                    // alert(Response);
                    $("#update_btn").html("Save");
                    if(response=="success"){
                        $(".update_notice").html("<div class='alert alert-success'> Your profile data is successfully updated <i class='fa fa-times close' data-dismiss='alert'> </i> </div>");
                        // put full in basic info

                    $("#fullname_basic_info").html($("input[name='fullname']").val());
                    }
                    else if(response=="failed"){
                        $(".update_notice").html("<div class='alert alert-danger'> opps! Data was not saved <i class='fa fa-times close' data-dismiss='alert'> </i> </div>");
                    }
                    else if(response=="logout"){
                        window.location = "../logout";
                    }
                    else{
                        $(".update_notice").html("<div class='alert alert-danger'>error! somthing went wrong , please try after sometimes <i class='fa fa-times close' data-dismiss='alert'> </i> </div>");
                    }
                     
                    
                }
            })
        });
    });
    </script>
</body>
</html>