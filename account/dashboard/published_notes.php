<?php
require("../check_valid_user.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Published Notes - <?php echo $full_name; ?></title>
    <link rel="icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="48x48">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <link rel="shortcut icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <?php include("includes/css.php"); ?>
    <!-- context style  -->
    <style>
        .context_btn:hover {
            cursor: pointer;
        }

        .context_buttons {
            padding: 10px 10px;
        }

        .context_buttons .row {
            padding: 5px 0px;
        }

        .context_buttons .row:hover {
            cursor: pointer;
            box-shadow: 0px 0px 3px #ccc;
            border-radius: 10px;
            color: red;

        }

        #contxt {
            position: absolute;
            z-index: 9999 !important;
            background-color: white;
            top: 0;
            left: 0;
        }

        .context_show {
            display: block !important;
        }

        .context_hide {
            display: none !important;
        }

        .context_blur {
            width: 100% !important;
            min-height: 100vh !important;
            background: rgba(0, 0, 0, 0.2);
            position: absolute;
            top: 0;
            left: 0;
            z-index: 999;
            position: fixed;
        }
    </style>

    <!-- modal style  -->

    <style>
        .modal.fade .modal-bottom,
        .modal.fade .modal-left,
        .modal.fade .modal-right,
        .modal.fade .modal-top {
            position: fixed;
            z-index: 1055;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: 0;
            max-width: 100%
        }

        .modal.fade .modal-right {
            left: auto !important;
            transform: translate3d(100%, 0, 0);
            transition: transform .3s cubic-bezier(.25, .8, .25, 1)
        }

        .modal.fade.show .modal-bottom,
        .modal.fade.show .modal-left,
        .modal.fade.show .modal-right,
        .modal.fade.show .modal-top {
            transform: translate3d(0, 0, 0)
        }

        .w-xl {
            width: 320px
        }

        .modal-content,
        .modal-footer,
        .modal-header {
            border: none
        }

        .list-group.no-radius .list-group-item {
            border-radius: 0 !important
        }

        .btn-light {
            color: #212529;
            background-color: #f5f5f6;
            border-color: #f5f5f6
        }

        .btn-light:hover {
            color: #212529;
            background-color: #e1e1e4;
            border-color: #dadade
        }

        .modal-footer {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            padding: 1rem;
            border-top: 1px solid rgba(160, 175, 185, .15);
            border-bottom-right-radius: .3rem;
            border-bottom-left-radius: .3rem
        }
    </style>
    <!-- edit notes style -->
    <style type="text/css">
        .title {
            color: #000;
        }

        .title h1 span {
            line-height: 10px !important;
        }

        .title h1 {
            font-weight: bold;
        }

        .public {
            color: red;
        }

        .itm {
            background-color: #fff;
            padding: 15px;
            margin: 15px 10px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px #ccc;
        }

        .item {
            background-color: #fff;
            padding: 15px;
            border-radius: 10px;
            box-shadow: 10px 10px 10px #ccc;
        }

        .itm h2,
        h4 {
            color: #696969;
            font-weight: bold;
            padding-bottom: 5px;
        }

        .item h3 {
            color: #000;
            font-weight: bold;
            font-size: 16px;
            line-height: 20px;
        }

        .item h4 {
            color: #000;
            font-weight: bold;
            font-size: 12px;
            line-height: 10px;

        }

        .file p {
            color: red;
        }

        .file_span {
            color: #000;
            font-size: 18px;
            padding-bottom: 10px;

        }

        .btn_save {
            background-color: red;
            color: #fff;
            border: none;
            padding: 10px 25px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        .new {
            display: flex;
        }

        .second h4 {
            padding-left: 15px;
        }

        .icon_file {
            width: 70px;
            height: 70px;
        }

        @media (max-width:768px) {
            .title h1 {
                font-size: 25px;
                line-height: 30px;
                text-align: left;
            }

            .itm {
                padding: 0px;
            }

            .icon_file {
                display: none;
            }

            .file_heading {
                font-size: 15px;
                font-weight: bold;

            }

            .row h4 {
                font-size: 12px;
            }

        }
    </style>
</head>

<body class="sidebar-light">
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php include("includes/header.php"); ?>
        <!-- Navbar End -->

        <!-- Sidebar Start -->
        <?php include("includes/aside.php");  ?>
        <!-- Sidebar End -->

        <!-- Main Container Start -->
        <main class="main--container">
            <!-- Main Content Start -->
            <section class="main--content">
                <div class="panel">
                    <div class="panel-content">
                        <button class="btn btn-danger btn-sm border-0 mb-3 d-none" id="publish_direct">Publish new notes <i class="fa fa-plus"> </i> </button>
                        <div class="published_notes row">
                            <h1 class="text-center">
                                <i class="fa fa-spinner fa-spin"> </i>
                                Loading....
                            </h1>
                        </div>
                        <div class="no_files text-center d-none text-info">
                            <h1>
                                <i class="fa fa-file "> </i>
                                You haven't published any notes
                            </h1>
                            <button class="btn btn-danger" id="publish_new_direct"> Publish new notes</button>
                        </div>


                    </div>
                </div>
            </section>
            <!-- Main Content End -->
        </main>


        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->

    <!-- context  -->

    <div id="contxt" class="context_hide" style="box-shadow:0px 0px 8px -1px #ccc;border-radius:10px;padding:10px  10px;text-align:center;width:180px">
        <small>Options</small>
        <div class="context_buttons mt-1 w-100 text-left">


            <div class="row view_published my-2">
                <div class="col-2">
                    <i class="fa fa-external-link-square-alt"></i>
                </div>
                <div class="col-8"> View </div>
            </div>


            <div class="row  my-2 edit_published ">
                <div class="col-2">
                    <i class="fa fa-pen-square"></i>
                </div>
                <div class="col-8"> Edit </div>
            </div>



            <div class="row delete_published my-2">
                <div class="col-2">
                    <i class="fa fa-cloud-download-alt"></i>
                </div>
                <div class="col-8"> Delete </div>
            </div>

        </div>
    </div>

    <div class="context_blur context_hide"> </div>

    <!--  edit modal  -->
    <div id="modal-top-edit" class="modal fade" data-backdrop="true">
        <div class="modal-dialog modal-top">
            <div class="modal-content no-radius">
                <div class="modal-header">
                    <div class="modal-title text-md">Edit <span class="course_name_header text-danger"> </span> </div> <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="height:80vh;overflow-y:scroll">
                    <div class="row itm p-0">
                        <div class="col-md-2">
                            <img src="assets/img/icons/pdf.png" class="icon_file">
                        </div>
                        <div class="col-md-10">
                            <h3 class="file_heading"></h3>
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>File Size: <span class="file_size"> </span> </h4>
                                </div>
                                <div class="col-md-9 ">
                                    <h4>Published Date : <span class="upload_date"></span> </h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Views: 300</h4>
                                </div>
                                <div class="col-md-3">
                                    <h4>Like: 14</h4>
                                </div>
                                <div class="col-md-3">
                                    <h4>Dislike: 1</h4>
                                </div>
                                <div class="col-md-3">
                                    <h4>Notes Point: 140</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form id="edit_form">
                        <div class="form-group">
                            <label for="addnote">Title<span class="star">*</span></label>
                            <input type="text" class="form-control" id="note" name="title" placeholder="Enter Note Title">
                        </div>
                        <div class="form-group">
                            <label for="notes_description">Description <span class="star">*</span></label>
                            <textarea id="editor" name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="addnotes">Notes Related Tags<span class="star">*</span></label>
                            <input type="text" class="form-control" id="notes_tag" name="tags" placeholder="example,example2">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                            </select>
                        </div>
                        <button class="btn_save" type="submit">Update</button>
                    </form>
                    <script src="<?php echo $home_page; ?>\management\admin\assets\ckeditor\ckeditor.js"></script>
                    <script type="text/javascript">
                        CKEDITOR.replace('editor');
                    </script>

                </div>
                <div class="modal-footer"><button type="button" class="btn btn-light" data-dismiss="modal">Close</button></div>
            </div>
        </div>
    </div>

    <!-- edit modal -->

    <!-- publis modal  -->
    <div id="modal-top-publish" class="modal fade" data-backdrop="true">
        <div class="modal-dialog modal-top">
            <div class="modal-content no-radius">
                <div class="modal-header">
                    <div class="modal-title text-md">Publish New Notes <span class="course_name_header text-danger"> </span> </div> <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="height:80vh;overflow-y:scroll">
                    <div class="row  p-1">
                        <label for="upload_file"> Choose pdf file </label>
                        <input type="file" accept=".pdf" class="form-control" id="new_file" name="file">
                    </div>

                    <form class="publis_new_form">
                        <div class="form-group">
                            <label for="addnote">Title<span class="star">*</span></label>
                            <input type="text" class="form-control" name="notes_title" placeholder="Enter Note Title">
                        </div>
                        <div class="form-group">
                            <label for="notes_description">Description <span class="star">*</span></label>
                            <textarea id="editor2" name="notes_description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="addnotes">Notes Related Tags<span class="star">*</span></label>
                            <input type="text" class="form-control" name="notes_tags" placeholder="example,example2">
                        </div>
                        <div class="form-group">
                            <label for="notes_status">Status</label>
                            <select name="notes_status" id="d_notes_status" class="form-control">
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                            </select>
                        </div>
                        <button class="btn_save">Publish</button>
                    </form>
                    <script src="<?php echo $home_page; ?>\management\admin\assets\ckeditor\ckeditor.js"></script>
                    <script type="text/javascript">
                        CKEDITOR.replace('editor2');
                    </script>

                </div>
                <div class="modal-footer"><button type="button" class="btn btn-light" data-dismiss="modal">Close</button></div>
            </div>
        </div>
    </div>

    <!-- publish modal -->

    <!-- context -->

    <?php include("includes/js.php"); ?>
    <script src="https://sdk.amazonaws.com/js/aws-sdk-2.855.0.min.js"></script>
    <script>
        $(document).ready(function() {
            function get_all() {
                $.ajax({
                    type: "POST",
                    url: "php/publish_notes.php",
                    data: {
                        for: "get_all_user_notes"
                    },
                    beforeSend: () => {},
                    success: (result) => {
                        // result = JSON.parse(result);
                        if (result.status == "success") {
                            $("#publish_direct").removeClass("d-none");
                            let container = $(".published_notes");
                            $(".published_notes").html("");
                            var notes_data = result.data;
                            for (let i = 0; i < notes_data.length; i++) {
                                let id = notes_data[i].id;
                                let title = notes_data[i].title;
                                let description = notes_data[i].description;
                                let tags = notes_data[i].tags;
                                let file_id = notes_data[i].file_id;
                                let updated_date = notes_data[i].updated_date;
                                let status = notes_data[i].status;
                                $(container).append('<div class="col-md-4 file_box" data-id="' + id + '" data-file-id="' + file_id + '"> <div class="row my-2 p-2 w-100" style="box-shadow:0px 0px 8px -1px #ccc;border-radius:10px"> <div class="col-2 p-0"> <img src="<?php echo $home_page; ?>/account/dashboard/assets/img/icons/pdf.png" style="width:100% !important; height:100% !important"> </div> <div class="col-9 p-0 py-2 px-1"> <h6> ' + title + ' </h6>   </div> <div class="col-1 p-0"> <i class="fa fa-ellipsis-v context_btn"></i> </div> <div class="col-12"> Publish date : ' + updated_date + ' </div> <div class="col-12"> Status : ' + status + ' </div> </div>  </div>');
                            }
                            context();
                        } else if (result.status == "notfound") {
                            $(".published_notes").html("");
                            $(".no_files").removeClass("d-none");

                        } else {
                            swal("error", "somthing went wrong , please try after somthime", "error");
                        }
                    }
                });
            };
            get_all();


            // context funtions 

            function show() {
                $("#contxt").removeClass("context_hide");
                $("#contxt").addClass("context_show");
                $(".context_blur").addClass("context_show");
                $(".context_blur").removeClass("context_hide");
            }

            function hide() {
                $("#contxt").addClass("context_hide");
                $("#contxt").removeClass("context_show");
                $(".context_blur").removeClass("context_show");
                $(".context_blur").addClass("context_hide");
            }

            function context() {

                $(".context_btn").each(function() {
                    $(this).click(function(event) {
                        var parent = this.parentElement.parentElement.parentElement;
                        var left = event.pageX - 150 + "px";
                        var top = event.pageY - 50 + "px";
                        $("#contxt").css({
                            left: left,
                            top: top
                        });
                        sessionStorage.setItem("active_pub_id", $(parent).attr("data-id"));
                        sessionStorage.setItem("active_pub_file_id", $(parent).attr("data-file-id"));
                        show();
                    });
                });

                $(".file_box").each(function() {
                    $(this).on("contextmenu", function(event) {
                        var parent = this;
                        var left = event.pageX - 150 + "px";
                        var top = event.pageY - 50 + "px";
                        $("#contxt").css({
                            left: left,
                            top: top
                        });
                        show();
                        show();
                        sessionStorage.setItem("active_pub_id", $(this).attr("data-id"));
                        sessionStorage.setItem("active_pub_file_id", $(this).attr("data-file-id"));
                        return false;
                    });
                });

                $(".context_blur").click(function() {
                    hide();
                });

            }

            // context functions edn
            // notes action 

            $(".delete_published").click(function() {
                let file_id = sessionStorage.getItem("active_pub_file_id");
                let notes_id = sessionStorage.getItem("active_pub_id");
                swal({
                        title: "Are you sure?",
                        text: "your published notes will be deleted , don't worry your saved notes will not deleted",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: "POST",
                                url: "php/publish_notes.php",
                                data: {
                                    for: "delete_user_notes",
                                    notes_id: notes_id,
                                    file_id: file_id
                                },
                                beforeSend: function() {
                                    swal({
                                        text: "Processing....",
                                        button: false,
                                        allowEscapeKey: false,
                                        allowOutsideClick: false,
                                        closeOnConfirm: false,
                                        closeOnClickOutside: false
                                    });
                                },
                                success: (result) => {
                                    // console.log(result);
                                    swal.close();
                                    if (result.status == "success") {
                                        get_all();
                                        swal("success", "notes deleted", "success");
                                        hide();
                                    } else {
                                        swal("erro", "notes not deletd , please try after sometime", "error");
                                    }
                                }
                            })
                        }
                    })
            });
            $(".edit_published").click(function() {
                hide();
                $("#modal-top-edit").modal("show");
                let file_id = sessionStorage.getItem("active_pub_file_id");
                let notes_id = sessionStorage.getItem("active_pub_id");
                get_notes_data(file_id, notes_id);
            });


            function get_notes_data(file_id, notes_id) {
                $.ajax({
                    type: "POST",
                    url: "php/publish_notes.php",
                    data: {
                        for: "published_notes_data",
                        file_id: file_id,
                        notes_id: notes_id
                    },
                    beforeSend: () => {
                        swal({
                            text: "please wait....",
                            button: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            closeOnConfirm: false,
                            closeOnClickOutside: false
                        });
                    },
                    success: (result) => {
                        swal.close();

                        function bytesToSize(bytes) {
                            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                            if (bytes == 0) return '0 Byte';
                            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
                        }
                        if (result.status == "success") {
                            let title = result.data.title;
                            let updated_date = result.data.updated_date;
                            let description = result.data.description;
                            let tags = result.data.tags;
                            let size = result.data.size;
                            let notes_status = result.data.status;
                            $(".file_heading").html(title);
                            $(".file_size").html(bytesToSize(size));
                            $(".upload_date").html(updated_date);
                            $("input[name='title']").val(title);
                            var objEditor = CKEDITOR.instances["editor"];
                            var dec = objEditor.setData(description);
                            $("input[name='tags']").val(tags);
                            $("#status").val(notes_status);
                            update(file_id, notes_id);

                        }
                    }
                });
            }

            function update(file_id, notes_id) {
                $("#edit_form").submit((event) => {
                    event.preventDefault();
                    var title = $("input[name='title']").val();
                    var tags = $("input[name='tags']").val();
                    var status = $("select[name='status']").val();
                    var objEditor = CKEDITOR.instances["editor"];
                    var description = objEditor.getData(description);
                    if (title.length !== 0 && tags.length !== 0 && description.length !== 0) {
                        swal({
                            title: "Are you sure?",
                            text: "Please make sure have you entered correct details",
                            icon: "warning",
                            buttons: true,
                            dangerMode: false,
                        }).then((confirm) => {
                            if (confirm) {
                                $.ajax({
                                    type: "POST",
                                    url: "php/publish_notes.php",
                                    data: {
                                        for: "update_publish_notes",
                                        file_id: file_id,
                                        notes_id: notes_id,
                                        title: title,
                                        tags: tags,
                                        description: description,
                                        status: status
                                    },
                                    beforeSend: () => {},
                                    success: (result) => {
                                        if (result.status == "success") {
                                            swal("success", "Notes successfully updated");
                                            $(".modal").modal("hide");
                                            $("form").trigger("reset");
                                            get_all();
                                        } else {
                                            swal("error", "somthing went wrong , please try again", "error");
                                        }
                                    }
                                })
                            }
                        });
                    } else {
                        swal("warning", "all filed are required", "warning");
                    }
                });
            }
            $(".view_published").click(() => {
                let file_id = sessionStorage.getItem("active_pub_file_id");
                let notes_id = sessionStorage.getItem("active_pub_id");
                $.ajax({
                    type: "POST",
                    url: "php/publish_notes.php",
                    data: {
                        for: "get_pub_view_url",
                        notes_id: notes_id,
                        file_id: file_id
                    },
                    beforeSend: () => {

                    },
                    success: (result) => {
                        if (result.status == "success") {
                            open(result.data, "_blank");
                        } else {
                            swal("erro", "somthing went wrong , please try after sometimes", "error");
                        }
                    }
                })
            });


            // direct public 


            function getCookie(cookieName) {
                var name = cookieName + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i].trim();
                    if ((c.indexOf(name)) == 0) {
                        return c.substr(name.length);
                    }

                }
                return null;
            };

            var id = getCookie("id");

            function direct_new_publish() {
                $("#publish_new_direct").click(function() {
                    publish();
                });
                $("#publish_direct").click(function() {
                    publish();
                });

                function publish() {
                    $("#modal-top-publish").modal("show");
                    $(".publis_new_form").submit(function(event) {
                        event.preventDefault();

                        var title = $("input[name='notes_title']").val();
                        var objEditor = CKEDITOR.instances["editor2"];
                        var description = objEditor.getData(description);
                        var tags = $("input[name='notes_tags']").val();
                        var status = $("#d_notes_status").val();
                        var file = $("input[type='file']");
                        var file_obj = file[0].files[0];

                        // console.log(file_obj);

                        if (title.length !== 0 && description.length !== 0 && tags.length !== 0 && file[0].files.length !== 0) {
                            var file_name = file[0].files[0].name.split('.').pop();
                            var filename = file[0].files[0].name;

                            var file_size = file[0].files[0].size

                            if (file_name == "pdf") {
                                swal({
                                    title: "Are you sure?",
                                    text: "Please make sure have you entered correct details",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: false,
                                }).then((confirm) => {
                                    if (confirm) {
                                        function upoad_aws() {

                                            // cofig aws s3 
                                            var config = AWS.config;
                                            config.update({
                                                accessKeyId: "AKIASAWDBAK5WAWBA67V",
                                                secretAccessKey: "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",
                                            });
                                            config.region = "ap-south-1";
                                            var s3 = new AWS.S3();
                                            <?php if ($home_page == "http://localhost/notesocean") {
                                                $btk =  "test.notesocean.com";
                                            } else {
                                                $btk =  "www.notesocean.com";
                                            } ?>
                                            var timeStamp = Math.floor(Date.now() / 1000);
                                            var params = {
                                                Bucket: '<?php echo $btk; ?>',
                                                Key: id + "_" + timeStamp + "_" + filename,
                                                Body: file_obj,
                                                ACL: "public-read"
                                            };
                                            var options = {partSize: 6 * 1024 * 1024, queueSize: 10};
                                            s3.upload(params,options).on("httpUploadProgress", function(event) {
                                                $(".upload_btn_box").addClass("d-none");
                                                // uploading progressing 
                                                var loaded = event.loaded;
                                                var total = event.total;
                                                var total_size = $("#total_size");
                                                var percent = Math.floor((loaded * 100) / total);

                                                swal({
                                                    text: "Please wait..file  is uploading... " + percent + "%",
                                                    button: false,
                                                    allowEscapeKey: false,
                                                    allowOutsideClick: false,
                                                    closeOnConfirm: false,
                                                    closeOnClickOutside: false
                                                })

                                            }).send(function(error, data) {
                                                // console.log(data);
                                                //  complte uploaded 
                                                if (error == null) {
                                                    var url = data.Location;
                                                    var key = data.Key;
                                                    var filename = name;
                                                    // return false;
                                                    update_data_db(key, url);
                                                }
                                            });
                                        };
                                        upoad_aws();
                                    }
                                });

                                function update_data_db(file_key, pdf_url) {

                                    $.ajax({
                                        type: "POST",
                                        url: "php/direct_publish.php",
                                        data: {
                                            pdf_url: pdf_url,
                                            file_key: file_key,
                                            file_size: file_size,
                                            notes_title: title,
                                            notes_description: description,
                                            notes_tags: tags,
                                            notes_status: status,
                                        },
                                        beforeSend: function(data) {
                                            swal({
                                                text: "please wait.... data updating.....",
                                                button: false,
                                                allowEscapeKey: false,
                                                allowOutsideClick: false,
                                                closeOnConfirm: false,
                                                closeOnClickOutside: false
                                            });
                                        },
                                        success: function(data) {
                                            // console.log(data);
                                            if (data.trim() == "success") {
                                                swal("success", "your notes successfully published", "success");
                                            } else {
                                                swal("error", "opps! , somthing went wrong , please try after sometimes", "error");
                                            }
                                            $("#modal-top-publish").modal("hide");
                                            get_all();
                                            $(".publis_new_form").trigger("reset");
                                            var objEditor = CKEDITOR.instances["editor2"];
                                            var dec = objEditor.setData("");
                                            $("input[type='file']").val("");
                                        }
                                    });
                                }
                            } else {
                                swal("warning", "File must be a PDF file", "warning");
                            }
                        } else {
                            swal("warning", "all feild are required", "warning");
                        }

                    });
                };

            }
            direct_new_publish();

        });
    </script>

    <script>
        $(document).ready(function() {


        });
    </script>
</body>

</html>