<?php
require_once("../db.php");
require("../check_valid_user.php");
if (isset($_GET["code"])) {
    $file_id  = base64_decode($_GET["code"]);
    // check user file id
    $check_file = "SELECT * FROM files WHERE id ='$file_id'AND userid = '$user_id'";
    if ($check_file = $file_db->query($check_file)) {
        $user_file = $check_file->fetch_assoc();
        $filename = $user_file["name"];
        $file_size = $user_file["file_size"];
        $file_key = $user_file["file_key"];
        $extension = pathinfo($file_key, PATHINFO_EXTENSION);
        session_start();
        $_SESSION["user_id"] = $user_id;
        $_SESSION["file_id"] = $file_id;
    } else {
        header("Location:index.php");
    }
} else {
    header("Location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Share : <?php echo $filename; ?></title>
    <link rel="icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="48x48">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <link rel="shortcut icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <script>
        var url = window.location.origin + window.location.pathname;
    </script>
    <?php echo $ads_script; ?>
    <?php include("includes/css.php"); ?>
    <!-- custom  style here  -->


    <!-- custom style here  -->
    <style type="text/css">
        .title {
            color: #000;
        }

        .title h1 {
            font-weight: bold;
        }

        .public {
            color: red;
        }

        .itm {
            background-color: #fff;
            padding: 15px;
            margin: 15px 10px;
            border-radius: 10px;
        }

        .file p {
            color: red;
        }

        .file_span {
            color: #000;
            font-size: 26px;
            font-weight: bold;
            padding: 10px 0px;

        }


        .share {
            color: #222;
            font-size: 18px;


        }

        .share p {
            text-align: center;

        }

        .share input {
            border: 1px solid red;
            border-radius: 5px;
            width: 400px;
            height: 40px;
            padding: 15px;
            font-size: 12px;
        }

        .share button {
            background-color: red;
            color: #fff;
            margin-left: 15px;
            padding: 5px 20px;
            font-size: 18px;
            border: none;
            border-radius: 5px;
        }

        .get_link {
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 40px;
        }

        @media (max-width:768px) {
            .title h1 {
                font-weight: bold;
                font-size: 25px;
            }

            .file_icon {
                display: none;
            }

            .itm h3 {
                font-size: 15px;
            }

            .itm p {
                font-size: 20px;
            }

        }
    </style>
</head>


<body class="sidebar-light">
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php include("includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- aside start  -->
        <?php include("includes/aside.php");  ?>
        <!-- aside end  -->

        <!-- Main Container Start -->
        <main class="main--container">
            <!-- Main Content Start -->
            <section class="main--content">
                <div class="panel">
                    <div class="panel-content">
                        <!-- main section  -->
                        <?php
                        $views = "not shared";
                        $check_file_shared = "SELECT * FROM shared WHERE user_id = '$user_id' AND file_id = '$file_id' LIMIT 1";
                        if ($check_file_shared = $file_db->query($check_file_shared)) {
                            if ($row = $check_file_shared->num_rows !== 0) {
                                $shared_details = $check_file_shared->fetch_assoc();
                                $shred_type = $shared_details["shred_type"];
                                $share_key = $shared_details["share_key"];
                                $shared_with = json_decode($shared_details['shared_with']);
                                $all_email = "";
                              
                                $get_views = "SELECT *,COUNT(*) OVER () AS total_count FROM shared_view WHERE shared_key = '$share_key'";
                                if ($get_views = $file_db->query($get_views)) {
                                    if ($get_views->num_rows !== 0) {
                                        $views = $get_views->fetch_assoc();
                                        $views = $views["total_count"];
                                    }else{
                                        $views = 0;
                                    }
                                }

                                echo ' <div class="title">
                                <h1>Share Your <span class="public">Document</span></h1>
                                </div>
                                <div class="row itm">
                                <div class="col-1 file_icon">
                                    <img src="assets/img/icons/' . $extension . '.png">
                                </div>
                                
                                <div class="col-md-5">
                                    <h3>File Name</h3>
                                    <p class="file_span"> ' . $filename . '</p>
                                </div>
                                <div class="col-md-3">
                                    <h3>File Size</h3>
                                    <p class="file_span">' . $file_size . ' </p>
                                </div>
                                
                                <div class="col-md-3">
                                    <h3> Views</h3>
                                    <p class="file_span"> '.$views.' </p>
                                </div>
                                </div>
                                <div class="share">
                                <div class="notice"></div>
                                <p>Share this <span class="public">Document</span> with your friends</p>';
                                
                                if ($shred_type == "individual") {
                                    echo '
                                        <div class="text-center" style="font-size:15px;color:red"> your file is not public , your file can access only ';
                                    echo '
                                        <div class="get_link">
                                <input type="text" name="share"value="';
                                    foreach ($shared_with as $emails) {
                                        echo  $emails . ",";
                                    }
                                    echo '"><button class="update_user">update</button>
                            </div>
                                        
                                        </div>
                                        <div class="get_link">
                                        
                                                <input type="text" name="copy_link" placeholder="" value="' . $home_page . "/share-file?access_key=" . $share_key . '"> <button class="friend_send_copy_link">Copy link </button> <button class="friend_send">Send</button>
                                            </div>
                                            
                                            <div class="public_share_bx"> 
                                            <p>Generate the public url for this <span class="public">Document</span></p>
                                            <div class="get_link">
                                                <button class="public_share_btn">Get Link</button>
                                            </div>
                                            
                                            </div>

                                            <div class="text-center">  <button class="btn btn-warning mx-auto stop_sharing"> <i class="fa fa-hand-paper"> </i> Stop sharing </button> </div>
                                            
                                            
                                            ';
                                } else {
                                    echo ' <div class="text-center" style="font-size:15px;color:red"> your file is now public , anyone can asses this file thorugh this link  </div>  <div class="get_link">
<input type="text" name="copy_link" placeholder="" value="' . $home_page . "/share-file?access_key=" . $share_key . '"> <button class="friend_send_copy_link">Copy link </button> <button class="friend_send">Send</button>


</div> <div class="text-center">  <button class="btn btn-warning mx-auto stop_sharing"> <i class="fa fa-hand-paper"> </i> Stop sharing </button> </div> ';
                                }
                            } else {

                                echo ' <div class="title">
<h1>Share Your <span class="public">Document</span></h1>
</div>
<div class="row itm">
<div class="col-1 file_icon">
    <img src="assets/img/icons/' . $extension . '.png">
</div>

<div class="col-md-5">
    <h3>File Name</h3>
    <p class="file_span"> ' . $filename . '</p>
</div>
<div class="col-md-3">
    <h3>File Size</h3>
    <p class="file_span">' . $file_size . ' </p>
</div>

<div class="col-md-3">
    <h3> Views</h3>
    <p class="file_span"> '.$views.' </p>
</div>
</div>
<div class="share">
<div class="notice"></div>
<p>Share this <span class="public">Document</span> with your friends</p>';
                                echo '<div class="get_link set_user">
                                <input type="text" name="share" placeholder=" Ex - example@gmailcom,example2@gmail.com"><button class="friend_shere">Share</button>
                            </div>

                            <div class="get_link d-none share_url_box">
                                <input type="text" name="copy_link" placeholder=""> <button class="friend_send_copy_link">Copy link </button> <button class="friend_send">Send</button>
                            </div>
<div class="public_share_bx"> 
<p>Generate the public url for this <span class="public">Document</span></p>
<div class="get_link">
    <button class="public_share_btn">Get Link</button>
</div>

</div>  
                            ';
                            }
                        }

                        ?>

                        <div class="text-center d-none stop_cont_box"> <button class="btn btn-warning mx-auto stop_sharing"> <i class="fa fa-hand-paper"> </i> Stop sharing </button> </div>

                    </div>
                    <!-- main section  -->
                    <!-- ads here  -->
                    <?php  include("includes/display_ad.php");?>
                    <!-- ads here  -->


                </div>
    </div>
    </section>
    <!-- Main Content End -->
    </main>

    <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->
    <?php include("includes/js.php"); ?>
    <!-- custom script  -->
    <script>
        $(document).ready(function() {
            // friend share 
            $(".friend_shere").click(function() {
                let emails = $("input[name='share']").val();
                emails = emails.split(",");
                let sherd_with = document.createElement("div");
                for (let i = 0; i < emails.length; i++) {
                    sherd_with.append(emails[i] + "    ");
                }

                swal({
                    title: "Your file only shred with",
                    content: sherd_with,
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: false,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        let email_data = JSON.stringify(Object.assign({}, emails));
                        // send share data to database
                        $.ajax({
                            type: "POST",
                            url: "php/share_with_people.php",
                            data: {
                                for: "user",
                                emails: email_data
                            },
                            beforeSend: function() {},
                            success: function(response) {
                                if (response.status == "success") {
                                    swal("success!", "share this link to your friend to access this file  \n " + "<?php echo $home_page; ?>/share-file?access_key=" + response.share_key, "success");
                                    $(".set_user").trigger("remove");
                                    $(".share_url_box").removeClass("d-none");
                                    $(".stop_cont_box").removeClass("d-none");
                                    $("input[name='copy_link']").val("<?php echo $home_page; ?>/share-file?access_key=" + response.share_key + "");
                                } else {
                                    swal("error", "somthing went wrong , please try after sometime");
                                }
                            }
                        });
                        // send data to database
                    }
                })

            });
            // public share 

            $(".public_share_btn").click(function() {
                swal({
                    title: "Your file can view anyone",
                    content: "your file can access anyone through link",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: false,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        // send share data to database
                        $.ajax({
                            type: "POST",
                            url: "php/share_with_people.php",
                            data: {
                                for: "public",
                            },
                            beforeSend: function() {},
                            success: function(response) {
                                console.log(response);
                                if (response.status == "success") {
                                    swal("success!", "share this link to your friend to access this file  \n " + "<?php echo $home_page; ?>/share-file?access_key=" + response.share_key, "success");
                                    $(".set_user").trigger("remove");
                                    $(".share_url_box").removeClass("d-none");
                                    $(".public_share_bx").trigger("remove");
                                    $(".stop_cont_box").removeClass("d-none");
                                    $("input[name='copy_link']").val("<?php echo $home_page; ?>/share-file?access_key=" + response.share_key + "");
                                } else {
                                    swal("error", "somthing went wrong , please try after sometime");
                                }
                            }
                        });
                        // send data to database
                    }
                })
            });
            $(".friend_send_copy_link").click(function() {
                let copyText = $("input[name='copy_link']");
                copyText.select();
                // copyText.setSelectionRange(0, 99999);
                document.execCommand("copy");
                $(this).html("copied");
            });
            $(".friend_send").click(function() {
                let url = $("input[name='copy_link']").val();
                let share_data = {
                    title: "Hey i'm sharing  to my  notesocean document ",
                    text: "Click the link below to open the document",
                    url: url
                }
                navigator.share(share_data);
            });
            $(".update_user").click(function() {
                let emails = $("input[name='share']").val();
                emails = emails.split(",");
                let sherd_with = document.createElement("div");
                for (let i = 0; i < emails.length; i++) {
                    sherd_with.append(emails[i] + "    ");
                }

                swal({
                    title: "Your file only shred with",
                    content: sherd_with,
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: false,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        let email_data = JSON.stringify(Object.assign({}, emails));
                        // send share data to database
                        $.ajax({
                            type: "POST",
                            url: "php/share_with_people.php",
                            data: {
                                for: "update_user",
                                emails: email_data
                            },
                            beforeSend: function() {},
                            success: function(response) {
                                if (response.status == "success") {
                                    swal("success!", "share this link to your friend to access this file  \n " + "<?php echo $home_page; ?>/share-file?access_key=" + response.share_key, "success");
                                    $(".set_user").trigger("remove");
                                    $(".share_url_box").removeClass("d-none");
                                    $("input[name='copy_link']").val("<?php echo $home_page; ?>/share-file?access_key=" + response.share_key + "");
                                } else {
                                    swal("error", "somthing went wrong , please try after sometime");
                                }
                            }
                        });
                        // send data to database
                    }
                })
            });
            $(".stop_sharing").click(function() {
                swal({
                    title: "are you sure?",
                    text: "Your friends will not be able to access this document",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: false,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: "php/share_with_people.php",
                            data: {
                                for: "stop"
                            },
                            beforeSend: function() {},
                            success: function(response) {
                                if (response.status == "success") {
                                    swal("success!", "your shared file has been deletd and your friend will not acccess", "success");
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    swal("error", "somthing went wrong , please try after sometimes", "error");
                                }
                            }
                        })
                    }
                })
            });
        });
    </script>
    <!-- custom script -->
</body>

</html>