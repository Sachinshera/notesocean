<?php
require("../db.php");
if (isset($_GET["access_key"])) {
    $full_name = "user";
    $picture = $home_page . "/assest/img/logo.png";
    $access_key = $_GET["access_key"];
    // global veriable 

    $found = " Document Not found";
    $doc_name = $found;
    // global veriable 
    // get file data 
    $get_file_data = $file_db->prepare("SELECT * FROM shared WHERE share_key = ?");
    $get_file_data->bind_param("s", $access_key);
    $get_file_data->execute();
    $file_data = $get_file_data->get_result();
    $file_data = $file_data->fetch_assoc();
    // check valid url
    if ($file_data) {
        $file_id = $file_data["file_id"];
        $user_id = $file_data["user_id"];
        $shared_with = $file_data["shared_with"];
        $shred_type = $file_data["shred_type"];
        $status = $file_data["status"];
        // check status
        if ($status == "active") {
            //    get file info 
            $get_doc_info = $file_db->query("SELECT * FROM files WHERE id = '$file_id'");
            if ($get_doc_info) {
                $doc_info = $get_doc_info->fetch_assoc();
                $doc_name = $doc_info["name"];
                $doc_url = $doc_info["url"];
                $file_size = $doc_info["file_size"];
                $found = "found";
                session_start();
                $_SESSION["save_share_key"] = $access_key;
            } else {
                $found = "notfound";
            }
            // get user info 
            if (!empty($_COOKIE["url"])) {
                // get user id 
                $cookiekey = $_COOKIE["url"];
                $get_user_id = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$cookiekey' LIMIT 1");
                if ($get_user_id->num_rows !== 0) {
                    $userid = $get_user_id->fetch_assoc();
                    $userid = $userid["userid"];
                    $_SESSION["user_id"] = $userid;

                    $get_info = $user_db->query("SELECT * FROM profiles WHERE user_id =  '$userid'");
                    if ($get_info->num_rows !== 0) {
                        $user_info = $get_info->fetch_assoc();
                        $full_name = $user_info['fullname'];
                        $email = $user_info['email'];
                        $picture = $user_info['picture'];
                    }
                }
            }
        } else {
            $found = "notfound";
        }
    } else {
        $found = "notfound";
    }
} else {
    header("Location:" . $home_page . "");
}

function  update_views($db, $file_id, $user_id, $shared_key)
{
    $ip_address = file_get_contents('http://checkip.dyndns.com/');
    $ip_address = str_replace("<html><head><title>Current IP Check</title></head><body>Current IP Address: ", "", $ip_address);
    $ip_address = str_replace("</body></html>", "", $ip_address);
    $client_ip = $ip_address;
    $today = date("d-m-Y h:i:sa");
    $sql = $db->query("INSERT INTO shared_view(file_id,user_id,ip_address,date_time,shared_key) VALUES('$file_id','$user_id','$client_ip','$today','$shared_key')");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php echo $doc_name; ?></title>
    <link rel="icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="48x48">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <link rel="shortcut icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <?php include("includes/css.php"); ?>
    <?php echo $ads_script; ?>

    <style type="text/css">
        .title {
            color: #000;
        }

        .title h1 {
            font-weight: bold;
        }

        .public {
            color: red;
        }

        .itm {
            background-color: #fff;
            padding: 15px;
            margin: 15px 10px;
            border-radius: 10px;
        }

        .file p {
            color: red;
        }

        .file_span {
            color: #000;
            font-size: 26px;
            font-weight: bold;
            padding: 10px 0px;

        }


        .share {
            color: #222;
            font-size: 18px;


        }

        .share p {
            text-align: center;

        }

        .share input {
            border: 1px solid red;
            border-radius: 5px;
            width: 400px;
            height: 40px;
            padding: 15px;
            font-size: 12px;
        }

        .share button {
            background-color: red;
            color: #fff;
            margin-left: 15px;
            padding: 5px 20px;
            font-size: 18px;
            border: none;
            border-radius: 5px;
        }

        .get_link {
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 40px;
        }

        @media (max-width:768px) {
            .title h1 {
                font-weight: bold;
                font-size: 25px;
            }

            .file_icon {
                display: none;
            }

            .itm h3 {
                font-size: 15px;
            }

            .itm p {
                font-size: 20px;
            }

        }
    </style>
</head>

<body class="sidebar-light">
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <!-- static header -->
        <header class="navbar navbar-fixed">
            <!-- Navbar Header Start -->
            <div class="navbar--header">
                <!-- Logo Start -->
                <a href="<?php echo $home_page; ?>" class="logo">
                    <img src="<?php echo $home_page; ?>/account/dashboard/assets/img/animate_logo.gif" alt="notesocean-logo">
                </a>
                <!-- Logo End -->

                <!-- Sidebar Toggle Button Start -->
                <a href="#" class="navbar--btn" data-toggle="sidebar" title="Toggle Sidebar">
                    <i class="fa fa-bars"></i>
                </a>
                <!-- Sidebar Toggle Button End -->
            </div>
            <!-- Navbar Header End -->

            <!-- Sidebar Toggle Button Start -->
            <a href="#" class="navbar--btn" data-toggle="sidebar" title="Toggle Sidebar">
                <i class="fa fa-bars"></i>
            </a>
            <!-- Sidebar Toggle Button End -->

            <!-- Navbar Search Start -->
            <div class="navbar--search">
                <form action="search-results.html">
                    <input type="search" name="search" class="form-control" placeholder="Search your notes" required>
                    <button class="btn-link"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- Navbar Search End -->

            <div class="navbar--nav ml-auto">
                <ul class="nav">
                    <!-- Nav User Start -->
                    <li class="nav-item dropdown nav--user ">
                        <a href="#" class="nav-link" data-toggle="dropdown">
                            <img src="<?php echo $picture; ?>" alt="" class="rounded-circle">
                            <span> <?php echo $full_name; ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo $home_page; ?>/account/dashboard/profile.php"><i class="far fa-user"></i>Profile</a></li>
                            <li><a href="#"><i class="fa fa-cog"></i>Settings</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="<?php echo $home_page; ?>/account/logout.php"><i class="fa fa-power-off"></i>Logout</a></li>
                        </ul>
                    </li>
                    <!-- Nav User End -->
                </ul>
            </div>
        </header>

        <!-- static header  -->
        <!-- Navbar End -->

        <!-- Sidebar Start -->
        <?php include("includes/aside.php");  ?>
        <!-- Sidebar End -->

        <!-- Main Container Start -->
        <main class="main--container">
            <!-- Main Content Start -->
            <section class="main--content">
                <div class="panel">
                    <div class="panel-content">
                        <?php
                        if ($found == "found") {
                            echo ' <div class="title">
                        <h1>Document <span class="public">Details</span></h1>
                        </div>
                        ';
                            echo '
                        <div class="row itm">
                            <div class="col-2 file_icon">
                                <img src="' . $home_page . '/account/dashboard/assets/img/icons/' . pathinfo($doc_name, PATHINFO_EXTENSION) . '.png">
                            </div>

                            <div class="col-md-7">
                                <h3>File Name</h3>
                                <p class="file_span">' . $doc_name . ' </p>
                            </div>
                            <div class="col-md-3">
                                <h3>File Size</h3>
                                <p class="file_span">' . $file_size . ' </p>
                            </div>
                        </div>';


                            echo ' <div class="notice_box">';
                            if ($full_name !== "user") {
                                if ($shred_type == "public") {
                                    echo '<div class="alert alert-success text-center"> congratulation now you can open and download this file <br> <a class="btn btn-success" href="' . $home_page . '/open-file?filename=' . $doc_name . '&code=' . base64_encode($doc_url) . '"> Open </a>
                                    <br>
                                    <br>
                                  <a class="btn btn-danger" href="' . $home_page . '/download-file?filename=' . $doc_name . '&code=' . base64_encode($doc_url) . '"> Dowanload </a>
                                  <div class="text-center mt-2"> <button class="btn btn-primary save_btn"> Save</button>  </div>
                                  </div>';
                                    update_views($file_db, $file_id, $user_id, $access_key);
                                } else {
                                    $shared_with = json_decode($shared_with);
                                    $access = "";
                                    foreach ($shared_with as $approve_email) {
                                        if ($email == $approve_email) {
                                            $access = "ok";
                                        } else {
                                            $access = "no";
                                        }
                                    }
                                    if ($access == "no") {
                                        echo '<div class="alert alert-danger text-center"> sorry , you do not have permission to access this file , make sure you had login with correct email address <br>  <a class="btn btn-danger" href="' . $home_page . '/account/logout.php"> Logout </a> </div>';
                                    } else {
                                        echo '<div class="alert alert-success text-center"> congratulation now you can open and download this file <br> <a class="btn btn-success" href="' . $home_page . '/open-file?filename=' . $doc_name . '&code=' . base64_encode($doc_url) . '"> Open </a>
                                    <br>
                                    <br>
                                  <a class="btn btn-danger" href="' . $home_page . '/download-file?filename=' . $doc_name . '&code=' . base64_encode($doc_url) . '"> Dowanload </a>
                                  <div class="text-center mt-2"> <button class="btn btn-primary save_btn"> Save</button>  </div>
                                  </div>';
                                        update_views($file_db, $file_id, $user_id, $access_key);
                                    }
                                }
                            } else {
                                echo '<div class="alert alert-danger text-center">  you cannot acces this  document because you have not logged in <br>  <a class="btn btn-danger" href="' . $home_page . '/account?redirect_to=' . $home_page . '/share-file?access_key=' . $access_key . '"> Login here </a> </div>';
                            }
                            echo '</div>';
                        } else {
                            echo '<div class="alert alert-danger text-center"> <span style="font-size:20px"> opps!  </span>  Document not found, kindly recheck your link </div>';
                        }
                        ?>

                    </div>
                    <!-- ads here  -->
                    <?php include("includes/display_ad.php"); ?>
                    <!-- ads here  -->
                </div>
            </section>
            <!-- Main Content End -->
        </main>
        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->
    <?php include("includes/js.php"); ?>
    <script>
        $(document).ready(function() {
            $(".save_btn").click(function() {
                swal({
                    title: "Save this document?",
                    text: "This document will visible to you as long as it is sahred",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, save it'
                    ],
                    dangerMode: false,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo $home_page; ?>/account/dashboard/php/share_with_people.php",
                            data: {
                                for: "save_shared"
                            },
                            beforeSend: function() {},
                            success: function(response) {
                                console.log(response);
                                if(response.status=="success"){
                                    swal("success","document saved , you can see in shared documents","success");
                                } else if(response.status=="exist"){
                                    swal("warning","document already saved","warning");
                                }
                                else{
                                    swal("error","somthing went wrong , please try after sometimes","erro");
                                }
                            }
                        })
                    }
                });

            })
        })
    </script>
</body>