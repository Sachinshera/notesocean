<?php
require_once("../db.php");
require("../check_valid_user.php");
$_SESSION["user_id"] = $user_id;
?>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Shared  Document : <?php echo $full_name; ?></title>
    <link rel="icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="48x48">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <link rel="shortcut icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <script>
        var url = window.location.origin + window.location.pathname;
    </script>
    <?php echo $ads_script; ?>
    <?php include("includes/css.php"); ?>

    <style>
        .context_btn:hover {
            cursor: pointer;
        }

        .context_buttons {
            padding: 10px 10px;
        }

        .context_buttons .row {
            padding: 5px 0px;
        }

        .context_buttons .row:hover {
            cursor: pointer;
            box-shadow: 0px 0px 3px #ccc;
            border-radius: 10px;
            color: red;

        }

        #contxt {
            position: absolute;
            z-index: 20 !important;
            background-color: white;
            top: 0;
            left: 0;
        }

        .context_show {
            display: block !important;
        }

        .context_hide {
            display: none !important;
        }

        .context_blur {
            width: 100% !important;
            height: 100vh !important;
            background: rgba(0, 0, 0, 0.2);
            position: absolute;
            top: 0;
            left: 0;
            z-index: 10;
        }
    </style>
</head>

<body class="sidebar-light">
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php include("includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- aside start  -->
        <?php include("includes/aside.php");  ?>
        <!-- aside end  -->

        <!-- Main Container Start -->
        <main class="main--container">
            <!-- Main Content Start -->
            <section class="main--content">
                <div class="panel">
                    <div class="panel-content">
                        <div class="loading_files d-none text-center">
                            <h1>
                                <i class="fa fa-spinner fa-spin"> </i>
                                Loading....
                            </h1>
                        </div>

                        <div class="no_files d-none text-center">
                            <h1>
                                <i class="fa fa-share-alt-square"> </i>
                               Not Shared document!
                            </h1>
                        </div>

                        <div class="row files_cont">
                        </div>

                        <!-- ads here  -->
                        <?php include("includes/display_ad.php"); ?>
                        <!-- ads here  -->
                    </div>
                </div>
            </section>
            <!-- Main Content End -->
        </main>

        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->



    <div id="contxt" class="context_hide" style="box-shadow:0px 0px 8px -1px #ccc;border-radius:10px;padding:10px  10px;text-align:center;width:180px">
        <small>Options</small>
        <div class="context_buttons mt-1 w-100 text-left">


            <div class="row file_open_btn my-2">
                <div class="col-2">
                    <i class="fa fa-external-link-square-alt"></i>
                </div>
                <div class="col-8"> Open </div>
            </div>


            <div class="row file_download_btn my-2">
                <div class="col-2">
                    <i class="fa fa-cloud-download-alt"></i>
                </div>
                <div class="col-8"> Download </div>
            </div>

            <!-- 
            <div class="row  my-2 rename_file_btn">
                <div class="col-2">
                    <i class="fa fa-pen-square"></i>
                </div>
                <div class="col-8"> Rename </div>
            </div> -->



            <!-- <div class="row  my-2">
                <div class="col-2">
                    <i class="fa fa-users"></i>
                </div>
                <div class="col-8"> Make public </div>
            </div> -->

            <!-- <div class="row share_file_btn my-2">
                <div class="col-2">
                    <i class="fa fa-share-alt-square"></i>
                </div>
                <div class="col-8"> Share </div>
            </div> -->

            <div class="row  my-2 file_delete_btn">
                <div class="col-2">
                    <i class="fa fa-trash-alt"></i>
                </div>
                <div class="col-8"> Delete </div>
            </div>

            <!-- <div class="row  my-2">
                <div class="col-2">
                    <i class="fa fa-info-circle"></i>
                </div>
                <div class="col-8"> File info </div>
            </div> -->

        </div>
    </div>

    <div class="context_blur context_hide"> </div>
    <div class="modal" id="rename_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                hello
            </div>
        </div>
    </div>
    <?php include("includes/js.php"); ?>
    <script>
        $(document).ready(function() {
            //  load all user files 
            var url = window.location.origin + window.location.pathname;

            function load() {
                $.ajax({
                    type: "POST",
                    url: "php/share_with_people.php",
                    data: {
                        for: "get_saved_docs"
                    },
                    beforeSend: function() {
                        $(".loading_files").removeClass("d-none");
                    },
                    success: function(response) {
                        console.log(response);
                        $(".loading_files").addClass("d-none");
                        var file_box = $(".files_cont");
                        $(file_box).html("");
                        if (response.status == "success") {
                            $(".no_files").addClass("d-none");
                            var result = response.data;
                            //convert size 
                            function converFromBytes(bytes, decimalLength) {
                                if (bytes == 0) return '0 Bytes';
                                var k = 1000,
                                    dm = decimalLength || 2,
                                    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
                                    i = Math.floor(Math.log(bytes) / Math.log(k));
                                return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
                            }

                            for (let i = 0; i < result.length; i++) {
                                let id = result[i].id;
                                let name = result[i].filename;
                                let file_url = btoa(result[i].url);
                                let shared_by = result[i].shared_by;
                                let size = converFromBytes(result[i].file_size);
                                var extension = name.substring(name.lastIndexOf('.') + 1);
                                //   appending dom
                                $(file_box).append('<div class="col-md-4 file_box" data-name="' + name + '" data-size="' + size + '" " data-id="' + id + '" data-url="' + file_url + '"> <div class="row my-2 p-2 w-100" style="box-shadow:0px 0px 8px -1px #ccc;border-radius:10px"> <div class="col-2 p-0"> <img src="<?php echo $home_page; ?>/account/dashboard/assets/img/icons/' + extension + '.png" style="width:100% !important; height:100% !important"> </div> <div class="col-9 p-0 py-2 px-1"> <h6> ' + name + ' </h6>  </div> <div class="col-1 p-0"> <i class="fa fa-ellipsis-v context_btn"></i> </div>\
                                <div class="col-9 mx-auto"><span> ' + size + ' </span> </div>\
                                <div class="col-9 mx-auto"> <p style="font-weight:600"> shared by  <span style="color:red"> ' + shared_by + ' </span> </p> </div>\
                                </div> </div>');
                            }
                            context();

                        } else if (response.status == "notfound") {
                            $(".no_files").removeClass("d-none");
                        }
                    }
                })
            };
            load();
            var data_url = "";
            var filename = "";
            var data_id = "";

            function show() {
                $("#contxt").removeClass("context_hide");
                $("#contxt").addClass("context_show");
                $(".context_blur").addClass("context_show");
                $(".context_blur").removeClass("context_hide");
            }

            function hide() {
                $("#contxt").addClass("context_hide");
                $("#contxt").removeClass("context_show");
                $(".context_blur").removeClass("context_show");
                $(".context_blur").addClass("context_hide");
            }

            function context() {

                $(".context_btn").each(function() {
                    $(this).click(function(event) {
                        var parent = this.parentElement.parentElement.parentElement;
                        var left = event.pageX - 150 + "px";
                        var top = event.pageY - 50 + "px";
                        $("#contxt").css({
                            left: left,
                            top: top
                        });
                        show();
                        data_url = $(parent).attr("data-url");
                        filename = $(parent).attr("data-name");
                        data_id = $(parent).attr("data-id");


                    });
                });

                $(".file_box").each(function() {
                    $(this).on("contextmenu", function(event) {
                        var parent = this;
                        var left = event.pageX - 150 + "px";
                        var top = event.pageY - 50 + "px";
                        $("#contxt").css({
                            left: left,
                            top: top
                        });
                        show();
                        show();
                        data_url = $(parent).attr("data-url");
                        data_id = $(parent).attr("data-id");
                        filename = $(parent).attr("data-name");
                        return false;
                    });
                });

                $(".context_blur").click(function() {
                    hide();
                });
            }
            // file open btn 

            $(".file_open_btn").click(function(){
                let file_url = atob(data_url);
                let a = document.createElement("a");
                a.href="open_file.php?filename="+filename+"&code="+btoa(file_url);
                a.target ="_blank";
                a.click();
            });
            //  downlaod file function 
            $(".file_download_btn").click(function() {
                let file_url = atob(data_url);
                let a = document.createElement("a");
                a.href="download_file.php?filename="+filename+"&code="+btoa(file_url);
                a.target ="_blank";
                a.click();
            });
            // delete file functionality
            $(".file_delete_btn").click(function() {
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will lose your  this saved document !",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: "POST",
                                url: "php/share_with_people.php",
                                data: {
                                    for: "delete_save_shared",
                                    id: data_id,
                                },
                                beforeSend: function() {},
                                success: function(response) {
                                    console.log(response);
                                    if (response.status == "success") {
                                        swal("Poof! Your  saved file  has been deleted!", {
                                            icon: "success",
                                        });
                                        load();
                                        hide();
                                    } else {
                                        swal("Opps!", "we cannot process at the moment, please try again later!", "error");
                                    }
                                }
                            })
                        }
                    });
            });

        });
    </script>
</body>

</html>