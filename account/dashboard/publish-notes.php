<?php
require("../check_valid_user.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Publish notes</title>
    <?php 
     echo $ads_script;
    include("includes/css.php"); ?>
    <style type="text/css">
        .title {
            color: #000;
        }

        .title h1 span {
            line-height: 10px !important;
        }

        .title h1 {
            font-weight: bold;
        }

        .public {
            color: red;
        }

        .itm {
            background-color: #fff;
            padding: 15px;
            margin: 15px 10px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px #ccc;
        }

        .item {
            background-color: #fff;
            padding: 15px;
            border-radius: 10px;
            box-shadow: 10px 10px 10px #ccc;
        }

        .itm h2,
        h4 {
            color: #696969;
            font-weight: bold;
            padding-bottom: 5px;
        }

        .item h3 {
            color: #000;
            font-weight: bold;
            font-size: 16px;
            line-height: 20px;
        }

        .item h4 {
            color: #000;
            font-weight: bold;
            font-size: 12px;
            line-height: 10px;

        }

        .file p {
            color: red;
        }

        .file_span {
            color: #000;
            font-size: 18px;
            padding-bottom: 10px;

        }

        .btn_save {
            background-color: red;
            color: #fff;
            border: none;
            padding: 10px 25px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        .new {
            display: flex;
        }

        .second h4 {
            padding-left: 15px;
        }

        .icon_file {
            width: 70px;
            height: 70px;
        }
        .panel{
            padding: 20px 10%;
        }

        @media (max-width:768px) {
            .title h1 {
                font-size: 25px;
                line-height: 30px;
                text-align: left;
            }

            .itm {
                padding: 0px;
            }

            .icon_file {
                display: none;
            }

            .file_heading {
                font-size: 15px;
                font-weight: bold;

            }
            .panel{
            padding:10px;
        }
        .row h4{
            font-size: 12px;
        }
       

        }
    </style>
</head>

<body class="sidebar-light">
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php include("includes/header.php"); ?>
        <!-- Navbar End -->

        <!-- Sidebar Start -->
        <?php include("includes/aside.php");  ?>
        <!-- Sidebar End -->

        <!-- Main Container Start -->
        <main class="main--container">
            <!-- Main Content Start -->
            <section class="main--content">
                 <!-- tutorail notice -->
                 <div class="alert alert-info" role="alert" id="alert"> <strong>Wecome <?php echo $full_name; ?>  </strong> <br> If you don't know how to use this website , we recommend to you  please watch this tutorial <br> <button class="btn btn-sm btn-primary" id="show_tutorial"> Watch </button>  <i class="close fa fa-times" data-dismiss="alert"> </i>  </div>

<!-- tutorail notice -->
                <div class="panel">
                    <div class="title">
                        <h1>Publish your <span class="public">Notes</span> to  website<br> and <span class="public">earn money.</span></h1>
                    </div>

                    <div class="row itm mt-5">
                        <div class="col-md-2">
                            <img src="assets/img/icons/pdf.png" class="icon_file">
                        </div>
                        <div class="col-md-10">
                            <h3 class="file_heading"></h3>
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>File Size: <span class="file_size"> </span> </h4>
                                </div>
                                <div class="col-md-9 ">
                                    <h4>Uploaded Date : <span class="upload_date"></span> </h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form>
                        <div class="form-group">
                            <label for="addnote">Title<span class="star">*</span></label>
                            <input type="text" class="form-control" id="note" name="title" placeholder="Enter Note Title">
                        </div>

                        <div class="form-group">
                            <label for="notes_description">Description <span class="star">*</span></label>
                            <textarea id="editor" name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="addnotes">Notes Related Tags<span class="star">*</span></label>
                            <input type="text" class="form-control" id="notes_tag" name="tags" placeholder="example,example2">
                        </div>
                        <button class="btn_save">Publish</button>
                    </form>
                    <script src="<?php echo $home_page; ?>\management\admin\assets\ckeditor\ckeditor.js"></script>
                    <script type="text/javascript">
                        CKEDITOR.replace('editor');
                    </script>

                </div>
    </div>
    </section>
    <!-- Main Content End -->
    </main>
    <!-- Main Container End -->
    </div>
    <?php include("includes/js.php"); ?>
    <script>
        $(document).ready(() => {
            var publish_id = localStorage.getItem("publish_id");
            if (publish_id !== null) {
                // check exist 

                $.ajax({
                    type: "POST",
                    url: "php/publish_notes.php",
                    data: {
                        for: "check",
                        id: publish_id
                    },
                    beforeSend: () => {},
                    success: (result) => {
                        if (result.status == "exist") {
                            swal({
                                text: "This notes is already published",
                                icon: "warning",
                                buttons: [
                                    'My document',
                                    'preview'
                                ],
                                dangerMode: false,
                            }).then(function(isConfirm) {
                                if (isConfirm) {
                                    open(result.url,"_blank");
                                    window.location = "index.php";
                                    // window.location = result.url;
                                } else {
                                    window.location = "index.php";
                                }
                            })
                        } else {
                            get_notes_data();
                        }
                    }

                });

                //    get notes data
                function get_notes_data() {
                    $.ajax({
                        type: "POST",
                        url: "php/publish_notes.php",
                        data: {
                            for: "get_notes_data",
                            id: publish_id
                        },
                        beforeSend: () => {
                            swal({
                                text: "please wait....",
                                button: false,
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                closeOnConfirm: false,
                                closeOnClickOutside: false
                            });
                        },
                        success: (result) => {
                            swal.close();

                            function bytesToSize(bytes) {
                                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                                if (bytes == 0) return '0 Byte';
                                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
                            }
                            if (result.status == "success") {
                                var filename = result.data.filename;
                                var file_size = result.data.file_size;
                                var upload_date = result.data.upload_date;
                                $(".file_heading").html(filename);
                                $(".file_size").html(bytesToSize(file_size));
                                $(".upload_date").html(upload_date);
                                $("input[name='title']").val(filename);
                                publish();
                            } else {
                                swal("error", "somthing went wrong , please try after sometime", "error");
                                setTimeout(() => {
                                    window.location = "index.php";
                                }, 1000);
                            }
                        }
                    });
                }

                function publish() {
                    $("form").submit((event) => {
                        event.preventDefault();
                        var title = $("input[name='title']").val();
                        var tags = $("input[name='tags']").val();
                        var objEditor = CKEDITOR.instances["editor"];
                        var description = objEditor.getData(description);
                        if (title.length !== 0 && tags.length !== 0 && description.length !== 0) {
                            swal({
                                title: "Are you sure?",
                                text: "Please make sure have you entered correct details",
                                icon: "warning",
                                buttons: true,
                                dangerMode: false,
                            }).then((confirm) => {
                                if (confirm) {
                                    $.ajax({
                                        type: "POST",
                                        url: "php/publish_notes.php",
                                        data: {
                                            for: "publish",
                                            id: publish_id,
                                            title: title,
                                            tags: tags,
                                            description: description
                                        },
                                        beforeSend: () => {},
                                        success: (result) => {
                                            if (result.status == "success") {
                                                swal({
                                                    text: "Your notes successfully published",
                                                    icon: "success",
                                                    buttons: [
                                                        'My document',
                                                        'preview'
                                                    ],
                                                    dangerMode: false,
                                                }).then(function(isConfirm) {
                                                    if (isConfirm) {
                                                        open(result.url,"_blank");
                                                        window.location = "index.php";
                                                    } else {
                                                        window.location = "index.php";
                                                    }
                                                })
                                            } else {
                                                swal("error", "somthing went wrong , please try again", "error");
                                            }
                                        }
                                    })
                                }
                            });
                        } else {
                            swal("warning", "all filed are required", "warning");
                        }
                    });

                }
            } else {
                window.location = "index.php";
            }

        });
    </script>
</body>

</html>