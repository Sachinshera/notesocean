<?php 
require_once("../../db.php");
header('Content-Type: application/json');
if($_SERVER["REQUEST_METHOD"]=="POST"){
    if(isset($_POST["update_file_name"])){
        $filename = mysqli_real_escape_string($file_db,trim($_POST["filename"]));
        $id = mysqli_real_escape_string($file_db,trim($_POST["id"]));
        $ext = mysqli_real_escape_string($file_db,trim($_POST["ext"]));
        $filename = $filename.".".$ext;
        $cookie_key = $_COOKIE["url"];
        if ($cookie_key == "") {
            $result = array("status"=> "error", "message"=>"user not found");
            echo json_encode($result);
        } else {
            $get_user_id = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$cookie_key' ");
            $user_id = $get_user_id->fetch_assoc();
            $user_id = $user_id["userid"];
            // update filename 
          $update =   $file_db->query("UPDATE files SET name = '$filename' WHERE userid = '$user_id' AND id = '$id'");
          if($update){
            $result = array("status"=> "success", "message"=>"filename updated successfully");
            echo json_encode($result); 
          }else{
            $result = array("status"=> "error", "message"=>"error to change filename");
            echo json_encode($result); 
          }
            
        }
        // $result = array("status"=> "success", "message"=>"ok");
        // echo json_encode($result);
    }
    else{
        $result = array("status"=> "error", "message"=>"invalid request");
        echo json_encode($result);
    }
}else{
    $result = array("status"=> "error", "message"=>"invalid request");
    echo json_encode($result);
}

?>