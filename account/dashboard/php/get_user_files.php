
<?php
header('Content-Type: application/json');
if (isset($_POST["get_user_files"])) {
    $base_url = $_SERVER["SERVER_NAME"];
    require_once("../../db.php");
    $cookie_key = $_COOKIE["url"];
    if ($cookie_key == "") {
        $response = array("status" => "error", "messege" => "cookie not found");
        echo json_encode($response);
    } else {
        $get_user_id = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$cookie_key' ");
        $user_id = $get_user_id->fetch_assoc();
        $user_id = $user_id["userid"];

        $get_user_files = $file_db->query("SELECT * FROM files WHERE userid = '$user_id'");
        if ($get_user_files->num_rows !== 0) {
            $files_array = [];
            while ($file = $get_user_files->fetch_assoc()) {
                array_push($files_array, $file);
            }
            $response = array("status" => "success", "result" => $files_array);
            echo json_encode($response);
        } else {
            $response = array("status" => "notfound", "messege" => "notfound");
            echo json_encode($response);
        }
    }
} else {
    $response = array("status" => "error", "messege" => "invalid request");
    echo json_encode($response);
}
?>