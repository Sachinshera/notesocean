<?php
header('Content-Type: application/json');
require("../../db.php");
session_start();
// function contrl start 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["for"])) {
        $today = date("d-m-Y h:i:sa");
        if ($_SESSION["user_id"] !== "") {
            $user_id  = $_SESSION["user_id"];
            if ($_POST["for"] == "user") {
                $file_id = $_SESSION["file_id"];
                $share_key =  md5($file_id);
                $emails = $_POST["emails"];
                individual($file_db, $file_id, $user_id, $emails, $share_key, $today);
            } else if ($_POST["for"] == "public") {
                $file_id = $_SESSION["file_id"];
                $user_id  = $_SESSION["user_id"];
                public_file($file_db, $user_id, $file_id, $today);
            } else if ($_POST["for"] == "update_user") {
                $emails = $_POST["emails"];
                $file_id = $_SESSION["file_id"];
                $user_id  = $_SESSION["user_id"];
                $share_key =  md5($file_id);
                $emails = $_POST["emails"];
                update_user($file_db, $file_id, $user_id, $emails, $share_key, $today);
            } else if ($_POST["for"] == "stop") {
                $file_id = $_SESSION["file_id"];
                $user_id  = $_SESSION["user_id"];
                stop_sharing($file_db, $file_id, $user_id);
            } else if ($_POST["for"] == "save_shared") {
                $user_id  = $_SESSION["user_id"];
                $access_key = $_SESSION["save_share_key"];
                save_shared($file_db, $user_id, $access_key, $today);
            } else if ($_POST["for"] == "get_saved_docs") {
                get_saved_docs($file_db, $user_id, $user_db);
            } else if ($_POST["for"] == "delete_save_shared") {
                $saved_id =  $_POST["id"];
                delete_saved_shared($file_db, $saved_id);
            } else {
                $array = array("status" => "error", "messege" => "invalid reason");
                echo json_encode($array);
            }
        } else {
            $array = array("status" => "error", "messege" => "file id and userid cannot fetch");
            echo json_encode($array);
        }
    }
} else {
    $array = array("status" => "error", "messege" => "invalid request");
    echo json_encode($array);
}
// function contrl end 



function individual($file_db, $file_id, $user_id, $emails, $share_key, $today)
{
    $sql = "INSERT INTO shared (file_id,user_id,shared_with,share_key,shared_date) VALUES('$file_id','$user_id','$emails','$share_key','$today')";
    if ($file_db->query($sql)) {
        $array = array("status" => "success", "messege" => "file shared successfully", "share_key" => $share_key);
        echo json_encode($array);
    } else {
        $array = array("status" => "error", "messege" => "file cannot shared-> reason ->" . $file_db->error);
        echo json_encode($array);
    }
}
function public_file($file_db, $user_id, $file_id, $today)
{
    $check_exist = "SELECT * FROM shared WHERE user_id ='$user_id' AND file_id = '$file_id' ";
    if ($check_exist = $file_db->query($check_exist)) {
        if ($check_exist->num_rows !== 0) {
            $file_data = $check_exist->fetch_assoc();
            $share_key = $file_data["share_key"];
            // update file data 
            $update = $file_db->query("UPDATE shared SET shred_type = 'public' WHERE user_id = '$user_id'AND file_id = '$file_id'");
            if ($update) {
                $array = array("status" => "success", "messege" => "file shared successfully", "share_key" => $share_key);
                echo json_encode($array);
            } else {
                $array = array("status" => "error", "messege" => "file cannot shared-> reason ->" . $file_db->error);
                echo json_encode($array);
            }
        } else {
            $share_key_gen =  md5($file_id);
            $shared_type = 'public';
            $sql = "INSERT INTO shared (file_id,user_id,shred_type,share_key,shared_date) VALUES('$file_id','$user_id', '$shared_type' ,'$share_key_gen','$today')";
            if ($file_db->query($sql)) {
                $array = array("status" => "success", "messege" => "file shared successfully", "share_key" => $share_key_gen);
                echo json_encode($array);
            } else {
                $array = array("status" => "error", "messege" => "file cannot shared-> reason ->" . $file_db->error);
                echo json_encode($array);
            }
        }
    } else {
        $array = array("status" => "error", "messege" => "file cannot shared-> reason ->" . $file_db->error);
        echo json_encode($array);
    }
}
function update_user($file_db, $file_id, $user_id, $emails, $share_key, $today)
{
    $sql = "UPDATE shared SET shared_with ='$emails' WHERE file_id = '$file_id' AND user_id = '$user_id'";
    if ($file_db->query($sql)) {
        $get_share_key = "SELECT share_key FROM shared WHERE file_id = '$file_id'";
        if ($get_share_key = $file_db->query($get_share_key)) {
            if ($get_share_key->num_rows !== 0) {
                $file_share_key = $get_share_key->fetch_assoc();
                $file_share_key = $file_share_key["share_key"];
                $array = array("status" => "success", "messege" => "file shared successfully", "share_key" => $file_share_key);
                echo json_encode($array);
            } else {
                $array = array("status" => "error", "messege" => "file cannot shared-> reason ->" . $file_db->error);
                echo json_encode($array);
            }
        } else {
            $array = array("status" => "error", "messege" => "file cannot shared-> reason ->" . $file_db->error);
            echo json_encode($array);
        }
    } else {
        $array = array("status" => "error", "messege" => "file cannot shared-> reason ->" . $file_db->error);
        echo json_encode($array);
    }
}

function stop_sharing($file_db, $file_id, $user_id)
{
    $sql = "DELETE FROM shared WHERE file_id = '$file_id'AND user_id = '$user_id'";
    if ($file_db->query($sql)) {
        $array = array("status" => "success", "messege" => "shared file deleted");
        echo json_encode($array);
    } else {
        $array = array("status" => "error", "messege" => "file cannot shared-> reason ->" . $file_db->error);
        echo json_encode($array);
    }
}

function save_shared($file_db, $user_id, $access_key, $today)
{
    $check_exist = $file_db->query("SELECT * FROM save_shared WHERE share_key = '$access_key'AND user_id = '$user_id'");
    if ($check_exist->num_rows == 0) {
        $insert = "INSERT INTO save_shared(share_key,user_id,date_time) VALUES('$access_key','$user_id','$today')";
        if ($file_db->query($insert)) {
            $array = array("status" => "success", "messege" => "data  saved");
            echo json_encode($array);
        } else {
            $array = array("status" => "error", "messege" => "data not saved" . $file_db->error);
            echo json_encode($array);
        }
    } else {
        $array = array("status" => "exist", "messege" => "already saved");
        echo json_encode($array);
    }
}
function get_saved_docs($file_db, $user_id, $user_db)
{
    // c key exsit 
    $get_all_share_key = $file_db->query("select * from save_shared WHERE user_id = '$user_id'");
    if ($get_all_share_key->num_rows !== 0) {
        $data_array = [];
        $status = "notfound";
        while ($row = $get_all_share_key->fetch_assoc()) {
            $share_key = $row["share_key"];
            $share_date_time = $row["date_time"];
            $saved_shared_id  = $row["id"];
            // check share key  and get data 
            $sherd = $file_db->query("select * from shared where share_key = '$share_key'");
            if ($sherd->num_rows !== 0) {
                $shared_data =  $sherd->fetch_assoc();
                $shared_user_id = $shared_data["user_id"];
                $shared_file_id = $shared_data["file_id"];
                // check file and get file data 
                $file_data = $file_db->query("select * from files where id = '$shared_file_id'");
                if ($file_data->num_rows !== 0) {
                    while ($final_file_data = $file_data->fetch_assoc()) {
                        $filename = $final_file_data["name"];
                        $url = $final_file_data["url"];
                        $file_size = $final_file_data["file_size"];
                        // get user shared by user email 
                        $user_data = $user_db->query("select fullname from  profiles where user_id = '$shared_user_id'");
                        $fullname  = "user";
                        if ($user_data->num_rows !== 0) {
                            $user_data = $user_data->fetch_assoc();
                            $fullname = $user_data["fullname"];
                        }
                        $temp_array = array("filename" => $filename, "url" => $url, "file_size" => $file_size, "shared_by" => $fullname, "id" => $saved_shared_id);
                        array_push($data_array, $temp_array);
                        $status = "success";
                    }
                }else{
                    $status = "notfound";
                }
            }
        }
        $array = array("status" => $status, "messege" => "data fetched", "data" => $data_array);
        echo json_encode($array);
    } else {
        $array = array("status" => "notfound", "messege" => "already saved");
        echo json_encode($array);
    }
}
 function delete_saved_shared($file_db,$saved_id){
     $sql = $file_db->query("delete from save_shared where id = '$saved_id'");
     if($sql){
        $array = array("status" => "success", "messege" => "data deleted");
        echo json_encode($array);
     }else{
        $array = array("status" => "error", "messege" => "data not deleted");
        echo json_encode($array);
     }
 }