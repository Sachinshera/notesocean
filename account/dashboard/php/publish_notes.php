
<?php
header('Content-Type: application/json');
require("../../db.php");
// function contrl start 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["for"])) {
        if ($_POST["for"] == "get_notes_data") {
            if (isset($_POST["id"])) {
                notes_data($file_db, $_POST["id"]);
            } else {
                $array = array("status" => "error", "messege" => "id missing");
                echo json_encode($array);
            }
        } else if ($_POST["for"] == "publish") {
            if (isset($_POST["title"]) && isset($_POST["tags"]) && isset($_POST["description"]) && isset($_POST["id"])) {
                $title = mysqli_real_escape_string($db, strip_tags(trim($_POST["title"])));
                $tags = mysqli_real_escape_string($db, strip_tags(trim($_POST["tags"])));
                $description = mysqli_real_escape_string($db,trim($_POST["description"]));
                $file_id = $_POST["id"];
                $user_id = get_userid($user_db);
                $pdf_url = get_pdf_url($file_db, $file_id);
                if ($user_id !== "null") {
                    publish_notes($db, $title, $tags, $description, $file_id, $user_id, $pdf_url, $home_page);
                } else {
                    $array = array("status" => "logout", "messege" => "user not logedin");
                    echo json_encode($array);
                }
            } else {
                $array = array("status" => "error", "messege" => "data missing");
                echo json_encode($array);
            }
        } else if ($_POST["for"] == "check") {
            if (isset($_POST["id"])) {
                check_notes($db, $_POST["id"], $home_page);
            } else {
                $array = array("status" => "error", "messege" => "id missing");
                echo json_encode($array);
            }
        } else if ($_POST["for"] == "get_all_user_notes") {
            $user_id = get_userid($user_db);
            if ($user_id !== "null") {
                get_all_user_notes($db, $user_id);
            } else {
                $array = array("status" => "logout", "messege" => "user logged out");
                echo json_encode($array);
            }
        } else if ($_POST["for"] == "delete_user_notes") {
            if (isset($_POST["notes_id"]) && isset($_POST["file_id"])) {
                $notes_id = $_POST["notes_id"];
                $file_id = $_POST["file_id"];
                $user_id = get_userid($user_db);
                delete_user_notes($db, $notes_id, $file_id, $user_id);
            } else {
                $array = array("status" => "error", "messege" => "data missing");
                echo json_encode($array);
            }
        } else if ($_POST["for"] == "published_notes_data") {
            if (isset($_POST["file_id"]) && isset($_POST["notes_id"])) {
                $notes_id = $_POST["notes_id"];
                $file_id = $_POST["file_id"];
                $user_id = get_userid($user_db);
                get_publlished_data($db, $notes_id, $file_id, $user_id);
            } else {
                $array = array("status" => "error", "messege" => "data missing");
                echo json_encode($array);
            }
        } else if ($_POST["for"] == "update_publish_notes") {
            if (isset($_POST["notes_id"]) && isset($_POST["file_id"]) && isset($_POST["title"]) && isset($_POST["tags"]) && isset($_POST["description"])) {
                $array = array("status" => "ok", "messege" => "data missing");
                $notes_id = $_POST["notes_id"];
                $file_id = $_POST["file_id"];
                $user_id = get_userid($user_db);
                $title = mysqli_real_escape_string($db, strip_tags(trim($_POST["title"])));
                $tags = mysqli_real_escape_string($db, strip_tags(trim($_POST["tags"])));
                $description = mysqli_real_escape_string($db,trim($_POST["description"]));
                $status = $_POST["status"];
                update_publish_notes($db, $file_id, $notes_id, $user_id, $title, $tags, $description, $status);
            } else {
                $array = array("status" => "error", "messege" => "data missing");
                echo json_encode($array);
            }
        } else if ($_POST["for"] == "get_pub_view_url") {
            if (isset($_POST["file_id"]) && isset($_POST["notes_id"])) {
                $notes_id = $_POST["notes_id"];
                $file_id = $_POST["file_id"];
                get_pub_view_url($db, $file_id, $notes_id,$home_page);
            } else {
                $array = array("status" => "error", "messege" => "data missing");
                echo json_encode($array);
            }
        }
    }
}

function notes_data($file_db, $notes_id)
{
    $sql = $file_db->query("select * from files where id = '$notes_id' limit 1");
    if ($sql) {
        if ($sql->num_rows !== 0) {
            $file_data = $sql->fetch_assoc();
            $filename = $file_data["name"];
            $file_size = $file_data["file_size"];
            $upload_date = $file_data["upload_date"];
            $data_array = array("filename" => $filename, "file_size" => $file_size, "upload_date" => $upload_date);
            $array = array("status" => "success", "data" => $data_array);
            echo json_encode($array);
        } else {
            $array = array("status" => "error", "messege" => "file data not found" . $file_db->error);
            echo json_encode($array);
        }
    } else {
        $array = array("status" => "error", "messege" => "cannot process" . $file_db->error);
        echo json_encode($array);
    }
}
function get_userid($user_db)
{
    $cookie_key = $_COOKIE["url"];
    if ($cookie_key == "") {
        $user_id = "null";
    } else {
        $get_user_id = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$cookie_key' ");
        $user_id = $get_user_id->fetch_assoc();
        $user_id = $user_id["userid"];
    }
    return $user_id;
}
function get_pdf_url($file_db, $file_id)
{
    $sql = $file_db->query("SELECT url FROM files WHERE id = '$file_id'");
    if ($sql->num_rows !== 0) {
        $sql = $sql->fetch_assoc();
        $url = $sql["url"];
    } else {
        $url = "null";
    }
    return $url;
}
function publish_notes($db, $title, $tags, $description, $file_id, $user_id, $pdf_url, $home_page)
{
    $short_desc  = strip_tags($description);
    $file_key = $title . ".pdf";
    $today = date("d-m-Y h:i:sa");
    // check exist
    $check_exist = $db->query("SELECT * FROM products WHERE file_id = '$file_id'");
    if ($check_exist->num_rows == 0) {
        $file_size = $check_exist->fetch_assoc();
        $file_size = $file_size["file_size"];
        $sql = $db->query("INSERT INTO products(product_name,short_desc,long_desc,keyword,pdf_url,notes_by,file_id,file_key,created_date,updated_date,size) VALUES('$title','$short_desc','$description','$tags','$pdf_url','$user_id','$file_id','$file_key','$today','$today','$file_size')");
        if ($sql) {
            // get notes data
            $get_notes_data = $db->query("select * from products where file_id = '$file_id'  and notes_by = '$user_id'");
            if ($get_notes_data) {
                $notes_data = $get_notes_data->fetch_assoc();
                $note_id = $notes_data["id"];
                $note_title = $notes_data["product_name"];
                include("clean_string.php");
                $note_title = just_clean($note_title);
                $note_url = $home_page . "/" . $note_id . "/" . $note_title . ".html";
                $array = array("status" => "success", "messege" => "inserted", "url" => $note_url);
                echo json_encode($array);
            } else {
                $array = array("status" => "error", "messege" => "Notes data cannnot fetch");
                echo json_encode($array);
            }
        } else {
            $array = array("status" => "error", "messege" => "not inserted" . $db->error);
            echo json_encode($array);
        }
    } else {
        $array = array("status" => "exist", "messege" => "notes exist");
        echo json_encode($array);
    }
}

function check_notes($db, $file_id, $home_page)
{
    $sql = $db->query("SELECT * FROM products WHERE file_id = '$file_id'");
    if ($sql->num_rows !== 0) {
        $notes_data = $sql->fetch_assoc();
        $title = $notes_data["product_name"];
        $notes_id = $notes_data["id"];
        include("clean_string.php");
        $clean = just_clean($title);
        $url = $home_page . "/" . $notes_id . "/" . $clean . ".html";
        $array = array("status" => "exist", "messege" => "notes exist", "url" => $url);
        echo json_encode($array);
    } else {
        $array = array("status" => "noexist", "messege" => "notes not exist");
        echo json_encode($array);
    }
}
function get_all_user_notes($db, $user_id)
{
    $sql = $db->query("select * from products where notes_by = '$user_id' order by id desc");
    if ($sql) {
        if ($sql->num_rows !== 0) {
            $data_array = [];
            while ($row = $sql->fetch_assoc()) {
                $title = $row["product_name"];
                $description = $row["long_desc"];
                $tags = $row["keyword"];
                $file_id = $row["file_id"];
                $updated_date = $row["updated_date"];
                $status = $row["status"];
                $id = $row["id"];
                $temp_array = array("id" => $id, "title" => $title, "description" => $description, "tags" => $tags, "file_id" => $file_id, "updated_date" => $updated_date, "status" => $status);
                array_push($data_array, $temp_array);
            }
            $array = array("status" => "success", "data" => $data_array);
            echo json_encode($array);
        } else {
            $array = array("status" => "notfound", "messege" => "no notes found");
            echo json_encode($array);
        }
    } else {
        $array = array("status" => "error", "messege" => "error from server");
        echo json_encode($array);
    }
}
function delete_user_notes($db, $notes_id, $file_id, $user_id)
{
    $sql = "DELETE FROM products WHERE id = '$notes_id' AND file_id = '$file_id' AND notes_by = '$user_id'";
    if ($db->query($sql)) {
        $array = array("status" => "success", "messege" => "notes deleted" . $db->error);
        echo json_encode($array);
    } else {
        $array = array("status" => "error", "messege" => "error from server" . $db->error);
        echo json_encode($array);
    }
}
function get_publlished_data($db, $notes_id, $file_id, $user_id)
{
    $sql = $db->query("select * from products where id = '$notes_id' and file_id = '$file_id' and notes_by = '$user_id'");
    if ($sql) {
        if ($sql->num_rows !== 0) {
            $file_data = $sql->fetch_assoc();
            $title = $file_data["product_name"];
            $updated_date = $file_data["updated_date"];
            $description = $file_data["long_desc"];
            $keyword = $file_data["keyword"];
            $size = $file_data["size"];
            $status = $file_data["status"];
            $data_array = array("title" => $title, "updated_date" => $updated_date, "description" => $description, "tags" => $keyword, "size" => $size, "status" => $status);
            $array = array("status" => "success", "data" => $data_array);
            echo json_encode($array);
        } else {
            $array = array("status" => "error", "messege" => "file data not found" . $db->error);
            echo json_encode($array);
        }
    } else {
        $array = array("status" => "error", "messege" => "error from server" . $db->error);
        echo json_encode($array);
    }
}
function update_publish_notes($db, $file_id, $notes_id, $user_id, $title, $tags, $description, $status)
{
    $short_desc = strip_tags($description);
    $today = date("d-m-Y h:i:sa");
    $update = "UPDATE  products SET product_name = '$title',short_desc = '$short_desc',long_desc = '$description',keyword = '$tags',updated_date = '$today',status = '$status' WHERE id = '$notes_id' AND file_id = '$file_id' AND notes_by = '$user_id'";
    if ($db->query($update)) {
        $array = array("status" => "success", "messege" => "notes updated");
        echo json_encode($array);
    } else {
        $array = array("status" => "error", "messege" => "error from server" . $db->error);
        echo json_encode($array);
    }
}
function get_pub_view_url($db, $file_id, $notes_id, $home_page)
{
    $sql = $db->query("select product_name from products where id = '$notes_id' and file_id = '$file_id'");
    if ($sql) {
        if ($sql->num_rows !== 0) {
            $sql = $sql->fetch_assoc();
            $title = $sql["product_name"];
            include("clean_string.php");
            $title = just_clean($title);
            $url = $home_page . "/" . $notes_id . "/" . $title . ".html";
            $array = array("status" => "success", "data" => $url);
            echo json_encode($array);
        } else {
            $array = array("status" => "notfound", "messege" => "notes missing");
            echo json_encode($array);
        }
    } else {
        $array = array("status" => "error", "messege" => "error from server" . $db->error);
        echo json_encode($array);
    }
}
