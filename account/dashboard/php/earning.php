<?php
require_once("../../db.php");
header('Content-Type: application/json');
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["for"])) {
        check_login($user_db);
        $user_id = $_SESSION["user_id"];
        // echo $user_id;
        // function control
        if ($_POST["for"] == "get_view_data") {
            if ($user_id !== null) {
                $response =  get_view_data($db, $user_id);
            } else {
                $response = array("status" => "logout", "messege" => "user logged out");
            }
        } else if ($_POST["for"] == "paid_history") {
            $response =  get_paid_history($pay_db, $user_id);
        } else if ($_POST["for"] == "get_process_payment") {
            $response = get_process_payment($pay_db, $user_id);
        }else if($_POST["for"]=="payout"){
            if(isset($_POST["amount"])&&isset($_POST["account_id"])){
                $amount = $_POST["amount"];
                $account_id = $_POST["account_id"];
                $method = $_POST["method"];
                $response = payout($pay_db,$user_id,$account_id,$amount,$method);
            }else{
                $response = array("status" => "error", "messege" => "data missing");
            }
        }
    } else {
        $response = array("status" => "error", "messege" => "reason missing");
    }
} else {
    $response = array("status" => "error", "messege" => "invalid request");
}
function check_login($user_db)
{
    if (!isset($_SESSION["user_id"])) {
        if ($_COOKIE["url"] != "") {
            // get user id 
            $cookiekey = $_COOKIE["url"];
            $get_user_id = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$cookiekey' ");
            if ($get_user_id->num_rows !== 0) {
                $userid = $get_user_id->fetch_assoc();
                $userid = $userid["userid"];
                session_start();
                $_SESSION["user_id"] = $userid;
            } else {
                $userid = null;
            }
        } else {
            $userid = null;
        }
        $userid = null;
    }
    return $userid;
};

function get_view_data($db, $user_id)
{
    // get total pubished notes 
    $publish_notes_total = 0;
    $total_like = 0;
    $total_dislike = 0;
    $total_views = 0;
    $get_publish_notes = $db->query("SELECT * FROM products WHERE notes_by = '$user_id'");
    if ($get_publish_notes->num_rows !== 0) {
        while ($publish_notes = $get_publish_notes->fetch_assoc()) {
            $publish_notes_total++;
            $notes_id = $publish_notes["id"];
            // get like  
            $get_like = $db->query("SELECT * FROM  reaction WHERE notes_id = '$notes_id' AND react = 1");
            if ($get_like->num_rows !== 0) {
                $get_total_like = $get_like->fetch_assoc();
                $total_like += $get_like->num_rows;
            }
            //  get dislike 
            $get_dislike = $db->query("SELECT * FROM  reaction WHERE notes_id = '$notes_id' AND react = 2");
            if ($get_dislike->num_rows !== 0) {
                $get_total_dislike = $get_dislike->fetch_assoc();
                $total_dislike += $get_dislike->num_rows;
            }

            // get views
            $get_views = $db->query("SELECT * FROM views WHERE notes_id = '$notes_id'");
            if ($get_views->num_rows !== 0) {
                $total_views += $get_views->num_rows;
            }
        }
    }
    $response = array("status" => "success", "total_notes" => $publish_notes_total, "likes" => $total_like, "dislike" => $total_dislike, "total_views" => $total_views);
    return $response;
};

function get_paid_history($pay_db, $user_id)
{
    $sql = $pay_db->query("SELECT * FROM paid WHERE user_id = '$user_id' ORDER BY id DESC");
    if ($sql) {
        if ($sql->num_rows !== 0) {
            $paid_array = [];
            while ($row = $sql->fetch_assoc()) {
                $points = $row["points"];
                $date_time = $row["date_time"];
                $method = $row["method"];
                $status = $row["status"];
                $account_id = $row["account_id"];
                $temp_array = array("points" => $points, "date_time" => $date_time, "method" => $method, "account_id" => $account_id, "status" => $status);
                array_push($paid_array, $temp_array);
            }
            $response = array("status" => "success", "data" => $paid_array);
        } else {
            $response = array("status" => "nodata", "messege" => "no tarnsactions found");
        }
    } else {
        $response = array("status" => "error", "messege" => "error from server" . $pay_db->error);
    }

    return $response;
}

function get_process_payment($pay_db, $user_id)
{
    $sql = $pay_db->query("SELECT * FROM process WHERE user_id = '$user_id' ORDER BY id DESC");
    if ($sql) {
        if ($sql->num_rows !== 0) {
            $pay_aaray = [];
            while ($row = $sql->fetch_assoc()) {
                array_push($pay_aaray, $row);
            }
            $response = array("status" => "success", "data" => $pay_aaray);
        } else {
            $response = array("status" => "nodata", "messege" => "no process paymant");
        }
    } else {
        $response = array("status" => "error", "messege" => "error from server");
    }
    return $response;
};

function payout($pay_db,$user_id,$account_id,$amount,$method)   
{
    $today = date("d-m-Y h:i:sa");
   $sql = $pay_db->query("INSERT INTO process(user_id,points,date_time,method,account_id) VALUES('$user_id','$amount','$today','$method','$account_id')");
   if($sql){
    $response = array("status" => "success", "messege" => "paymant in  process");
   }else{
    $response = array("status" => "error", "messege" => "paymant can't processed".$pay_db->error);
   }
   return $response;
}

// response print

echo json_encode($response);
