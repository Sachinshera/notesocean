<?php 
function just_clean($string)
{
    // Replace other special chars  
    $specialCharacters = array(
        '#' => '',
        '’' => '',
        '`' => '',
        '\'' => '',
        '$' => '',
        '%' => '',
        '&' => '',
        '@' => '',
        '.' => '',
        '€' => '',
        '+' => '',
        '=' => '',
        '§' => '',
        '\\' => '',
        '/' => '',
        '`' => '',
        '•' => ''
    );

    while (list($character, $replacement) = each($specialCharacters)) {
        $string = str_replace($character, '', $string);
    }

    $string = strtr(
        $string,
        "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
        "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn"
    );

    // Remove all remaining other unknown characters  
    $string = preg_replace('/[^a-zA-Z0-9\-]/', ' ', $string);
    $string = preg_replace('/^[\-]+/', '', $string);
    $string = preg_replace('/[\-]+$/', '', $string);
    $string = preg_replace('/[\-]{2,}/', ' ', $string);
    $string = clean_url($string);
    return $string;
}

function clean_url($text)
{
    $text = strtolower($text);
    $code_entities_match = array('&quot;', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '{', '}', '|', ':', '"', '<', '>', '?', '[', ']', ';', "'", ',', '.', '_', '/', '*', '+', '~', '`', '=', '---', '--', '--', '-', '’', '`', '•');
    $code_entities_replace = array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    $text = str_replace($code_entities_match, $code_entities_replace, $text);
    $text = trim($text, " ");
    $text = str_replace(" ", "-", $text);
    $text = cleanUnderScores($text);
    return $text;
}

function cleanUnderScores($text)
{
    $tst = $text;
    $under = "--";
    $pos = 0;

    while (strpos($tst, $under) != false) {
        //$pos = strpos($tst, $under);
        $tst = str_replace("--", "-", $tst);
    }
    return $tst;
}
?>