<?php
require_once("../db.php");
require("../check_valid_user.php");
if (isset($_SESSION["redirect_to"])) {
    $redirect_url = $_SESSION["redirect_to"];
    session_destroy();
    header("Location:" .

        $redirect_url . "");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>My Earning </title>
    <link rel="icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="48x48">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <link rel="shortcut icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <?php
    include("includes/css.php"); ?>
    <!-- css here  -->
    <style type="text/css">
        h2 {
            font-weight: bold;
            font-size: 1.3rem;
            color: #000;
        }

        .t-100 {
            margin-top: 100px;
        }

        .inner_content,
        .withdraw_money,
        .paymet {
            border: 3px solid #E8E8E8;
            padding: 20px 30px;
            color: #333;
        }

        .inner_content h2 {
            text-transform: uppercase;
            text-align: center;
        }

        .inner_content p {
            color: red;
            padding-top: 15px;
            text-align: center;
        }

        #history {
            color: #333;
            font-size: 1.2rem;
            font-weight: 600;
            padding-top: 5px !important;
        }

        table {
            width: 100%;
            font-size: 1.1rem;
            font-weight: 600;

        }

        table tr th {
            font-size: 1.2rem;
            padding: 10px 0;
        }

        table tr td {
            padding: 8px 0;
        }

        table tr td:nth-child(1) {
            color: rgba(0, 0, 0, 0.8);

        }

        table tr td:nth-child(2) {
            color: rgba(0, 0, 0, 0.6);

        }

        table tr td:nth-child(3) {
            color: red;

        }


        .withdraw label {
            font-size: 1rem;
            padding: 10px 0;
        }

        .withdraw input,select {
            border-radius: 25px;
            width: 18vw;
            height: 40px;
        }

        .withdraw button {
            color: #fff;
            background: red;
            padding: 10px 40px;
            margin-left: 15px;
            border: none;
            border-radius: 20px;
            width: 150px;
        }

        .withdraw p {
            color: red;
        }

        @media (max-width:767px) {
            h2 {
                font-size: 1rem;
            }

            .t-100 {
                margin-top: 50px;
            }

            .inner_content,
            .withdraw_money,
            .paymet {
                padding: 10px;
            }

            table {
                padding: 0;
                font-size: .7rem;
            }

            table tr th {
                font-size: 0.8rem;

            }

            #history {
                font-size: 0.8rem;
            }

            .history {
                text-align: center;
            }

            .withdraw label {
                font-size: .8rem;
                padding: 5px 0;
            }

            .withdraw input,select {
                border-radius: 25px;
                width: 55vw;
                height: 35px;
            }

            .withdraw button {
                margin-top: 10px;
                padding: 7px 15px;
                width: 100px;
                margin-left: 0;
            }

            .point_convert table {
                text-align: center;
            }

            .paymet_history table th {
                font-size: 0.7rem;
            }

            .paymet_history table {
                text-align: center;
                font-size: 0.7rem;
            }


        }
    </style>

    <!--css here  -->
</head>

<body class="sidebar-light">

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php include("includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- aside start  -->
        <?php include("includes/aside.php");  ?>
        <!-- aside end  -->
        <!-- Main Container Start -->
        <main class="main--container">

            <section class="main--content">
                <div class="panel">
                    <div class="panel-content">

                     <!-- tutorail notice -->
                     <div class="alert alert-info" role="alert" id="alert"> <strong>Wecome <?php echo $full_name; ?>  </strong> <br> If you don't know how to use this website , we recommend to you  please watch this tutorial <br> <button class="btn btn-sm btn-primary" id="show_tutorial"> Watch </button>  <i class="close fa fa-times" data-dismiss="alert"> </i>  </div>

<!-- tutorail notice -->
                        <!--  code here -->

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 mt-2">
                                    <div class="inner_content">
                                        <h2>Total Data</h2>
                                        <table>
                                            <tbody>
                                                <h1 class="text-center d-none views_loader"> <i class="fa fa-spinner fa-spin"></i> </h1>

                                                <tr>
                                                    <td>Total Published Notes</td>
                                                    <td class="total_notes">0</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Views</td>
                                                    <td class="total_views">0</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Likes</td>
                                                    <td class="total_likes">0</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Dislikes</td>
                                                    <td class="total_dislike">0</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>


                                <div class="col-md-6 mt-2">
                                    <div class="inner_content">
                                        <h2>Total Points <span class="total_points text-success"> 0 </span> </h2>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>View Points</td>
                                                    <td class="views_points">0</td>
                                                </tr>
                                                <tr>
                                                    <td>Like Points</td>
                                                    <td class="likes_points">0</td>
                                                </tr>
                                                <tr>
                                                    <td>Dislike Points</td>
                                                    <td class="dislike_points">0</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                        <p> * Dislike point will substract from the total points </p>
                                    </div>

                                </div>

                            </div>

                            <div class="row t-100">
                                <div class="col-md-12">
                                    <div class="inner_content history">
                                        <h2> Available Point <span class="remain_points text-success">0</span> </h2>
                                        <p id="history"> History </p>
                                        <table>
                                            <tbody class="ramin_pts_history text-center">
                                                <!-- <tr>
                                                    <td>Withdrawl</td>
                                                    <td>20-5-2021</td>
                                                    <td> -1000 Points</td>
                                                </tr>
                                                <tr>
                                                    <td>Withdrawl</td>
                                                    <td>20-5-2021</td>
                                                    <td> -1000 Points</td>
                                                </tr>
                                                <tr>
                                                    <td>Withdrawl</td>
                                                    <td>20-5-2021</td>
                                                    <td> -1000 Points</td>
                                                </tr> -->


                                            </tbody>
                                        </table>
                                    </div>

                                </div>


                            </div>

                            <div class="row t-100 withdraw_money">
                                <div class="col-md-7">
                                    <h2>Withdrawl Money </h2>
                                    <div class="withdraw d-none">
                                        <div class="input_field">
                                            <label> Select payment method</label>
                                            <select name="method" id="method" class="input_field  my-2" autocomplete="off">
                                                <option value="upi"> Upi</option>
                                                <option value="paypal"> Paypal</option>
                                            </select>
                                        </div>

                                        <div class="input_field">
                                            <label>Enter your Account id </label>
                                            <input type="email" class="px-3" id="paypal_email" name="paypal_email" placeholder="Enter your upi id or paypal id">
                                        </div>
                                        <div class="input_field">
                                            <label>Enter points:</label>
                                            <input type="number" class="px-3" id="amount" name="amount">
                                            <button class="withdral_btn">Withdrawl</button>
                                        </div>
                                        <p> *Minimum Withddrwal 1000 Points </p>
                                    </div>
                                    <div class="no_avl_points d-none">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <h1> <i class="fa fa-exclamation-circle text-warning"></i></h1>
                                                <h3>You don't have enough points to withdrwal </h3>
                                                <p class="text-danger"> If your points are above 1000, you can withdrwal your money </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-5">
                                    <div class="point_convert">
                                        <h2>Points Convertor</h2>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>1000 pts</td>
                                                    <td> = </td>
                                                    <td>10 INR </td>
                                                </tr>
                                                <tr>
                                                    <td>1000 pts</td>
                                                    <td> = </td>
                                                    <td>16 NPR </td>
                                                </tr>
                                                <tr>
                                                    <td>1000 pts</td>
                                                    <td> = </td>
                                                    <td>11.6 BDT </td>
                                                </tr>
                                                <tr>
                                                    <td>1000 pts</td>
                                                    <td> = </td>
                                                    <td>0.14 USD </td>
                                                </tr>
                                                <tr>
                                                    <td>1000 pts</td>
                                                    <td> = </td>
                                                    <td>0.11 EURO </td>
                                                </tr>

                                            </tbody>
                                        </table>

                                    </div>

                                </div>

                            </div>


                            <div class="row t-100 paymet">
                                <div class="col-md-12">
                                    <div class="paymet_history">
                                        <h2> Payment History </h2>
                                        <table class="table table-responsive-md">
                                            <thead>

                                                <th>Pts</th>
                                                <th>Date</th>
                                                <th>Payment Method</th>
                                                <th>Account id</th>
                                                <th>Status</th>

                                            </thead>
                                            <tbody class="payment_history">
                                                <!-- <tr>
                                                    <td>1000</td>
                                                    <td>10-05-2021</td>
                                                    <td>Paypal</td>
                                                    <td>sachinshera5@gmail.com</td>
                                                    <td class="text-success">Success</td>
                                                </tr>
                                                <tr>
                                                    <td>1000</td>
                                                    <td>10-05-2021</td>
                                                    <td>Paypal</td>
                                                    <td>sachinshera5@gmail.com</td>
                                                    <td class="text-danger">Failed</td>
                                                </tr> -->

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>

                            <div class="row t-100 paymet">
                                <div class="col-md-12">
                                    <div class="paymet_history">
                                        <h2> Payment Processing </h2>
                                        <table class="table table-responsive-md">
                                            <thead>
                                                <tr>
                                                    <th>Pts</th>
                                                    <th>Date</th>
                                                    <th>Payment Method</th>
                                                    <th>Account Id</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody class="process_payment_box">
                                                <tr>

                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                            <div class="row t-100 paymet d-none">
                                <div class="col-md-12">
                                    <div class="notes">
                                        <h2> Notes by Points </h2>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>1. Convey meaning through color with a handful of color utility classes.</td>
                                                    <td>100 Pts</td>

                                                </tr>
                                                <tr>
                                                    <td>2. Convey meaning through color with a handful of color utility classes.</td>
                                                    <td>100 Pts</td>

                                                </tr>
                                                <tr>
                                                    <td>3. Convey meaning through color with a handful of color utility classes.</td>
                                                    <td>100 Pts</td>

                                                </tr>
                                                <tr>
                                                    <td>4. Convey meaning through color with a handful of color utility classes.</td>
                                                    <td>100 Pts</td>

                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <!--  code here -->
                    </div>
                </div>
            </section>
        </main>
        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->


    <!-- js here  -->

    <?php include("includes/js.php"); ?>
    <!-- js here -->
    <!-- on page script  -->
    <script>
        $(document).ready(() => {
            function get_view_data() {
                $.ajax({
                    type: "POST",
                    url: "php/earning.php",
                    data: {
                        for: "get_view_data"
                    },
                    beforeSend: function() {
                        $(".views_loader").removeClass("d-none");
                    },
                    success: function(result) {
                        $(".views_loader").addClass("d-none");
                        if (result.status == "success") {
                            let total_notes = Number(result.total_notes);
                            let likes = Number(result.likes);
                            let dislike = Number(result.dislike);
                            let total_views = Number(result.total_views);
                            $(".total_notes").html(total_notes);
                            $(".total_views").html(total_views);
                            $(".total_likes").html(likes);
                            $(".total_dislike").html(dislike);
                            calculate_points(likes, dislike, total_views);
                        } else {
                            swal("error", "somthing went wrong , please try after sometimes", "error");
                        }
                        get_paid_history();
                    }
                })
            }
            get_view_data();

            function calculate_points(likes, dislike, total_views) {
                let views_points = (total_views * 5);
                let likes_points = (likes * 10);
                let dislikes_points = (dislike * 2);
                let total_points = (views_points + likes_points) - (dislikes_points);
                $(".views_points").html(views_points);
                $(".likes_points").html(likes_points);
                $(".dislike_points").html("-" + dislikes_points);
                $(".total_points").html(total_points);
                sessionStorage.setItem('total_pts', total_points);

            };

            function get_paid_history() {
                $.ajax({
                    type: "POST",
                    url: "php/earning.php",
                    data: {
                        for: "paid_history"
                    },
                    beforeSend: () => {},
                    success: (result) => {
                        let paid_pts = 0;
                        let payment_hist_box = $(".payment_history");
                        if (result.status == "nodata") {
                            $(".ramin_pts_history").html('<div class="alert alert-warning text-center"> No transaction found </div>');
                            $(payment_hist_box).html('<div class="alert alert-warning text-center"> No transaction found </div>');
                        } else if (result.status == "success") {

                            let his_box = $(".ramin_pts_history");

                            let history_data = result.data;
                            for (let i = 0; i < history_data.length; i++) {
                                let points = Number(history_data[i].points);
                                paid_pts += points;
                                let date_time = history_data[i].date_time;
                                let method = history_data[i].method;
                                let account_id = history_data[i].account_id;
                                let status = history_data[i].status;
                                $(his_box).append('<tr><td>Withdrawl</td><td> ' + date_time + ' </td><td> - ' + points + ' Points</td></tr>');
                                if (status == "success") {
                                    $(payment_hist_box).append('<tr><td> ' + points + ' </td><td> ' + date_time + ' </td><td> ' + method + ' </td><td> ' + account_id + ' </td><td class="text-success">Success</td></tr>');
                                } else {
                                    $(payment_hist_box).append('<tr><td> ' + points + ' </td><td> ' + date_time + ' </td><td> ' + method + ' </td><td> ' + account_id + ' </td><td class="text-danger">Success</td></tr>');
                                }
                            }

                        }
                        sessionStorage.setItem('paid_pts', paid_pts);
                        get_processing_payment();
                    }
                })
            };

            function get_processing_payment() {
                $.ajax({
                    type: "POST",
                    url: "php/earning.php",
                    data: {
                        for: "get_process_payment"
                    },
                    beforeSend: () => {},
                    success: (result) => {
                        let process_pts = 0;
                        let process_box = $(".process_payment_box");
                        $(".process_payment_box").html("");
                        if (result.status == "nodata") {
                            process_pts = 0;
                            $(process_box).html('<div class="alert alert-warning text-center">  No processing payment  !</div>');
                        } else if (result.status == "success") {
                            let process_data = result.data;
                            for (let i = 0; i < process_data.length; i++) {
                                let points = Number(process_data[i].points);
                                let date_time = process_data[i].date_time;
                                let method = process_data[i].method;
                                let status = process_data[i].status;
                                let account_id = process_data[i].account_id;

                                if (status == "pending") {
                                    $(process_box).append('<tr><td> ' + points + '</td><td>  ' + date_time + ' </td><td> ' + method + ' </td><th> ' + account_id + ' </th><td class="text-warning"> ' + status + ' </td></tr>');
                                    process_pts += points;
                                } else {
                                    $(process_box).append('<tr><td> ' + points + '</td><td>  ' + date_time + ' </td><td> ' + method + ' </td><th> ' + account_id + ' </th><td class="text-danger"> ' + status + ' </td></tr>');
                                }

                            }
                        }
                        sessionStorage.setItem("process_pts", process_pts);
                        var avl_pts = sessionStorage.getItem("total_pts") - sessionStorage.getItem("paid_pts") - sessionStorage.getItem("process_pts");
                        withdrwal_pts(avl_pts);
                        $(".remain_points").html(avl_pts);
                    }
                })
            };


            function withdrwal_pts(remian_pts) {
                // alert(remian_pts);
                if (remian_pts >= 1000) {
                    $(".no_avl_points").addClass("d-none");
                    $(".withdraw").removeClass("d-none");
                    $(".withdral_btn").click(() => {
                        let account_id = $("input[name='paypal_email']").val();
                        let amount = $("input[name='amount']").val();
                        let method = $("select[name='method']").val();
                        if(account_id.length!==0){
                            if (amount <= remian_pts && amount >= 1000 && account_id) {
                            swal({
                                title: "Are you sure?",
                                text: "Before you confirm kindly recheck your account id",
                                icon: "warning",
                                buttons: [
                                    'Cancel',
                                    'Confrim'
                                ],
                                dangerMode: false,
                            }).then(function(isConfirm) {
                                if (isConfirm) {
                                    $.ajax({
                                        type: "POST",
                                        url: "php/earning.php",
                                        data: {
                                            for: "payout",
                                            amount: amount,
                                            account_id: account_id,
                                            method: method
                                        },
                                        beforeSend: () => {},
                                        success: (result) => {
                                            if (result.status == "success") {
                                                swal("success", "your payment in progress , your money will be credit in 24 in your account , kindly check your payment process", "success");
                                                setTimeout(() => {
                                                    window.location.reload();
                                                }, 1000);
                                            } else {
                                                swal("error", "somthing went wrong , please try after sometimes", "error");
                                                setTimeout(() => {
                                                    window.location.reload();
                                                }, 1000);
                                            }
                                            get_view_data();
                                            get_paid_history();
                                            get_processing_payment();
                                        }
                                    })
                                }
                            })
                        } else {
                            swal("warning", "Enter points between minimum 1000 points and your available points", "warning");
                        }
                        }else{
                          swal("warning","Account id cannot be empty","warning");  
                        }
                    });
                } else {
                    $(".no_avl_points").removeClass("d-none");
                    $(".withdraw").addClass("d-none");
                }
            };


        });
    </script>

    <!-- on page script -->

</body>

</html>