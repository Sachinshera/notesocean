<?php
require_once("../db.php");
require("../check_valid_user.php");
if (isset($_SESSION["redirect_to"])) {
    $redirect_url = $_SESSION["redirect_to"];
    session_destroy();
    header("Location:" .$redirect_url . "");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Dashboard : <?php echo $_SESSION["fullname"]; ?></title>
    <link rel="icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="16x16">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png" sizes="48x48">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $home_page; ?>/assest/img/title_icon.png">
    <link rel="shortcut icon" href="<?php echo $home_page; ?>/assest/img/title_icon.png">

    <script>
        var url = window.location.origin + window.location.pathname;
    </script>
    <?php 
    include("includes/css.php"); ?>

    <style>
.collection_header{
color: #000;
font-weight: bold;
}
.collection_btn{
background-color: #FF0000;
border: none;
padding: 10px 30px;
border-radius: 5px;
color: #fff;
font-size: 18px;
font-weight: bold;
}
.collection_table{
margin-top: 50px;
border-radius: 15px;
/* box-shadow:1px 1px 3px 3px #ccc; */
padding-bottom: 20px;
}
table thead tr th{
color: #333;
}


.cell-1 {
border-collapse: separate;
border-spacing: 0 4em;
background: #ffffff;
border-bottom: 5px solid transparent;
background-clip: padding-box;
cursor: pointer
}
thead {
background: #dddcdc;
}
.table-elipse {
cursor: pointer;
}
#demo {
-webkit-transition: all 0.3s ease-in-out;
-moz-transition: all 0.3s ease-in-out;
-o-transition: all 0.3s 0.1s ease-in-out;
transition: all 0.3s ease-in-out
}
.row-child {
background-color: #f8f8f8;
color: #000;
}
</style>
</head>

<body class="sidebar-light">
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php include("includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- aside start  -->
        <?php include("includes/aside.php");  ?>
        <!-- aside end  -->

        <!-- Main Container Start -->
<main class="main--container">
    <!-- Main Content Start -->
    <section class="main--content bg-white">
        <div class="container pt-3">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="collection_header">Your Collections </h1>
                </div>
                <div class="col-md-3">
                    <button class="collection_btn ml-auto"> New Collection</button>
                </div>
            </div>
        </div>
        
        
        <div class=" mx-2 collection_table mt-3">
            <div class="d-flex justify-content-center row">
                <div class="col-md-12">
                    <div class="rounded">
                        <div class="table-responsive table-borderless">
                            <table class="table mt-4">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Collections</th>
                                        <th scope="col">Visibility</th>
                                        <th scope="col">Notes count</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="table-body">
                                    <tr class="cell-1" data-toggle="collapse" data-target="#demo">
                                        <td scope="row">1</td>
                                        <td>
                                            <img src="../../assest/img/logo2.svg" height="15px" width="15px">
                                            &nbsp; This is a dummy title
                                        </td>
                                        <td><i class="fa fa-eye" aria-hidden="true"></i></td>
                                        <td>150</td>
                                        <td>
                                            <a class="edit" data-toggle="tooltip"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                            <a class="delete" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <form>
                                                        <div class="form-group p-3">
                                                            <label>Collection Name (reuired)</label>
                                                            <input type="text" class="form-control" >
                                                        </div>
                                                        <div class="form-group pl-3 pr-3">
                                                            <label for="exampleFormControlSelect1">Visibility</label>
                                                            <select class="form-control" id="exampleFormControlSelect1">
                                                                <option>Public</option>
                                                                <option>Private</option>
                                                            </select>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-danger">Save changes</button>
                                                        </div>
                                                        
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    <tr id="demo" class="collapse cell-1 row-child">
                                        <td class="text-center" colspan="1"><i class="fa fa-angle-up"></i></td>
                                        <td colspan="2">Demo</td>
                                        <td colspan="2">Demo Content</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </section>
    <!-- Main Content End -->
</main>
        <!-- Main Container End -->

        
    </div>
    <!-- Wrapper End -->


    
    

    <div class="context_blur context_hide"> </div>

   
    <?php include("includes/js.php"); ?>

</body>

</html>