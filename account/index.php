<?php
	require('setup.php');
    require('../includes/db.php');
    require("auto_login.php");
    if(isset($_GET["redirect_to"])){
     $redirect_url = $_GET["redirect_to"];
     session_start();
     $_SESSION["redirect_to"] = $redirect_url;
    }
?>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- fav icon -->
    <!-- Favicon and touch icons  -->
    <link href="../assest/img/title_icon.png" rel="apple-touch-icon-precomposed" sizes="48x48">
    <link href="../assest/img/title_icon.png" rel="apple-touch-icon-precomposed">
    <link href="../assest/img/title_icon.png" rel="shortcut icon">
    <meta name="description" content="Login into your account with Google and Facebook and upload your unlimited noted and share with your friends">
    <meta name="keyword" content="login, login notes ocean,account notesocean, signup notesocean, login,signup notesocean.com,login notesocean.com">

    <!-- Bootstrap  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title> Signup/Login </title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-37ZHXRTVW8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag("js", new Date());

  gtag("config", "G-37ZHXRTVW8");
</script>

    <style>
    
    ::-webkit-scrollbar{
        width: 0%;
    }

    @media (max-width:768px){
        #side_banner{
            display: none !important;
        }
        #login_box{
            padding:0%  4% !important;
        }
        #login_box .container{
            height:100vh !important;
            padding:0% !important;
        }
    }
    </style>
</head>

<body>

<?php include("../includes/header.php") ?>
    <div class="row mt-5">
        <div class="col-md-8">
            <img src="../assest/login_page_bg.svg" alt="notesocean.com" width="100%" id="side_banner">
        </div>
        <div class="col-md-4" style="padding:2% 10% 5% 0%" id="login_box"> 
            <div class="container" style="box-shadow:0px 0px 8px -1px #ccc;height:90vh">
                <div class="logo_cont">
                    <img src="../assest/img/nav_logo.png" alt=" notesocean logo" style="width:100px; margin:10% 10% 10% 30%">
                </div>
                <div class="heading-text text-center mb-5">
                    <h3 style="font-weight:700;font-size: 30px;"> <span style="color:red"> Signup or login  </span> <br> <span>Account</span> </h3>
                    <span class="text-danger"> You don't need to fill up form to signup , just click on continue with google and your will be signup  and  login  automatically  </span>
                </div>

                <div class="account_buttons mb-4">
                    <div onclick="window.location = '<?php echo $google->createAuthUrl(); ?>'" class="div google-button py-1 my-5" style="box-shadow:0px 0px 8px -1px #ccc;padding:10px;border-radius:30px;margin:10px 10px;cursor:pointer">
                        <div class="row px-4">

                            <div class="col-2 px-1">
                                <img src="../assest/img/google.jpeg" alt="" width="80%" class="ml-1">
                            </div>
                            <div class="col-10 pt-1" >
                                <a href="<?php echo $google->createAuthUrl();?>"> <h6> Continue with Google </h6> <a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="div facebook-button py-1" style="box-shadow:0px 0px 8px -1px #ccc;padding:10px;border-radius:30px;margin:10px 10px;cursor:pointer">
                        <div class="row px-4" id="fb_login_box">
                            <div class="col-2 px-1">
                                <img src="images/facebook logo.jpg" alt="" width="100%" class="ml-1">
                            </div>
                            <div class="col-10 pt-1">
                            <h6>Continue with Facebook</h6>
                            </div>
                        </div>
                    </div> -->

                </div>

                <div class="contents-text text-center">
                <p class="privacy-text">
                By logging in, you agree to our  <br> <a href="<?php echo $home_page; ?>/terms-and-conditions"> terms and conditions</a> and <a href="<?php echo $home_page; ?>/privacy-policies">privacy policies</a>
                </p>
<br> 
                <p class="why-we-text">
                Why we don't use custom login form <br> <a href="<?php echo $home_page; ?>/privacy-policies#why-not-custom-login"> know more </a>
                </p>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>
<script>
   $(document).ready(function() {
    $("#fb_login_box").click(function() {
        facebook_login();
    });
    var app_id = "476612776712457";
    var app_version = "v10.0";
   function facebook_login(){
    FB.login(function(response){
        checkLoginState();
});
   }
  function statusChangeCallback(response) {  
    if (response.status === 'connected') { 
      var access_token = response.authResponse.accessToken;
        FB.api('/me',{"fields":"id,name,email,picture"},function(response) {
            var user_name = response.name;
            var email = response.email;
            var profile_pic = response.picture.data.url;
            // send data to backend
             
          $.ajax({
              type:"POST",
              url:"http://localhost/notesocean/login/fb_login",
              data:{
                fb:"fb",
                name:user_name,
                email:email,
                picture:profile_pic
              },
              beforeSend:function(){},
              success:function(data){
              var json = JSON.parse(data);
// console.log(data);
//                 console.log(json);
                if(json.status == "success"){
                    let login = json.url;
                    window.location = login;
                }
               else{
                   console.log(data);
               }
              }
          })
          

    });  
    } else {                                 
        console.log("account not connected");
    }
  }


  function checkLoginState() {              
    FB.getLoginStatus(function(response) {  
      statusChangeCallback(response);
    });
  }


  window.fbAsyncInit = function() {
    FB.init({
      appId      : app_id,
      cookie     : true,                    
      xfbml      : true,                   
      version    : app_version          
    });
  };


   });

</script>
    <?php  include("../includes/footer.php") ?>
</body>

</html>