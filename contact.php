<?php require_once("includes/db.php"); ?>
<!DOCTYPE html>
<html lang=" en-US" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="utf-8">
    <title>Contact us - Notesocean</title>
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <meta name="keyword" content="basic computer notes for students,ms word notes pdf,note to pdf,lecture notes in,,mechanical engineering,engineering mechanics notes,lecture notes in electrical engineering,total quality management notes,basic electrical engineering notes,handwritten notes to text,highway engineering notes,made easy handwritten notes,irrigation engineering notes,business management notes,lacture notes,handwritten notes,pdf notes,engineering notes,management notes,notesocean, notes ocean,notes ocean bba notes">
    <meta name="robots" content="index,follow">
    <!-- og start  -->
    <meta property="og:site_name" content="Notes Ocean" />
    <meta property="og:title" content="Contact us - Notes Ocean " />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://notesocean.com/contact-us" />
    <meta name="og:description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- meta end  -->
    <?php include("includes/cdn.php"); ?>

    <!-- styles start here -->
    <style type="text/css">
        .mt-100{
            margin-top: 100px;
        }
        .mb-100{
            margin-bottom: 100px;
        }
        .banner_main{
            height: 50vh;
            background-image:linear-gradient(rgba(0,0,0,0.5),
                rgba(0,0,0,0.5)), url('assest/img/contact-us-banner.png');
            background-size: cover;
            display:flex;
            justify-content: center;
            align-items: center;

        }
        .banner_child h1{
            color: #fff;
            font-weight: 900;
            font-size: 4rem;
        }
        .contact h2{
            font-size: 2.5rem;
            font-weight: bold;
        }
        .contact p{
            font-size: 1.4rem;
            text-align: justify;
        }
        .address{
            font-size: 1.4rem;
        }
        .icon{
            margin-right: 20px;
            color: red;
        }
        div.column2{
        color: #222;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
       padding: 20px 40px;
        background-color: #fff;
        border-radius: 2px;
    }
    .column2 h2{
        font-size: 28px;
        margin-bottom: 30px;
        padding-top: 30px;

    }
    .column2 input{
        border:2px solid red;
        border-radius: 5px;
        width: 100%;
        height: 50px;
        margin-bottom: 20px;
    }
    .column2 textarea{
        border:2px solid red;
        border-radius: 5px;
        height: 150px;
        width: 100%;
    }
    .button_submit{
        color: #fff;
        background-color: red;
        border-radius: 5px;
        width:100%;
        margin: 30px 0;
        padding: 10px 0;
        text-transform: uppercase;
        font-size: 20px;
        border: none;
}
::-webkit-input-placeholder { /* WebKit browsers */
    padding-left: 10px;
}
    </style>
}
</head>

<body>
    <!-- Header -->

    <?php include("includes/header.php") ?>
   <!-- header end  -->

   <!-- content start here -->

<div class="banner_main">
    <div class="banner_child">
        <h1> Contact Us</h1>
    </div>
</div>

<div class="contact mt-100 mb-100">
    <div class="container">
        <div class="row">
        <div class="col-md-6 p-4">
            <h2>GET IN TOUCH</h2>
            <p>Notesocean is the best destination for students to find perfect notes for every subject and courses. we provide students with perfect study materials for their education, unlimated data storage, platform to earn money.</p>
            <div class="address mt-5 pt-3">
                <i class="fa fa-envelope icon" aria-hidden="true"></i> contact@notesocean.com<br>
                <i class="fa fa-envelope icon" aria-hidden="true"></i> admin@notesocean.com <br>
                <i class="fa fa-phone icon" aria-hidden="true"></i> +918167226367 <br>
                <i class="fa fa-map-marker icon" aria-hidden="true"></i> Bihar, India
            </div>
        </div>
        <div class="col-md-6 column2">
            <h2>Say Something!</h2>
            <form action="contact_mail.php" method="post">
                <input type="text" name="name" placeholder="Your Name..." ><br>
                <input type="text" name="email" placeholder="Your Email..." >
                <input type="text" name="phone" placeholder="Your Phone Number...">
                 <textarea name="msg" placeholder="Your Message..."></textarea>
                 <button class="button_submit" name="submit">send</button>
            </form>
        </div>
        
    </div>
        
    </div>
</div>




   <!-- content end  here -->

<!-- footer start here -->
    <?php include("includes/footer.php"); ?>
</body>

</html>