<header>

    <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
        <a class="navbar-brand" href="<?php echo $home_page; ?>">
            <img src="<?php echo $home_page; ?>/assest/img/nav_logo.png" alt="notesocean logo " width="60"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto nav_list">
                <li class="nav-item ">
                    <a class="nav-link" target="_self" href="https://notesocean.com">Home</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" target="_self" href="https://notesocean.com/about/docs/how-to-earn-money-by-uploading-your-notes/">Earn Money</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" target="_self" href=" https://notesocean.com/about/docs/how-to-upload-notes/">Upload notes</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link"  target="_self" href="https://notesocean.com/about-us">About us</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link"  target="_self" href="https://notesocean.com/contact-us">Contact us</a>
                </li>

                
            </ul>


            <div>


<?php
if (!empty($_COOKIE["url"])) {
    $cookie = $_COOKIE["url"];
    $today = date("d-m-Y h:i:sa");
    $get_login_info = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$cookie'");
    $login_info = $get_login_info->fetch_assoc();
    $userid = $login_info["userid"];
    $get_user_email = $user_db->query("SELECT * FROM profiles WHERE user_id = '$userid'");
    $user_data = $get_user_email->fetch_assoc();
    $user_email = $user_data["email"];
    $fullname = $user_data["fullname"];
    $picture = $user_data["picture"];
    echo '<a class="nav-link login_btn" href="' . $home_page . '/account/dashboard?user_id=' . $userid . '"> <img width="25px" src="' . $picture . '" style="border-radius:50%" alt="user-img"> </img> Dashboard</a>';
} else {
    echo '<a class="nav-link login_btn" href="' . $home_page . '/account">Login</a>';
}


?>

</div>



        </div>

       
    </nav>
</header>
<style>
    .login_btn {
        background-color: #EB0000;
        border-radius: 5px;
        color: white;
    }

    .nav_list li a {
        color: #323232 !important;
        margin: 0px 10px;
        font-weight: 500;
        text-transform: uppercase;
        font-size:16px;

    }
    nav{
        border-bottom: 1px solid #ccc;
    }
</style>