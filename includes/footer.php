<style>
    /*footer*/
    .col_white_amrc {
        color: #FFF;
    }

    footer {
        width: 100%;
        background-color: #263238;
        min-height: 250px;
        padding: 10px 0px 25px 0px;
    }

    .pt2 {
        padding-top: 40px;
        margin-bottom: 20px;
    }

    footer p {
        font-size: 13px;
        color: #CCC;
        padding-bottom: 0px;
        margin-bottom: 8px;
    }

    .mb10 {
        padding-bottom: 15px;
    }

    .footer_ul_amrc {
        margin: 0px;
        list-style-type: none;
        font-size: 14px;
        padding: 0px 0px 10px 0px;
    }

    .footer_ul_amrc li {
        padding: 0px 0px 5px 0px;
    }

    .footer_ul_amrc li a {
        color: #CCC;
    }

    .footer_ul_amrc li a:hover {
        color: #fff;
        text-decoration: none;
    }

    .fleft {
        float: left;
    }

    .padding-right {
        padding-right: 10px;
    }

    .footer_ul2_amrc {
        margin: 0px;
        list-style-type: none;
        padding: 0px;
    }

    .footer_ul2_amrc li p {
        display: table;
    }

    .footer_ul2_amrc li a:hover {
        text-decoration: none;
    }

    .footer_ul2_amrc li i {
        margin-top: 5px;
    }

    .bottom_border {
        border-bottom: 1px solid #323f45;
        padding-bottom: 20px;
    }

    .foote_bottom_ul_amrc {
        list-style-type: none;
        padding: 0px;
        display: table;
        margin-top: 10px;
        margin-right: auto;
        margin-bottom: 10px;
        margin-left: auto;
    }

    .foote_bottom_ul_amrc li {
        display: inline;
    }

    .foote_bottom_ul_amrc li a {
        color: #999;
        margin: 0 12px;
    }

    .social_footer_ul {
        display: table;
        margin: 15px auto 0 auto;
        list-style-type: none;
    }

    .social_footer_ul li {
        padding-left: 20px;
        padding-top: 10px;
        float: left;
    }

    .social_footer_ul li a {
        color: #CCC;
        border: 1px solid #CCC;
        padding: 8px;
        border-radius: 50%;
    }

    .social_footer_ul li i {
        width: 20px;
        height: 20px;
        text-align: center;
    }
</style>
<footer class="footer">
    <div class="container bottom_border">
        <div class="row">
            <div class=" col-sm-4 col-md col-sm-4  col-12 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Find us</h5>
                <!--headin5_amrc-->
                <div><img src="<?php  echo $home_page; ?>/assest/img/logo.png" width="50" alt="notesocean logo"></div>
                <p class="mb10">Notes Ocean finds perfect notes for you.</p>
                <p><i class="fa fa-location-arrow"></i> Bihar, India </p>
                <p><i class="fa fa-phone"></i> +91-8210071758 </p>
                <p><i class="fa fa fa-envelope"></i> contact@notesocean.com </p>
                <p><i class="fa fa fa-envelope"></i> admin@notesocean.com </p>
                <p><i class="fa fa fa-clock"></i> 24 hours Available </p>


            </div>


            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Information</h5>
                <!--headin5_amrc-->
                <ul class="footer_ul_amrc">
                <li><a href="<?php  echo $home_page; ?>/privacy-policies">Privacy Policy</a></li>
                    <li><a href="<?php  echo $home_page; ?>/terms-and-condition">Terms & Conditions</a></li>
                    <li><a href="<?php  echo $home_page; ?>/about-us">About Us</a></li> 
                    <li><a href="<?php  echo $home_page; ?>/contact-us">Contact Us</a></li>
                    <!-- <li><a href="<?php  echo $home_page; ?>/blog">New Updates</a></li>
                    <li><a href="<?php  echo $home_page; ?>/blog">Latest News</a></li>
                    <li><a href="<?php  echo $home_page; ?>/account">My Account Info</a></li> -->
                </ul>
                <!--footer_ul_amrc ends here-->
            </div>


            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Our Services</h5>
                <!--headin5_amrc-->
                <ul class="footer_ul_amrc">
                    
                     <li><a href="<?php  echo $home_page; ?>">Unlimited storage </a></li>
                    <li><a href="<?php  echo $home_page; ?>">Earn Money </a></li>
                    <li><a href="<?php  echo $home_page; ?>">Share Docuemnts </a></li>
                </ul>
                <!--footer_ul_amrc ends here-->
            </div>



        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="foote_bottom_ul_amrc">
                    <li><a href="https://notesocean.com">Home</a></li>
                    <li><a href="https://notesocean.com/account">Login</a></li>
                    <li><a href="https://notesocean.com/about/docs/how-to-upload-notes/">How to upload notes</a></li>
                    <li><a href="https://notesocean.com/about/docs/how-to-earn-money-by-uploading-your-notes/">How To Earn Money</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="social_footer_ul">
                    <li><a href="https://www.facebook.com/notesoceanofficial/"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://www.twitter.com/notesoceanofficial/"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://www.linkedin.com/notesoceanofficial/"><i class="fab fa-linkedin"></i></a></li>
                    <li><a href="https://www.instagram.com/notesoceanofficial/"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>

        </div>

        <!--foote_bottom_ul_amrc ends here-->
        <p class="text-center"><a href="https://notesocean.com" class="text-center" style="text-decoration: none;color:red">Notes Ocean</a> @ {2021} By Notes Ocean. | All Rights Reserved.</p>
        

        <!--social_footer_ul ends here-->
    </div>

</footer>

