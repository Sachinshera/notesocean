<?php
require("includes/db.php");
$title = "";
$homepage_url = $_SERVER["REQUEST_URI"];
if (isset($_GET["q"])) {
    if (($_GET["q"]) !== "") {
        $page = 0;
        if (isset($_GET["page"])) {
            $page = $_GET["page"];
        }
        $count = 20;
        $page = $count * $page;
        $query = mysqli_real_escape_string($db, strip_tags(trim($_GET["q"])));
        $title = $query;
        $srch =  show_result($db, $query, $page, $count);
    } else {
        header("Location:https://www.notesocean.com");
    }
} else {
    header("Location:https://www.notesocean.com");
}

function show_result($db, $query, $page, $count)
{
    $queryCondition = "";
    $wordsAry = explode(" ", $query);
    $wordsCount = count($wordsAry);
    $queryCondition = " WHERE ";
    $ranKqueryCondition = " WHERE ";
    for ($i = 0; $i < $wordsCount; $i++) {
        $queryCondition .= "product_name LIKE '%" . $wordsAry[$i] . "%' OR short_desc LIKE '%" . $wordsAry[$i] . "%' OR keyword LIKE '%" . $wordsAry[$i] . "%'";
        if ($i != $wordsCount - 1) {
            $queryCondition .= " OR ";
        }
    }
    // exit;
    $srch = $db->query("SELECT *,COUNT(*) OVER () AS total_count FROM products $queryCondition   AND status = 'active'  ORDER BY product_name ASC LIMIT $page,$count");

    if ($srch) {
        return $srch;
    }
};
include("includes/clean.php");

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">


<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php echo $title; ?></title>
    <?php echo $ads_script; ?>
    <?php include("includes/cdn.php"); ?>
    <style>
        .jumbotron {
            background-color: white;
            padding: 0px 0px;
        }

        .srch_item {
            box-shadow: -2px 0px 0px 0px red;
            padding: 10px 20px;
            margin: 10px 0px;
        }

        .srch_item h5 {
            color: black;
            font-weight: bold;
            font-size: 20px;
            color: red;
        }

        .srch_item p {
            color: black;
            font-size: 12px;
        }

        .srch_item .item_text {
            padding: 10px 10px;
        }

        .item_cont {
            /* padding: 0% 20%; */
            width: 100%;
            margin: 0 auto;
        }

        .item_cont img {
            display: block;
        }

        .jumbotron>h2 {
            padding: 10px 10%;
        }

        a:hover {
            text-decoration: none;
        }

        form {
            width: 50%;
            margin: 0 auto;
            margin-top: 30px;
        }

        .pagination li a {
            font-weight: bold;
            color: red;
        }

        @media only screen and (max-width:768px) {
            .item_cont {
                padding: 10px 0%;
                width: 100%;
            }

            .item_cont img {
                display: none;
            }
            .srch_item img{
                display: block;
            }

            .jumbotron>h2 {
                padding: 10px 0%;
            }

            form {
                width: 90%;
                margin: 10px auto;
                margin-top: 10px;
            }

            .page_link {
                display: block;
            }

        }
    </style>
</head>

<body>
    <?php include("includes/header.php"); ?>
    <section id="serach_result" style="margin-top:70px">
        <form method="get" action="<?php echo $home_page . "/search"; ?>">
            <input class="btn_note form-control" type="search" placeholder="Search your notes" name="q" value="<?php echo $title; ?>">
        </form>
        <div class="jumbotron">
            <div class="row item_cont">
                <div class="col-md-6 mx-auto">
                    <?php
                    // heilight text 
                    function highlightKeywords($text, $keyword)
                    {
                        $wordsAry = explode(" ", $keyword);
                        $wordsCount = count($wordsAry);

                        for ($i = 0; $i < $wordsCount; $i++) {
                            $highlighted_text = "<span style='font-weight:bold;color:red'>$wordsAry[$i]</span>";
                            $text = str_ireplace($wordsAry[$i], $highlighted_text, $text);
                        }

                        return $text;
                    }

                    // heilight text end 
                    $counter = 0;
                    $total_page = $counter;
                    if ($srch->num_rows !== 0) {
                        while ($data = $srch->fetch_assoc()) {
                            $counter  = $counter + 1;
                            $pro_title = $data["product_name"];
                            $pro_title_text = $data["product_name"];
                            $short_desc = $data["short_desc"];
                            $long_desc = $data["long_desc"];
                            $notes_id = $data["id"];

                            // $image_url = $data["image_url"];
                            // $alias = $data["alias"];
                            $course = $data["course"];
                            $sems = $data["sems"];
                            $subject = $data["sub"];
                            $keyword = $data["keyword"];
                            $total_count = $data['total_count'];

                            // heilight text 

                            $pro_title_text = highlightKeywords(substr($pro_title_text, 0, 100), $query);
                            $short_desc = highlightKeywords(substr($short_desc, 0, 200), $query);

                            // end 


                            // get user info 
                            $notes_by = $data["notes_by"];
                            if ($notes_by == 0) {
                                $fullname = "notesocean";
                                $picture = $home_page."/assest/img/nav_logo.png";
                            } else {
                                $get_user_info = $user_db->query("SELECT fullname,picture FROM profiles WHERE user_id = '$notes_by'");
                                if ($get_user_info) {
                                    $user_data = $get_user_info->fetch_assoc();
                                    $fullname = $user_data["fullname"];
                                    $picture = $user_data["picture"];
                                }
                            }
                            // get user info end 

                            echo '
                                <div class="row srch_item ">
                                <div class="col-md-12 item_text">
                                <a href="' . $home_page . '/' . $notes_id . '/' . just_clean($pro_title) . '.html">
                            
                       
                 <h6> ' . str_replace("&nbsp;","",htmlspecialchars(strip_tags(trim($pro_title_text)))) . ' </h6>
               
                 
                 <p>' . str_replace("&nbsp;","",htmlspecialchars(strip_tags(trim($short_desc)))) . '...</p>
               
                 </a> 

                 <div class="btn-group " style="box-shadow:0px 0px 1px #ccc">
                 <button class="btn btn-sm">  Published By :  </button>
                 <button class="btn btn-sm"> <img src="'.$picture.'" style="width:25px;border-radius:50%"> </button>
                 <button class="btn btn-sm"> '.$fullname.' </button>
                 </div>
                     </div>
                    
                    
                    
                    </div>
                    

                            ';
                        };
                        $total_page = round($total_count / $count);
                    } else {
                        echo '<div class="alert alert-danger text-center"> No Notes found try another keyword </div>';
                    }
                    ?>

                </div>
                <?php
                if ($total_page !== 0) {
                    echo ' <div class="pagination_box container" style="width:100%;box-shadow:0px 0px 5px #ccc">
<ul class="pagination d-flex justify-content-around pt-3">';
                    $prev_page = ($page / $count) - 1;
                    if ($prev_page !== -1) {
                        echo ' <li class="page-item px-2"><a class="page-link" href="' . $home_page . '/search?q=' . $title . '&page=' . $prev_page . '"> <i class="fa fa-arrow-left"> </i> Previous</a></li>';
                    }

                    // for($i = 0; $i <$total_page;$i++){
                    //     echo '<li class="page-item page_link"><a href="'.$home_page.'/search?q='.$title.'&page='.$i.'"> '.$i.' </a></li>';
                    // }
                    $current_paage =  $page / $count + 1;
                    if ($current_paage < $total_page) {
                        echo ' <li class="page-item px-2"><a class="page-link" href="' . $home_page . '/search?q=' . $title . '&page=' . $current_paage . '"> <i class="fa fa-arrow-right"> </i> Next</a></li>';
                    } else {
                        // echo $total_page;
                        // echo ' <li class="page-item px-2"><a class="page-link" href="'.$home_page.'/search?q='.$title.'&page='.$current_paage.'"> <i class="fa fa-arrow-right"> </i> Next</a></li>';
                    }
                    echo '
   
</ul>
</div>';
                }

                ?>

            </div>
        </div>

    </section>
    <!-- ext js  -->
</body>

</html>