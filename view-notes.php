<?php
require("includes/db.php");
include("includes/clean.php");
function httpPost($url, $data)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

if (isset($_GET["id"])) {
    $notes_id = $_GET["id"];
    if ($home_page == "http://localhost/notesocean") {
        $api_url = "https://test-api.notesocean.com/v2/";
    } else {
        $api_url = "https://api.notesocean.com/v2/";
    }
    $api_key = "1234";
    $today = date("i:s");
    $today = explode(":", $today);
    $minute = $today["0"];
    $second = $today["1"];
} else {
    echo "error notes id";
}
function update_views($db, $notes_id, $user_id)
{
    session_start();
    if (!isset($_SESSION[$notes_id])) {
        $client_ip = "0";
        $today = date("d-m-Y h:i:sa");
        $check_views = $db->query("SELECT * FROM  views WHERE notes_id = '$notes_id' AND ip_address = '$client_ip'");
        if ($check_views->num_rows == 0) {
            $sql = $db->query("INSERT INTO views(notes_id,user_id,ip_address,date_time) VALUES('$notes_id','$user_id','$client_ip','$today')");
            if ($sql) {
                $_SESSION[$notes_id] = $notes_id;
            }
        }
    }
}

function check_login($user_db)
{
    if (isset($_COOKIE["url"])) {
        if ($_COOKIE["url"] != "") {
            // get user id 
            $cookiekey = $_COOKIE["url"];
            $get_user_id = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$cookiekey' ");
            if ($get_user_id->num_rows !== 0) {
                $userid = $get_user_id->fetch_assoc();
                $userid = $userid["userid"];
            } else {
                $userid = null;
            }
        } else {
            $userid = null;
        }
    } else {
        $userid = null;
    }

    return $userid;
};
$user_id = check_login($user_db);
if ($user_id !== null) {
    update_views($db, $notes_id, $user_id);
} else {
    $user_id = 0;
    update_views($db, $notes_id, $user_id);
}
function current_page_url()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $CurPageURL = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    return  $CurPageURL;
}

// get notes data start
if ($user_id !== null) {
    $user_id = $user_id;
} else {
    $user_id = 0;
}
$final_api_key = base64_encode(base64_encode($minute . "-" . $api_key . "-" . $second));
$url = $api_url . $final_api_key . "/bm90ZXNfZGF0YQ==";
$data = array("user_id" => $user_id, "notes_id" => $notes_id);
$response = json_decode(httpPost($url, $data), true);
if ($response["status"] == "success") {
    $data = $response["data"];
    $notes_data = $data[0];
    $title = $notes_data["title"];
    $short_desc = $notes_data["short_description"];
    $before_date = $notes_data["before_date"];
    $pdf_url = $notes_data["file_url"];
    $pdf_url = str_replace("https://s3.ap-south-1.amazonaws.com/www.notesocean.com", "https://pdf.notesocean.com", $pdf_url);
    $uploaded_date = $notes_data["uploaded_date"];
    $updated_date = $notes_data["updated_date"];
    $notes_by = $notes_data["notes_by"];
    $keyword = $notes_data["tags"];
    $views = $notes_data["views"];
    $react_data = $notes_data["react"];
    $profile_data = $notes_data["profile"];

    // react data fetch 

    if ($react_data["status"] == "success") {
        $total_likes = $react_data["like"];
        $total_dislike = $react_data["dislike"];
        $user_react = $react_data["user_react"];
    } else {
        $total_likes = 0;
        $total_dislike = 0;
        $user_react = 0;
    }
} else {
    echo "failed to load notes data";
}

$clean_url = just_clean($title);
$clean_pdf_url = $clean_url . ".pdf";
$title = str_replace("-", " ", $title);

?>
<!DOCTYPE>
<html lang="en-US" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php echo $title; ?> </title>

    <meta name="description" content="<?php echo $short_desc; ?>">
    <meta name="keyword" content="<?php echo $keyword; ?>">
    <meta name="robots" content="index,follow">
    <meta name="author" content="<?php echo $profile_data["fullname"]; ?>">
    <meta name="copyright" content="<?php echo $profile_data["fullname"]; ?>">
    <meta http-equiv="content-language" content="en-US">
    <link rel="canonical" href="<?php echo current_page_url(); ?>">
    <!-- og start  -->
    <meta property="og:site_name" content="Notes Ocean" />
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:type" content="application/pdf" />
    <meta name="og:description" content="<?php echo $short_desc; ?>">
    <meta name="og:author" content="<?php echo $profile_data["fullname"]; ?>">
    <meta name="og:copyright" content="<?php echo $profile_data["fullname"]; ?>">
    <meta property="og:url" content="<?php echo current_page_url(); ?>" />
    <meta property="og:pdf" content="<?php echo $home_page . "/" . $notes_id . "/" . $clean_pdf_url; ?>" />
    <!-- og end  -->
    <?php echo $ads_script; ?>
    <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
    <?php include("includes/cdn.php"); ?>
    <style>
        .related-notes-img-thumb {
            width: 100% !important;
            height: 150px !important;
            object-fit: cover;
        }

        .related-notes-row .col-md-3 {
            margin: 10px 0px;
        }

        .related-notes-row a {
            text-decoration: none;
            color: black;
        }

        .related-notes-row .card-body {
            height: 150px;
            background-position: top;
        }

        .related-notes-row .card {
            box-shadow: 0 0rem 5px rgba(0, 0, 0, .175) !important;
        }

        #adobe-dc-view {
            height: 50vh;
        }

        .notes_data h1 {
            font-size: 20px;
        }

        .uploaded-user-pic img {
            width: 35;
            height: 35px;
            border-radius: 50%;
        }

        .related-heading {
            font-size: 25px;
        }

        a {
            text-decoration: none !important;
            color: black;
        }

        a:hover {
            color: black;
        }

        @media (max-width:768px) {
            .ads-box {
                display: none !important;
            }

            .jumbotron {
                padding: 60px 0px;
            }

            .notes-title h1 {
                font-size: 15px !important;
            }

            .icon-text {
                display: none;
            }
        }
    </style>
</head>

<body>
    <?php include("includes/header.php"); ?>
    <!-- section start  -->
    <section class="jumbotron bg-white">
        <div class="row notes_data">
            <div class="col-md-9 p-0">
                <div class="card m-0">
                    <div class="card-content">
                        <div class="card-header bg-white notes-title">
                            <h1> <?php echo $title; ?> </h1>
                        </div>
                        <div class="card-body p-0">
                            <div id="adobe-dc-view"></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-12">
                                            <a href="<?php echo $home_page . "/profile" . "/" . $profile_data["user_id"]; ?>">
                                                <div class="row border-1 border-danger">
                                                    <div class="col-2">
                                                        <div class="uploaded-user-pic">
                                                            <img src="  <?php echo $profile_data["picture"]; ?>" alt="  <?php echo    $profile_data["fullname"]; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="">
                                                        <?php echo $profile_data["fullname"]; ?>
                                                    </div>
                                                </div>
                                            </a>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col">
                                            <button data-react="1" class="btn-sm btn  react_btn <?php if ($user_react == "1") {
                                                                                                    echo "btn-primary";
                                                                                                } else {
                                                                                                    echo "btn-default";
                                                                                                } ?>"> <?php echo $total_likes; ?> <i class="fa fa-thumbs-up"></i> <span class="icon-text"> Like </span> </button>
                                        </div>
                                        <div class="col">
                                            <button data-react="2" class="btn btn-sm  react_btn <?php if ($user_react == "2") {
                                                                                                    echo "btn-primary";
                                                                                                } else {
                                                                                                    echo "btn-default";
                                                                                                } ?>"> <?php echo $total_dislike; ?> <i class="fa fa-thumbs-down"></i> <span class="icon-text"> Dislike </span> </button>
                                        </div>

                                        <div class="col">
                                            <button class="btn btn-sm btn-default report_button"> <i class="fa fa-flag"></i> <span class="icon-text"> Report </span> </button>
                                        </div>

                                        <div class="col">
                                            <a href="<?php echo $home_page . "/" . $notes_id . "/" . $clean_pdf_url; ?>">
                                                <button class="btn btn-sm btn-danger"> <i class="fa fa-download"></i> <span class="icon-text"> Download </span> </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 ads-box  <?php if ($home_page == "http://localhost/notesocean") {
                                                echo "d-none";
                                            } ?>">
                <!-- display ads  -->
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3834928493837917" crossorigin="anonymous"></script>
                <!-- desktop display ads -->
                <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-3834928493837917" data-ad-slot="6085336784" data-ad-format="auto" data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
                <!-- display ads  -->
            </div>

        </div>
        <!-- get related notes start  -->
        <!-- <h2 class="related-heading">Related Notes</h2> -->

        <div class="row related-notes-row">

            <?php

            $final_api_key = base64_encode(base64_encode($minute . "-" . $api_key . "-" . $second));
            $url = $api_url . $final_api_key . "/cmVsYXRlZF9ub3Rlcw==";
            $data = array("user_id" => 0, "notes_id" => $notes_id);
            $response = json_decode(httpPost($url, $data), true);
            if ($response["status"] == "success") {
                $notes_data = $response["data"];
                for ($i = 0; $i < 30; $i++) {
                    $related_notes_id = $notes_data[$i]["notes_id"];
                    $title =  $notes_data[$i]["title"];
                    $show_title = $title;
                    $title = just_clean($title);
                    $thumbnail = $notes_data[$i]["thumbnail"];
                    if ($thumbnail !== null) {
                        $thumbnail = explode(",", $thumbnail);
                        $all_thumbs = $thumbnail;
                        $thumbnail = $thumbnail[0];
                    } else {
                        $thumbnail = $home_page . "/img/nothumb.png";
                    }
                    echo '
            <div class="col-md-3">
            <a href="' . $home_page . '/' . $related_notes_id . '/' . $title . '.html">
            <div class="card related-cards" data-thumbs="' . implode(",", $all_thumbs) . '">
                 <div class="card-content p-0">
                 <div class="card-body" style="background-image: url(' . $thumbnail . ');">

                 </div>
                 <div class="card-footer p-0 p-2 bg-white">
                     <p class="related-notes-item-title">
                        ' . $show_title . '
                     </p>
                     <button class="btn btn-sm btn-danger quick_prev_notes">Preview </button>
                 </div>
                 </div>

             </div>
            </a>
            </div>

            ';
                }
            } else {
                echo "Failed to load related notes";
            }
            ?>
            <!-- <div class="col-md-3">
                <a href="#">
                    <div class="card related-cards" data-thumbs="https://notes-ocean-thumbnails.s3.ap-south-1.amazonaws.com/0_838_5644.jpg,https://notes-ocean-thumbnails.s3.ap-south-1.amazonaws.com/1_838_5644.jpg,https://notes-ocean-thumbnails.s3.ap-south-1.amazonaws.com/2_838_5644.jpg">
                        <div class="card-content p-0">
                            <div class="card-body" style="background-image: url(https://notes-ocean-thumbnails.s3.ap-south-1.amazonaws.com/0_1247_5815.jpg);">
                            </div>s
                            <div class="card-footer p-0 p-2 bg-white">
                                <p class="related-notes-item-title">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing
                                </p>
                            </div>
                        </div>

                    </div>
                </a>
            </div> -->




        </div>
        <!-- get related notes end  -->
    </section>
    <section>
        <div class="modal fade  preview_modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content p-0">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Quick Preview </h5>
                        <button type="button btn-sm btn-primary btn" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-0">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="500">
                            <div class="carousel-inner pdf-preview-box">

                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade  report_modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content p-0">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Report Note </h5>
                    </div>
                    <div class="modal-body">
                        <form>

                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="report_type" id="report_type" value="Spam or misleading" checked>
                                    <label class="form-check-label" for="report_type">
                                        Spam or misleading
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="report_type" id="report_type" value="Sexual content">
                                    <label class="form-check-label p-1" for="report_type">
                                        Sexual content
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="report_type" id="report_type" value="Violent or repulsive content">
                                    <label class="form-check-label" for="report_type">
                                        Violent or repulsive content
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input " type="radio" name="report_type" id="report_type" value="Infringes my rights">
                                    <label class="form-check-label" for="report_type">
                                        Infringes my rights
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="desc">Description</label>
                                <textarea required name="report_description" id="report_description" cols="10" rows="5" class="form-control"></textarea>
                            </div>


                        </form>
                    </div>
                    <div class="report_notice my-2">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary submit_report_btn">Submit</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade  login_error_modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content p-0">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"> You are not logged in</h5>
                        <button type="button btn-sm btn-primary btn" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        you can't like ,dislike and report note without logged in
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a href="<?php echo $home_page."/"."account?redirect_to=".current_page_url(); ?>"><button type="button" class="btn btn-primary submit_report_btn">Login</button></a>                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- section end -->
    <script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
    <script>
        $(document).ready(function() {
            $(".related-cards").each(function() {
                var card = this;
                var btn = card.querySelector(".quick_prev_notes");
                $(btn).click(function() {
                    $(".pdf-preview-box").html("");
                    var card_urls = $(card).attr("data-thumbs");
                    var card_urls = card_urls.split(",");
                    for (let i = 0; i < card_urls.length; i++) {
                        var img_url = card_urls[i];
                        $(".pdf-preview-box").append('<div class="carousel-item"> <img class="d-block w-100" src="' + img_url + '" alt="<?php echo $title; ?>"> </div>');
                    }
                    $($(".pdf-preview-box .carousel-item")[0]).addClass("active");
                    $(".preview_modal").modal("show");
                    return false;
                });
            });
        });
        $(document).ready(function() {
            function get_path(path) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo $home_page ?>/includes/url.php",
                    data: {
                        value: path
                    },
                    success: function(data) {
                        sessionStorage.setItem("temp_path", data);
                    }
                });
            };

            function getCookie(name) {
                var exp = new RegExp('[; ]' + name + '=([^\\s;]*)');
                var matchs = (' ' + document.cookie).match(exp);
                if (matchs) return matchs[1];
                return false;
            }
            //   add react start 
         

            $(".react_btn").each(function() {
                $(this).click(function() {
                    var cookie = getCookie("url");
                    if(cookie!==false){
                        get_path("react");
                    var api_path = sessionStorage.getItem("temp_path");
                    var btn = this;
                    if ($(btn).hasClass("btn-primary")) {
                        var reacted = true;
                    } else {
                        var reacted = false;
                    }
                    var reaction = $(this).attr("data-react");
                    if (reacted) {
                        var react = 0;
                    } else {
                        var react = reaction;
                    }
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $api_url ?>/" + api_path,
                        data: {
                            user_id: "<?php echo $user_id; ?>",
                            notes_id: "<?php echo $notes_id; ?>",
                            user_react: react
                        },
                        beforeSend: function() {
                            $(".react_btn").removeClass("btn-primary");
                            if (reaction == 1) {
                                $(btn).children().removeClass("fa-thumbs-up");
                                $(btn).children().addClass("fa-spinner fa-spin");
                            } else if (reaction == 2) {
                                $(btn).children().removeClass("fa-thumbs-down");
                                $(btn).children().addClass("fa-spinner fa-spin");
                            }
                        },
                        success: function(data) {
                            if (reaction == 1) {
                                $(btn).children().addClass("fa-thumbs-up");
                                $(btn).children().removeClass("fa-spinner fa-spin");
                            } else if (reaction == 2) {
                                $(btn).children().addClass("fa-thumbs-down");
                                $(btn).children().removeClass("fa-spinner fa-spin");
                            }
                            if (reacted) {
                                $(btn).addClass("btn-default");
                                $(btn).removeClass("btn-primary");
                            } else {
                                $(btn).removeClass("btn-default");
                                $(btn).addClass("btn-primary");
                            }
                        }
                    })
                    }else{
                       $(".login_error_modal").modal("show");
                    }
                });
            });

            // add react end 
            // make report start 
            $(".report_button").click(function() {
                $(".report_modal").modal("show");
                $(".report_notice").html("");
            });
            $(".submit_report_btn").click(function() {
                var cookie = getCookie("url");
                    if(cookie!==false){
                        get_path("make_report");
                var report_type = $("input[name='report_type']").val();
                var report_description = $("textarea[name='report_description']").val();
                if (report_type.length !== 0 && report_description.length !== 0) {
                    get_path("make_report");
                    var api_path = sessionStorage.getItem("temp_path");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $api_url ?>/" + api_path,
                        data: {
                            notes_id: "<?php echo $notes_id; ?>",
                            user_id: "<?php echo $user_id; ?>",
                            type: report_type,
                            desc: report_description
                        },
                        beforeSend: function() {
                            $(".submit_report_btn").html("Please Wait...");
                        },
                        success: function(data) {
                            $(".submit_report_btn").html("submit");
                            if (data.status == "success") {
                                $(".report_notice").html('<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Success!</strong> Successfully reported this note <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> </div>');
                                setTimeout(() => {
                                    $(".report_modal").modal("hide");
                                }, 2000);
                            } else if (data.messege == "already reported") {
                                $(".report_notice").html('<div class="alert alert-warning alert-dismissible fade show" role="alert"> <strong>warning!</strong> You have already reported this note  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> </div>');
                            } else {
                                $(".report_notice").html('<div class="alert alert-danger alert-dismissible fade show" role="alert"> <strong>Error!</strong>Somthing went wrong , please try after sometimes <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> </div>');
                            }
                            console.log(data);

                        }
                    })

                } else {
                    $(".report_notice").html('<div class="alert alert-warning alert-dismissible fade show" role="alert"> <strong>warning!</strong> All filed are required  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> </div>');
                }
                    }
                    else{
  $(".login_error_modal").modal("show");
                    }
                
            });

            // make report end 


        });
    </script>
    <?php
    echo '<script type="text/javascript">
document.addEventListener("adobe_dc_view_sdk.ready", function(){ 
  var adobeDCView = new AdobeDC.View({clientId: "7681ea38f8244c9fb1eb60403c905c00", divId: "adobe-dc-view"});
  adobeDCView.previewFile({
    content:{location: {url:"' . $pdf_url . '"}},
    metaData:{fileName: "' . $title . '"}
  }, {embedMode: "SIZED_CONTAINER", showDownloadPDF: false, showPrintPDF: true});
});
</script>';
    include("includes/footer.php");
    ?>

</body>

</html>