<?php
require_once("includes/db.php");
if(isset($_GET["keyword"])){
   $keyword =  str_replace("-"," ",mysqli_real_escape_string($db,strip_tags(trim($_GET["keyword"]))));
   $get_notes = $db->query("SELECT * FROM products WHERE  product_name LIKE'%".$keyword."%' OR keyword LIKE'%".$keyword."%'  ORDER BY id DESC");
   if($get_notes){
    if($get_notes->num_rows !==0){
        
            
    }else{
       header("Location:".$home_page."/notfound");
        
       //  header("Location:".$home_page."/notfound");
    }

   }else{
       // header("Location:".$home_page."/notfound");
       header("Location:".$home_page."/notfound");
   }
}
?>

<?php
require_once("includes/db.php");
include("includes/ago_time.php");
include("includes/clean.php");
?>
<!DOCTYPE html>
<html lang=" en-US" prefix="og: https://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php echo strtoupper($keyword); ?> </title>

    <meta name="description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <meta name="keyword" content="handwritten notes,pdf notes,engineering notes,management notes,notesocean, notes ocean,notes ocean bba notes">
    <meta name="robots" content="index,follow">
    <!-- og start  -->
    <meta property="og:site_name" content="Notes Ocean" />
    <meta property="og:title" content="<?php echo strtoupper($keyword); ?>" />
    <meta property="og:type" content="website" />
    <meta name="og:description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <!-- og end  -->
    <!-- ads script  -->
    <?php echo $ads_script; ?>

    <?php include("includes/cdn.php"); ?>
    <!-- homepage styles start -->
    <style type="text/css">
    .notes{
        padding: 15px;
        margin-top: 50px;
        margin-bottom: 50px;
        box-shadow: 0 0 3px 0.5px #888;
    }
    .notes h2{
        color: red;
    }
    .item{
        text-align: right;
    }
    .public_notes{
        border: 3px solid #f8f8f8;
        border-radius: 10px;
        border-left: 10px solid red;
        margin-bottom: 20px;
        padding: 10px 20px;
        border-right: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
        border-top: 1px solid #ccc;
    }
    a:hover{
        text-decoration: none;
    }
    .published{
        padding: 0 30px;
        text-align:left;
    }
    @media  (max-width: 767px) {
        .item{
        text-align: center;

        }
        .notes h2{
        color: red;
        font-size: 22px;
        text-align: center;
    }
    h5{
        font-size: 16px !important;
    }


    }

      
    </style>

    <!-- homepage styles end -->

</head>

<body>
    <?php include("includes/header.php") ?>
    <!-- <section class="latest_event jumbotron bg-white p-0">
        <a href="<?php echo $home_page; ?>/account"> <img src="assest/img/events/start.jpg" alt="notesocean-latest-event"></a>
    </section> -->
   


    


        <?php
        echo '<div class="container notes" style="margin-top:70px">
        <div class="row">
            <div class="col-md-10">
                <h2>'.strtoupper($keyword).'</h2>
            </div>
            <div class="col-md-2 item">
                <h4> '.$get_notes->num_rows.' Items</h4>
            </div>     
        </div>
    </div>
    <div class="container">';
        while($get_notes_info = $get_notes->fetch_assoc()){
                 
            $id = $get_notes_info["id"];
            $short_desc = strip_tags($get_notes_info["short_desc"]);
            $title = strip_tags($get_notes_info["product_name"]);
            $pdf_url = $get_notes_info["pdf_url"];
            $long_desc = $get_notes_info["long_desc"];
            $file_key = $get_notes_info["file_key"];
            $keyword = strip_tags($get_notes_info["keyword"]);
            $notes_by = $get_notes_info["notes_by"];
            $date = date_create($get_notes_info["updated_date"]);
            // notes user info 


            if ($notes_by == 0) {
                $pub_fullname = "notesocean";
                $pub_picture = $home_page . "/assest/img/nav_logo.png";
            } else {
                $get_user_info = $user_db->query("SELECT fullname,picture FROM profiles WHERE user_id = '$notes_by'");
                if ($get_user_info) {
                    $user_data = $get_user_info->fetch_assoc();
                    $pub_fullname = $user_data["fullname"];
                    $pub_picture = $user_data["picture"];
                }
            }
            echo '
            <a style="color: #323232;" href="'.$home_page.'/'.$id.'/'.just_clean($title).'.html" style="text-decoration:none">
            <div class="container public_notes mx-2">
<div class="row px-2">
   <h5> '.$title.' </h5>
</div>
<div class="row">
<div class="col-md-6">
<p>Published by: '.$pub_fullname.'  </p>
</div>
<div class="col-md-6">
<p> Published Date:  '.date_format($date, "d M Y").' </p> 
</div>
   
</div>
</div>
            
            
            </a>';
    }
        
        ?>
    
    </div>
        

    <!-- search secton end  -->




    <?php
    include("includes/footer.php");
    ?>

    <script>
        $(document).ready(() => {
            let url = location.href.replace(/\/$/, "");

            if (location.hash) {
                const hash = url.split("#");
                $('.nav-item a[href="#' + hash[1] + '"]').tab("show");
                url = location.href.replace(/\/#/, "#");
                history.replaceState(null, null, url);
                setTimeout(() => {
                    $(window).scrollTop(0);
                }, 400);
            }

            $('a[data-toggle="tab"]').on("click", function() {
                let newUrl;
                const hash = $(this).attr("href");
                if (hash == "#all") {
                    newUrl = url.split("#")[0];
                } else {
                    newUrl = url.split("#")[0] + hash;
                }
                newUrl += "/";
                history.replaceState(null, null, newUrl);
            });
        });
    </script>
</body>

</html>