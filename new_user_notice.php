<section>
    <div class="modal show fade p-0" id="notice_modal">
        <div class="modal-dialog modal-lg p-0">
        <div class="modal-content p-0">
            
            <div class="modal-body p-0" >
            <iframe width="100%" style="height:60vh" src="https://www.youtube.com/embed/eot2c580Dfw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="modal-footer p-0">
            <a class="btn btn-danger text-light" data-dismiss="modal" id="close">close</a>
            </div>
        </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function() {
    $("#show_tutorial").click(function() {
        $("#notice_modal").modal("show");
    });
    
});
</script>