
<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- ==== Document Title ==== -->
    <title>Add New Products</title>

    <!-- ==== Document Meta ==== -->
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- ==== Favicon ==== -->
    <link rel="icon" href="favicon.png" type="image/png">

    <!-- ==== Google Font ==== -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700%7CMontserrat:400,500">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="assets/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" href="assets/css/morris.min.css">
    <link rel="stylesheet" href="assets/css/select2.min.css">
    <link rel="stylesheet" href="assets/css/jquery-jvectormap.min.css">
    <link rel="stylesheet" href="assets/css/horizontal-timeline.min.css">
    <link rel="stylesheet" href="assets/css/weather-icons.min.css">
    <link rel="stylesheet" href="assets/css/ion.rangeSlider.min.css">
    <link rel="stylesheet" href="assets/css/ion.rangeSlider.skinFlat.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Page Level Stylesheets -->

</head>

<body>

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <header class="navbar navbar-fixed">
            <!-- Navbar Header Start -->
            <div class="navbar--header">
                <!-- Logo Start -->
                <a href="/" class="logo">
                    <h3> <span class="text-warning">Notes</span> <span class="text-light">Ocean</span> </h3>
                </a>
                <!-- Logo End -->

                <!-- Sidebar Toggle Button Start -->
                <a href="#" class="navbar--btn" data-toggle="sidebar" title="Toggle Sidebar">
                    <i class="fa fa-bars"></i>
                </a>
                <!-- Sidebar Toggle Button End -->
            </div>
            <!-- Navbar Header End -->

            <!-- Sidebar Toggle Button Start -->
            <a href="#" class="navbar--btn" data-toggle="sidebar" title="Toggle Sidebar">
                <i class="fa fa-bars"></i>
            </a>
            <!-- Sidebar Toggle Button End -->

        </header>
        <!-- Navbar End -->

        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->

        <!-- Main Container Start -->
        <main class="main--container">
            <!-- Page Header Start -->

            <!-- Page Header End -->

            <!-- Main Content Start -->
            <section class="main--content">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Add New Products</h3>
                    </div>
                    <div class="panel-content">
                        <div class="alert_box">

                        </div>
                        <form class="add_produts_form">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Notes / Topic Name <sup class="text-danger">*</sup></label>
                                        <input type="text" class="form-control" name="product_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Short Description (max 155 )</label>
                                        <textarea maxlength="155" name="short_description" class="form-control" cols="20" rows="5"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label> Full Description </label>
                                        <textarea name="long_description" class="form-control" cols="20" rows="10"></textarea>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <label for="course">Course <i class="fa fa-spinner fa-spin course_loader d-none"> </i>  </label>
                                    <select name="course" class=" course_select_sub form-control my-2">
                                        <option value="one">One</option>
                                    </select>
                                    <label for="semster">Semester <i class="fa fa-spinner fa-spin sems_loader d-none"> </i> </label>
                                    <select name="semster" class="sems form-control mb-3">
                                        <option value="none">none</option>
                                    </select>
                                    <label for="subject">Subject <i class="fa fa-spinner fa-spin sub_loader d-none"> </i></label>
                                    <select name="subject" class="subject_select form-control my-2">
                                        <option value="none">none</option>

                                    </select>
                                    <div class="form-group">
                                        <label>Pdf ( if available )</label>
                                        <input type="file" accept=".pdf" class="form-control" name="product_pdf">
                                    </div>

                                    <div class="form-group text-left">
                                        <label> Keyword (saprate with a comma ) (min 10 keyword) <sup class="text-danger">*</sup> </label>
                                        <textarea minlength="30" name="keyword" class="form-control" style="text-align:left" cols="20" rows="5" required>example1,example2,example3</textarea>
                                    </div>
                                    <button class="btn btn-primary add_product_btn upload_sbumit_btn" type="submit">Submit</button>
                                    <div class="progress_box d-none float-right w-75 ml-auto py-3">
                                        uploading... <span class="uploading_notice"></span>
                                        <div class="progress  w-100">
                                            <div class="progress-bar bg-primary"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </form>

                    </div>
            </section>
            <!-- Main Content End -->

            <!-- Main Footer Start -->
            <footer class="main--footer main--footer-light">
                <p>Copyright &copy; <a href="#">DAdmin</a>. All Rights Reserved.</p>
            </footer>
            <!-- Main Footer End -->
        </main>
        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/perfect-scrollbar.min.js"></script>
    <script src="assets/js/select2.min.js"></script>
    <script src="assets/js/jquery-jvectormap.min.js"></script>
    <script src="assets/js/jquery-jvectormap-world-mill.min.js"></script>
    <script src="assets/js/horizontal-timeline.min.js"></script>
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <script src="assets/js/datatables.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/products.js?cache='<?php echo time(); ?>'"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Page Level Scripts -->

</body>

</html>