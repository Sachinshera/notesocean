<?php session_start();
if ($_SESSION["valid_user"] == "Admin") {
} else {
    header("Location:index.php");
} ?>
<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Add Course</title>
    <?php require("assets/includes/cdncss.php"); ?>
    <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>
    <style type="text/css">
        .title {
            color: #000;
        }

        .title h1 {
            font-weight: bold;
        }

        .public {
            color: red;
        }

        .itm {
            background-color: #fff;
            padding: 15px;
            margin: 15px 10px;
            border-radius: 10px;
        }

        .file p {
            color: red;
        }

        .file_span {
            color: #000;
        }

        .basic {
            padding: 20px 30px 100px 0px;
            margin: 15px 10px;
            color: #000;
        }

        .star {
            color: red;
        }

        .basic p {
            margin: 0;
        }

        .basic select {
            border: 1px solid red;
            border-radius: 5px;
            width: 290px;
            padding: 5px;
            margin-bottom: 20px;
            margin-right: 30px;
        }

        .btn_save {
            background-color: red;
            color: #fff;
            border: none;
            padding: 15px 25px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        .new_area {
            height: auto !important;
        }

        .status {
            width: 100%;
            border-color: #ebebea;
            padding: 8px 20px;
            border-radius: 5px;
            color: #495057;
        }
    </style>

</head>

<body>

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php require("assets/includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->


        <main class="main--container">

            <section class="main--content new_area">
                <div class="title">
                    <h1>Add New <span class="public">Course</span></h1>
                </div>

                <form class="course_form">
                    <div class="form-group">
                        <label for="addcourse">Add Course Title<span class="star">*</span></label>
                        <input type="text" class="form-control" id="course" name="course" placeholder="Enter Course Title">
                    </div>
                    <div class="form-group">
                        <label for="course_description">Course Description <span class="star">*</span></label>
                        <form method="post">
                            <textarea id="editor" name="course_description"></textarea>
                        </form>
                    </div>
                    <div class="form-group">
                        <label for="addcourse">Course Related Tags<span class="star">*</span></label>
                        <input type="text" class="form-control" id="course_tag" name="course_tag" placeholder="Enter Course Tag">
                    </div>
                    <div class="form-group">
                        <label for="choose_faculty">Choose Faculty <span class="star">*</span></label>
                        <select class="form-select faculty status" aria-label="Default select example" id="faculty">
                            <option selected>None</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="faculty_status">Faculty Status<span class="star">*</span></label>
                        <select class="form-select status" id="faculty_status" aria-label="Default select example">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                        </select>
                    </div>
                    <button class="btn_save btn" type="submit">Save</button>
                </form>
    
             </section>
    </main>
    <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->


    <?php require("assets/includes/cdnjs.php"); ?>
    <script type="text/javascript">
        CKEDITOR.replace('editor');
    </script>
    <!-- script -->
            <script>
            $(document).ready(function(){
                function get_all_faculty(){
                    var sel_box = $(".faculty");
                    $.ajax({
                        type: "POST",
                        data:{for:"get_all"},
                        url:"php/faculty.php",
                        beforeSend: function(){
                            $(sel_box).html('<option value=""> Loading... </option>');
                          
                        },
                        success: function(data){
                            if(data.status=="success"){
                            $(sel_box).html('');
                            $(sel_box).append('<option value="none"> none </option>');
                              var data = data.data;
                                for(let i=0;i<data.length;i++){
                                    let id = data[i].id;
                                    let name = data[i].name;
                                    $(sel_box).append('<option value="'+id+'"> '+name+'</option>');
                                }
                            }
                            add_course();
                        }
                    })
                };
                get_all_faculty();
                function add_course(){
                    $(".course_form").submit(function(event){
                        event.preventDefault();
                        add_to_db();
                    });
                    $(".btn_save").click(function(){
                        add_to_db();
                    });

                    function add_to_db(){
                        var name = $("#course").val();
                        var objEditor =CKEDITOR.instances["editor"];
                        var description = objEditor.getData();
                        var faculty = $(".faculty").val();
                        var tags = $("#course_tag").val();
                        var status = $("#faculty_status").val();
                        if (name == "" || description == "" || tags == "" || description == "") {
                        swal("warning", "All field are required", "warning");
                        }
                        else{
                            $.ajax({
                            type:"POST",
                            url:"php/course.php",
                            data:{
                                for:"add_course",
                                course_name:name,
                                course_desc:description,
                                course_status:status,
                                course_faculty:faculty,
                                tags:tags
                            },
                            beforeSend:function(){
                                swal({
                                    text:"Processing....",
                                    button:false,
                                    allowEscapeKey: false,
                                    allowOutsideClick:false,
                                    closeOnConfirm:false,
                                    closeOnClickOutside: false
                                });
                            },
                            success:function(data){
                              swal.close();
                              if(data.status=="success"){
                                  swal("success","course added","success");
                              }
                              else if(data.status=="exist"){
                                swal("warning","this course is already exist","warning");
                              }
                              else{
                                swal("error","somthing went wrong,please try again","error");
                              }
                            }
                        })
                        }
                    }
                };
                

            });
            </script>
    <!-- script -->

</body>

</html>