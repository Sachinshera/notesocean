$(document).ready(function () {
  // add new subject function
  function add_new_subject() {
    $(".add_subject_form").on("submit", function (event) {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "php/add_new_subject.php",
        data: new FormData(this),
        processData: false,
        contentType: false,
        Cache: false,
        beforeSend: function () {
          $(".submit_btn").prop("disabled", true);
          $(".submit_btn").html("Please wait...");
        },
        success: function (response) {
          load_all_subjects();
          $(".submit_btn").prop("disabled", false);
          $(".submit_btn").html("Submit");
          var json = JSON.parse(response);
          var data = JSON.parse(atob(json.result.split(".")[1]));
          if (data == "success") {
            swal("success!", "New Subject Added", "success");
          } else if (data == "exist") {
            swal("Warning!", "Subject already exist ", "warning");
            
          }  else {
            swal("Error!", "somthing went wrong please try again", "error");
          }
          setTimeout(function () {
            $(".alert_box").html("");
          }, 7000);
        },
      });
    });
  };
  // add new function end
  add_new_subject();
  function load_all_subjects() {
    $.ajax({
      type: "POST",
      url: "php/load_all_subjects.php",
      beforeSend: function () {
        $(".table_loader").removeClass("d-none");
      },
      success: function (response) {
        $(".table_loader").addClass("d-none");
        var data = JSON.parse(atob(JSON.parse(response).result.split(".")[1]));
        let count = 0;
        $(".table_body").html("");
        for (let i = 0; i < data.length; i++) {
          count++;
          let name = data[i].table_name;
          let pic = data[i].subject_pic;
          let status = data[i].status;
          let id = data[i].id;
          $(".table_body").append(
            "<tr><td>" +
              count +
              "</td><td> " +
              name +
              "</td><td> " +
              status +
              ' </td><td> <i class="fa fa-edit edit_icon" data-id="' +
              id +
              '" data-name="' +
              name +
              '"> </i> </td><td> <i class="fa fa-trash delete_icon" data-id="' +
              id +
              '" data-name="' +
              name +
              '"> </i> </td></tr>'
          );
        }
        delte_subject();
        edit_subject();
      },
    });
  };
  load_all_subjects();
  function delte_subject() {
    $(".delete_icon").each(function () {
      $(this).click(function () {
        let data_id = $(this).attr("data-id");
        let data_name = $(this).attr("data-name");
        let confirm = window.confirm("Are you sure");
        var this_btn = this;
        if (confirm) {
          $.ajax({
            type: "POST",
            url: "php/delete_subject.php",
            data: {
              id: data_id,
              name: data_name,
            },
            beforeSend: function () {
              $(this_btn).removeClass("fa fa-trash");
              $(this_btn).addClass("fa fa-spinner fa-spin");
            },
            success: function (response) {
              $(this_btn).removeClass("fa fa-spinner fa-spin");
              $(this_btn).addClass("fa fa-trash");
              var json = JSON.parse(response);
              var data = JSON.parse(atob(json.result.split(".")[1]));
              if (data == "success") {
                swal("success!", "Subject Deleted", "success");
                let parent_box = this_btn.parentElement.parentElement.remove();
              } else {
                swal("Error!", "Subject was not deleted", "error");
              }
              setTimeout(function () {
                $(".alert_box").html("");
              }, 7000);
            },
          });
        }
      });
    });
  };
  function edit_subject() {
    $(".edit_icon").each(function () {
      $(this).click(function () {
        let data_id = $(this).attr("data-id");
        let data_name = $(this).attr("data-name");
        var this_btn = this;
        $.ajax({
          type: "POST",
          url: "php/get_custom_subject_data.php",
          data: {
            id: data_id,
            name: data_name,
          },
          beforeSend: function () {
            $(this_btn).removeClass("fa fa-edit");
            $(this_btn).addClass("fa fa-spinner fa-spin");
          },
          success: function (response) {
            $(this_btn).removeClass("fa fa-spinner fa-spin");
            $(this_btn).addClass("fa fa-edit");
            $(".edit_modal").modal("show");
            
            //
            var json = JSON.parse(response);
            var data = JSON.parse(atob(json.result.split(".")[1]));
            for (let i = 0; i < data.length; i++) {
              let name = data[i].table_name;
              let pic = data[i].subject_pic;
              let status = data[i].status;
              let id = data[i].id;
              let created_date = data[i].created_date;
              let updated_date = data[i].updated_date;
              let sub_desc = data[i].sub_desc;
              let course = data[i].course;
              let sems = data[i].sems;
              $.ajax({
                type: "POST",
                url:"php/get_sems_by_course.php",
                async: true,
                data:{
                  course:course
                },
                beforeSend: function(){
                  $(".edit_sem_loader").removeClass("d-none");
                  $(".sel_sem_for_course").prop("disabled",true);
                },
                success: function(response){
                  $(".sel_sem_for_course").prop("disabled",false);
                  $(".edit_sem_loader").addClass("d-none");
                  $(".sel_sem_for_course").html(response);
                  $(".sel_sem_for_course").val(sems);
                }
              });
              
              $(".edited_subject_name").val(name);
              $("#sub_desc").val(sub_desc);
              $(".created_date_text").html(created_date);
              $(".updated_date_text").html(updated_date);
              $(".status_chnage").val(status);
             
              $(".course_select_sub").val(course);
              $(".uploaded_img2").attr(
                "src",
                "http://localhost/notesocean/management/" + pic
              );
              $(".form_id_edit_subject").val(id);
            }
            update_edited_subject();
          },
        });
      });
    });
  };
  // reaload btn
  $(".reload_btn").click(function () {
    load_all_subjects();
  });
  // image prevview
  function update_edited_subject() {
    $(".subject_edit_form").submit(function (event) {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "php/update_edited_subject.php",
        data: new FormData(this),
        processData: false,
        contentType: false,
        Cache: false,
        beforeSend: function () {
          $(".update_btn").html("Working.....");
          $(".update_btn").prop("disabled",true);
        },
        success: function (response) {
          $(".update_btn").prop("disabled",false);
          $(".update_btn").html("Update");
          load_all_subjects();
          var json = JSON.parse(response);
          var data = JSON.parse(atob(json.result.split(".")[1]));
          console.log(data);
          if (data == "success") {
            swal("success!", "Subject Updated", "success");
            $(".edit_modal").modal("hide");
          } else {
            swal("Error!", "Failed to update subject, please try again later", "error");
          }
          setTimeout(function () {
            $(".modal_alert").html("");
          }, 5000);
        },
      });
    });
  };
  function get_all_sub_in_sel() {
    var sel_tag = $("#slt_sub");
    $.ajax({
      type: "POST",
      url: "php/load_all_subjects.php",
      beforeSend: function () {},
      success: function (response) {
        $(sel_tag).html("");
        var response_data = JSON.parse(response).result;
        var data = JSON.parse(atob(JSON.parse(response).result.split(".")[1]));
        // after data processed
        for (let i = 0; i < data.length; i++) {
          let id = data[i].id;
          let name = data[i].table_name;
          $(sel_tag).append(
            '<option value="' + id + '"> ' + name + "  </option>"
          );
        }
        sessionStorage.setItem("active_sub", data[0].id);
      },
    });
  };
  var active_sub_id = 10;
  get_all_sub_in_sel();
  function get_all_pro_in_sel() {
    var sel_tag = $("#slt_pro");
    $.ajax({
      type: "POST",
      url: "php/manage_product.php",
      async: false,
      data: {
        get: "get_data",
      },
      beforeSend: function () {},
      success: function (response) {
        $(sel_tag).html("");
        var data = JSON.parse(response).result;
        var base64 = parseJwt(data);
        // extract data
        function parseJwt(token) {
          var base64Url = token.split(".")[1];
          var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
          var jsonPayload = decodeURIComponent(
            atob(base64)
              .split("")
              .map(function (c) {
                return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
              })
              .join("")
          );

          return JSON.parse(JSON.parse(jsonPayload));
        }
        active_sub_id = base64[0].id;
        for (let i = 0; i < base64.length; i++) {
          let id = base64[i].id;
          active_sub_id = base64[0].id;
          let product_name = base64[i].product_name;
          $(sel_tag).append(
            '<option value="' + id + '"> ' + product_name + " </option>"
          );
        }
      },
    });
  };
  get_all_pro_in_sel();
  function upload_pro_in_subject() {
    $(".add_sub_in_pro_form").submit(function (event) {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "php/add_pro_in_sub.php",
        data: new FormData(this),
        processData: false,
        contentType: false,
        async: false,
        cache: false,
        beforeSend: function () {},
        success: function (response) {
          let response_data = JSON.parse(response).result;
          let data = parseJwt(response_data);
          function parseJwt(token) {
            var base64Url = token.split(".")[1];
            var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
            var jsonPayload = decodeURIComponent(
              atob(base64)
                .split("")
                .map(function (c) {
                  return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
                })
                .join("")
            );

            return JSON.parse(jsonPayload);
          }
          if (data == "success") {
            $(".alert_box").html(
              '<div class="alert alert-success"> Success!  Product list successfuly </div>'
            );
            view_product_by_subejct_in_add_pro_in_sub("reload");
          } else if (data == "exist") {
            $(".alert_box").html(
              '<div class="alert alert-warning"> Warning!   Product already listed </div>'
            );
          } else {
            $(".alert_box").html(
              '<div class="alert alert-danger"> Opps! Somthing went wrong please try after sometimes</div>'
            );
          }
          setTimeout(function () {
            $(".alert_box").html("");
          }, 5000);
        },
      });
    });
  };
  upload_pro_in_subject();
  function view_product_by_subejct_in_add_pro_in_sub(get_data) {
    if (sessionStorage.getItem("active_sub") !== "null") {
      show_result(sessionStorage.getItem("active_sub"));
    }
    if (get_data == "reload") {
      show_result(sessionStorage.getItem("active_sub"));
    }
    $("#slt_sub").on("change", function () {
      let data_id = $(this).val();
      show_result(data_id);
      sessionStorage.setItem("active_sub", data_id);
    });
    function show_result(data_id) {
      $.ajax({
        type: "POST",
        url: "php/get_pro_by_sub.php",
        data: {
          sub_id: data_id,
        },
        async: false,
        beforeSend: function () {
          $(".loader_pro").removeClass("d-none");
        },
        success: function (response) {
          $(".loader_pro").addClass("d-none");
          $("#pro_in_show_table").html("");
          let response_data = JSON.parse(response).result;
          let data = parseJwt(response_data);
          function parseJwt(token) {
            var base64Url = token.split(".")[1];
            var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
            var jsonPayload = decodeURIComponent(
              atob(base64)
                .split("")
                .map(function (c) {
                  return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
                })
                .join("")
            );

            return JSON.parse(JSON.parse(jsonPayload));
          }

          if (data !== "nodata") {
            $(".nodata").addClass("d-none");
            $("#pro_in_show_table").append(
              "<thead>   <tr>  <th>#</th> <th>Product Name</th><th>Delete</th>   </tr> </thead>"
            );
            $("#pro_in_show_table").append(
              '<tbody class="add_pro_table_body"></tbody>'
            );
            var count = 0;
            for (let i = 0; i < data.length; i++) {
              count++;
              let product_id = data[i].product_id;
              let product_name = data[i].product_name;
              $(".add_pro_table_body").append(
                "<tr> <td> " +
                  count +
                  "  </td> <td> " +
                  product_name +
                  ' </td> <td> <i class="fa fa-trash delete_icon_sub" data-id="' +
                  product_id +
                  '" sub-id="' +
                  data_id +
                  '">  </i> </td> </tr>'
              );
            }
          } else {
            $(".nodata").removeClass("d-none");
          }
          function delete_pro_in_sub() {
            $(".delete_icon_sub").each(function () {
              $(this).click(function () {
                var this_btn = this;
                let del_id = $(this).attr("data-id");
                let sub_id = $(this).attr("sub-id");
                let confirm = window.confirm("Are you sure?");
                if (confirm) {
                  $.ajax({
                    type: "POST",
                    url: "php/delete_pro_in_sub.php",
                    data: {
                      id: del_id,
                      sub_id: sub_id,
                    },
                    beforeSend: function () {},
                    success: function (response) {
                      let response_data = JSON.parse(response).result;
                      let data = parseJwt(response_data);
                      function parseJwt(token) {
                        var base64Url = token.split(".")[1];
                        var base64 = base64Url
                          .replace(/-/g, "+")
                          .replace(/_/g, "/");
                        var jsonPayload = decodeURIComponent(
                          atob(base64)
                            .split("")
                            .map(function (c) {
                              return (
                                "%" +
                                ("00" + c.charCodeAt(0).toString(16)).slice(-2)
                              );
                            })
                            .join("")
                        );

                        return JSON.parse(jsonPayload);
                      }
                      if (data == "success") {
                        $(".alert_box").html(
                          '<div class="alert alert-success"> Success!  Product list successfuly </div>'
                        );
                        let parent_box = this_btn.parentElement.parentElement.remove();
                      } else {
                        $(".alert_box").html(
                          '<div class="alert alert-danger"> Opps! Somthing went wrong please try after sometimes</div>'
                        );
                      }

                      setTimeout(function () {
                        $(".alert_box").html("");
                      }, 5000);
                    },
                  });
                }
              });
            });
          }
          delete_pro_in_sub();
        },
      });
    }
  };
  view_product_by_subejct_in_add_pro_in_sub();
  function get_all_course() { 
    $(".course_select_sub").prop("disabled",true);
    var sel_tag = $(".course_select_sub");
    $.ajax({
      type: "POST",
      url: "php/load_all_courses.php",
      beforeSend:function () {
        $(".course_loader").removeClass("d-none");
        $(".course_select_sub").prop("disabled",true);
      },
      success: function (response) {
        $(".course_select_sub").prop("disabled",false);
        $(".course_loader").addClass("d-none");
         $(sel_tag).html("");
        var data = JSON.parse(atob(JSON.parse(response).result.split(".")[1]));
        // after data processed
        $(sel_tag).append(
            '<option value="none"> None </option>'
          );
        for (let i = 0; i < data.length; i++) {
          var id = data[i].id;
          let name = data[i].table_name;
          $(sel_tag).append(
            '<option value="' +name + '"> ' + name + "  </option>"
          );
        }
      }
    });

  };
  get_all_course();
  function get_sem_by_course() { 
    $(".course_select_sub").on("change", function () {
      let course = $(this).val();
      $.ajax({
        type: "POST",
        url: "php/get_sems_by_course.php",
        data: {
          course: course
        },
        beforeSend: function () {
          $(".semester_loader").removeClass("d-none");
          $(".sel_sem_for_course").prop("disabled",true);
         },
        success: function (response) {
          $(".sel_sem_for_course").prop("disabled",false);
          $(".semester_loader").addClass("d-none");
          $(".sel_sem_for_course").html(response);
        }
      });
    });
  };
  get_sem_by_course();
});
