$(document).ready(function () {
  function add_new_course() {
    $(".add_course_form").on("submit", function (event) {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "php/add_new_course.php",
        data: new FormData(this),
        processData: false,
        contentType: false,
        Cache: false,
        beforeSend: function () {
          $(".submit_btn").prop("disabled", true);
          $(".submit_btn").html("Please wait...");
        },
        success: function (response) {
          load_all_courses();
          $(".submit_btn").prop("disabled", false);
          $(".submit_btn").html("Submit");
          var json = JSON.parse(response);
          var data = JSON.parse(atob(json.result.split(".")[1]));
          if (data == "success") {
            $(".alert_box").html(
              '<div class="alert alert-success"> Success!  New course Added </div>'
            );
          } else if (data == "exist") {
            $(".alert_box").html(
              '<div class="alert alert-warning"> Warning!  course already Added </div>'
            );
          } else if (data == "imgerror") {
            $(".alert_box").html(
              '<div class="alert alert-warning"> Warning!  Unsupported image type please  choose another </div>'
            );
          } else {
            $(".alert_box").html(
              '<div class="alert alert-danger"> Opps!  Somthing went wrong please try after sometimes </div>'
            );
          }
          setTimeout(function () {
            $(".alert_box").html("");
          }, 7000);
        },
      });
    });
  }
  add_new_course();
  function load_all_courses() {
    $.ajax({
      type: "POST",
      url: "php/load_all_courses.php",
      beforeSend: function () {
        $(".table_loader").removeClass("d-none");
      },
      success: function (response) {
        $(".table_loader").addClass("d-none");
        var data = JSON.parse(atob(JSON.parse(response).result.split(".")[1]));
        let count = 0;
        $(".table_body").html("");
        for (let i = 0; i < data.length; i++) {
          count++;
          let name = data[i].table_name;
          let pic = data[i].course_pic;
          let status = data[i].status;
          let id = data[i].id;
          $(".table_body").append(
            "<tr><td>" +
              count +
              "</td><td> " +
              name +
              "</td><td> " +
              status +
              ' </td><td> <i class="fa fa-edit edit_icon" data-id="' +
              id +
              '" data-name="' +
              name +
              '"> </i> </td><td> <i class="fa fa-trash delete_icon" data-id="' +
              id +
              '" data-name="' +
              name +
              '"> </i> </td></tr>'
          );
        }
        delte_course();
        edit_course();
      },
    });
  }
  load_all_courses();
  function delte_course() {
    $(".delete_icon").each(function () {
      $(this).click(function () {
        let data_id = $(this).attr("data-id");
        let data_name = $(this).attr("data-name");
        let confirm = window.confirm("Are you sure");
        var this_btn = this;
        if (confirm) {
          $.ajax({
            type: "POST",
            url: "php/delete_course.php",
            data: {
              id: data_id,
              name: data_name,
            },
            beforeSend: function () {
              $(this_btn).removeClass("fa fa-trash");
              $(this_btn).addClass("fa fa-spinner fa-spin");
            },
            success: function (response) {
              $(this_btn).removeClass("fa fa-spinner fa-spin");
              $(this_btn).addClass("fa fa-trash");
              var json = JSON.parse(response);
              var data = JSON.parse(atob(json.result.split(".")[1]));
              if (data == "success") {
                $(".alert_box").html(
                  '<div class="alert alert-success"> Success!  New course Deleted </div>'
                );
                let parent_box = this_btn.parentElement.parentElement.remove();
              } else {
                $(".alert_box").html(
                  '<div class="alert alert-danger"> Opps!  Somthing went wrong please try after sometimes </div>'
                );
              }
              setTimeout(function () {
                $(".alert_box").html("");
              }, 7000);
            },
          });
        }
      });
    });
  }
  function edit_course() {
    $(".edit_icon").each(function () {
      $(this).click(function () {
        let data_id = $(this).attr("data-id");
        let data_name = $(this).attr("data-name");
        var this_btn = this;
        $.ajax({
          type: "POST",
          url: "php/get_custom_course_data.php",
          data: {
            id: data_id,
            name: data_name,
          },
          beforeSend: function () {
            $(this_btn).removeClass("fa fa-edit");
            $(this_btn).addClass("fa fa-spinner fa-spin");
          },
          success: function (response) {
            $(this_btn).removeClass("fa fa-spinner fa-spin");
            $(this_btn).addClass("fa fa-edit");
            $(".edit_modal").modal("show");
            image_preview();
            //
            var json = JSON.parse(response);
            var data = JSON.parse(atob(json.result.split(".")[1]));
            for (let i = 0; i < data.length; i++) {
              let name = data[i].table_name;
              let pic = data[i].course_pic;
              let status = data[i].status;
              let id = data[i].id;
              let created_date = data[i].created_date;
              let updated_date = data[i].updated_date;
              let course_desc = data[i].course_desc;
              $(".edited_course_name").val(name);
              $("#course_desc").val(course_desc);
              $(".created_date_text").html(created_date);
              $(".updated_date_text").html(updated_date);
              $(".status_chnage").val(status);
              $(".course_img_edited").attr(
                "src",
                "http://localhost/notesocean/" + pic
              );
              $(".form_id_edit_course").val(id);
            }
            update_edited_course();
          },
        });
      });
    });
  }
  // reaload btn
  $(".reload_btn").click(function () {
    load_all_courses();
  });
  // image prevview
  function image_preview() {
    $(".input_edit_image").on("change", function () {
      let img_file = this.files[0];
      let blob = URL.createObjectURL(img_file);
      $(".course_img_edited").attr("src", blob);
    });
  }
  function update_edited_course() {
    $(".course_edit_form").submit(function (event) {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "php/update_edited_course.php",
        data: new FormData(this),
        processData: false,
        contentType: false,
        Cache: false,
        beforeSend: function () {},
        success: function (response) {
          load_all_courses();
          var json = JSON.parse(response);
          var data = JSON.parse(atob(json.result.split(".")[1]));
          if (data == "success") {
            $(".modal_alert").html(
              '<div class="alert alert-success"> Success!  course Details Updated </div>'
            );
          } else {
            $(".modal_alert").html(
              '<div class="alert alert-danger"> Opps!  Somthing went wrong please try after sometimes </div>'
            );
          }
          setTimeout(function () {
            $(".modal_alert").html("");
          }, 5000);
        },
      });
    });
  }
  function get_all_course_in_sel() {
    var sel_tag = $("#slt_course");
    $.ajax({
      type: "POST",
      url: "php/load_all_courses.php",
      beforeSend: function () {},
      success: function (response) {
        $(sel_tag).html("");
        var data = JSON.parse(atob(JSON.parse(response).result.split(".")[1]));
        // after data processed
        for (let i = 0; i < data.length; i++) {
          var id = data[i].id;
          let name = data[i].table_name;
          $(sel_tag).append(
            '<option value="' + id + '"> ' + name + "  </option>"
          );
        }
        sessionStorage.setItem("active_course", data[0].id);
      },
    });
  }
  var active_course_id = 10;
  get_all_course_in_sel();
  function get_all_pro_in_sel() {
    var sel_tag = $("#slt_subject");
    $.ajax({
      type: "POST",
      url: "php/load_all_subjects.php",
      async: false,
      data: {
        get: "get_data",
      },
      beforeSend: function () {},
      success: function (response) {
        var sel_tag = $("#slt_subject");
        $(sel_tag).html("");
        var response_data = JSON.parse(response).result;
        var data = JSON.parse(atob(JSON.parse(response).result.split(".")[1]));
        // after data processed
        
        for (let i = 0; i < data.length; i++) {
          let id = data[i].id;
          let name = data[i].table_name;
          $(sel_tag).append(
            '<option value="' + id + '"> ' + name + "  </option>"
          );
        }
       
      },
    });
  }
  get_all_pro_in_sel();
  function upload_pro_in_course() {
    $(".add_course_in_pro_form").submit(function (event) {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "php/add_sub_in_course.php",
        data: new FormData(this),
        processData: false,
        contentType: false,
        async: false,
        cache: false,
        beforeSend: function () {},
        success: function (response) {
          let response_data = JSON.parse(response).result;
          let data = parseJwt(response_data);
          function parseJwt(token) {
            var base64Url = token.split(".")[1];
            var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
            var jsonPayload = decodeURIComponent(
              atob(base64)
                .split("")
                .map(function (c) {
                  return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
                })
                .join("")
            );

            return JSON.parse(jsonPayload);
          }
          if (data == "success") {
            $(".alert_box").html(
              '<div class="alert alert-success"> Success!  Product list successfuly </div>'
            );
            view_product_by_courseejct_in_add_pro_in_course("reload");
          } else if (data == "exist") {
            $(".alert_box").html(
              '<div class="alert alert-warning"> Warning!   Product already listed </div>'
            );
          } else {
            $(".alert_box").html(
              '<div class="alert alert-danger"> Opps! Somthing went wrong please try after sometimes</div>'
            );
          }
          setTimeout(function () {
            $(".alert_box").html("");
          }, 5000);
        },
      });
    });
  }
  upload_pro_in_course();
  // function view_product_by_courseejct_in_add_pro_in_course(get_data) {
  //   if (sessionStorage.getItem("active_course") !== "null") {
  //     show_result(sessionStorage.getItem("active_course"));
  //   }
  //   if (get_data == "reload") {
  //     show_result(sessionStorage.getItem("active_course"));
  //   }
  //   $("#slt_course").on("change", function () {
  //     let data_id = $(this).val();
  //     show_result(data_id);
  //     sessionStorage.setItem("active_course", data_id);
  //   });
  //   // function show_result(data_id) {
  //   //   $.ajax({
  //   //     type: "POST",
  //   //     url: "php/get_sub_by_course.php",
  //   //     data: {
  //   //       course_id: data_id,
  //   //     },
  //   //     async: false,
  //   //     beforeSend: function () {
  //   //       $(".loader_pro").removeClass("d-none");
  //   //     },
  //   //     success: function (response) {
  //   //       $(".loader_pro").addClass("d-none");
  //   //       $("#pro_in_show_table").html("");
  //   //       let response_data = JSON.parse(response).result;
  //   //       let data = parseJwt(response_data);
  //   //       function parseJwt(token) {
  //   //         var base64Url = token.split(".")[1];
  //   //         var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  //   //         var jsonPayload = decodeURIComponent(
  //   //           atob(base64)
  //   //             .split("")
  //   //             .map(function (c) {
  //   //               return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
  //   //             })
  //   //             .join("")
  //   //         );

  //   //         return JSON.parse(JSON.parse(jsonPayload));
  //   //       }

  //   //       if (data !== "nodata") {
  //   //         $(".nodata").addClass("d-none");
  //   //         $("#pro_in_show_table").append(
  //   //           "<thead>   <tr>  <th>#</th> <th>Product Name</th><th>Delete</th>   </tr> </thead>"
  //   //         );
  //   //         $("#pro_in_show_table").append(
  //   //           '<tbody class="add_pro_table_body"></tbody>'
  //   //         );
  //   //         var count = 0;
  //   //         for (let i = 0; i < data.length; i++) {
  //   //           count++;
  //   //           let product_id = data[i].subject_id;
  //   //           let product_name = data[i].subject_name;
  //   //           $(".add_pro_table_body").append(
  //   //             "<tr> <td> " +
  //   //               count +
  //   //               "  </td> <td> " +
  //   //               product_name +
  //   //               ' </td> <td> <i class="fa fa-trash delete_icon_course" data-id="' +
  //   //               product_id +
  //   //               '" sub-id="' +
  //   //               data_id +
  //   //               '">  </i> </td> </tr>'
  //   //           );
  //   //         }
  //   //       } else {
  //   //         $(".nodata").removeClass("d-none");
  //   //       }
  //   //       function delete_pro_in_course() {
  //   //         $(".delete_icon_course").each(function () {
  //   //           $(this).click(function () {
  //   //             var this_btn = this;
  //   //             let del_id = $(this).attr("data-id");
  //   //             let course_id = $(this).attr("sub-id");
  //   //             let confirm = window.confirm("Are you sure?");
  //   //             if (confirm) {
  //   //               $.ajax({
  //   //                 type: "POST",
  //   //                 url: "php/delete_sub_in_course.php",
  //   //                 data: {
  //   //                   id: del_id,
  //   //                   course_id: course_id,
  //   //                 },
  //   //                 beforeSend: function () {},
  //   //                 success: function (response) {
  //   //                   let response_data = JSON.parse(response).result;
  //   //                   let data = parseJwt(response_data);
  //   //                   function parseJwt(token) {
  //   //                     var base64Url = token.split(".")[1];
  //   //                     var base64 = base64Url
  //   //                       .replace(/-/g, "+")
  //   //                       .replace(/_/g, "/");
  //   //                     var jsonPayload = decodeURIComponent(
  //   //                       atob(base64)
  //   //                         .split("")
  //   //                         .map(function (c) {
  //   //                           return (
  //   //                             "%" +
  //   //                             ("00" + c.charCodeAt(0).toString(16)).slice(-2)
  //   //                           );
  //   //                         })
  //   //                         .join("")
  //   //                     );

  //   //                     return JSON.parse(jsonPayload);
  //   //                   }
  //   //                   if (data == "success") {
  //   //                     $(".alert_box").html(
  //   //                       '<div class="alert alert-success"> Success!  Product list successfuly </div>'
  //   //                     );
  //   //                     let parent_box = this_btn.parentElement.parentElement.remove();
  //   //                   } else {
  //   //                     $(".alert_box").html(
  //   //                       '<div class="alert alert-danger"> Opps! Somthing went wrong please try after sometimes</div>'
  //   //                     );
  //   //                   }

  //   //                   setTimeout(function () {
  //   //                     $(".alert_box").html("");
  //   //                   }, 5000);
  //   //                 },
  //   //               });
  //   //             }
  //   //           });
  //   //         });
  //   //       }
  //   //       delete_pro_in_course();
  //   //     },
  //   //   });
  //   // }
  // }
  // view_product_by_courseejct_in_add_pro_in_course();
  function load_all_faculty(){
    var sel_tag = $("#faculty-list");
    $.ajax({
      type: "POST",
      url:"php/faculty.php",
      data: {
        get_all:"get_all"
      },
      beforeSend: function(){
        $(".fac_loader").removeClass("d-none");
      },
      success: function (response) {
        $(".fac_loader").addClass("d-none");
        if(response !==""){
            if(response !== "error" || response !== "not found"){
              var obj = JSON.parse(response);
              $(sel_tag).html("");
              $(sel_tag).append("<option value='0'>  none </option>");
              for(let i=0; i < obj.length; i++){
                let name = obj[i].name;
                let id = obj[i].id;
                $(sel_tag).append("<option value="+id+"> "+name+" </option>");
              }
            }
        }
      }
    });
  };
  load_all_faculty();
  
});
