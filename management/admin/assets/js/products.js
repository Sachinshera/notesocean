$(document).ready(function () {
  function upload_data() {
    $(".add_produts_form").submit(function (event) {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "php/add_new_product.php",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () {
        $(".progress_box").removeClass("d-none");
        $(".upload_sbumit_btn").prop("disabled", true);
        },
        xhr: function () {
                var request = new XMLHttpRequest();
                request.upload.onprogress = function (event) {
                    var loaded = Math.round((event.loaded), 2);
                    var total = Math.round((event.total), 2);
                    var percent = Math.floor((loaded / total) * 100);
                    $(".progress-bar").css({
                        width: percent + "%"
                    });
                    $(".uploading_notice").html(percent + "%");
                    $(".upload_sbumit_btn").prop("disabled", true);

                }
                return request;
            },
        success: function (data) {
          $(".uploading_notice").html("0%");
                    $(".progress-bar").css({
                        width:"0%"
                    });
                    $(".upload_sbumit_btn").prop("disabled", false);
          $(".progress_box").addClass("d-none");
          if (data == "success") {
            swal("success!", "Product upload successfully", "success");
            $(".add_produts_form").trigger("reset");
          } else if (data == "exist") {
            swal("warning!", "Product already exist ", "warning");
          } else {
            swal("Error!", "Somthing went wrong, please try again later", "error");
          }
          setTimeout(function () {
            $(".alert_box").html("");
          }, 7000);
        },
      });
    });
  };
  upload_data();
  function get_all_data() {
    $.ajax({
      type: "POST",
      url: "php/manage_product.php",
      async: false,
      data: {
        get: "get_data",
      },
      beforeSend: function () {
        $(".table_loader").removeClass("d-none");
      },
      success: function (response) {
        $(".table_loader").addClass("d-none");
        $(".table_show").html("");
        var data = JSON.parse(response).result;
        var base64 = parseJwt(data);
        // extract data
        function parseJwt(token) {
          var base64Url = token.split(".")[1];
          var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
          var jsonPayload = decodeURIComponent(
            atob(base64)
              .split("")
              .map(function (c) {
                return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
              })
              .join("")
          );

          return JSON.parse(JSON.parse(jsonPayload));
        }
        // show data in page
        let count = 0;
        for (let i = 0; i < base64.length; i++) {
          count++;
          let product_name = base64[i].product_name;
          let id = base64[i].id;
          $(".table_show").append(
            "<tr> <td> " +
              count +
              " </td> <td> " +
              product_name +
              ' </td> <td> <i class="fa fa-edit pro_edit_icon" data-id="' +
              id +
              '"> </i> </td> <td> <i class="fa fa-trash pro_del_icon" data-id="' +
              id +
              '"> </i>  </td> </tr>'
          );
        }
        delete_product();
        edit_product();
      },
    });
  };
  get_all_data();

  function delete_product() {
    $(".pro_del_icon").each(function () {
      $(this).click(function () {
        let btn_id = $(this).attr("data-id");
        let confirm = window.confirm("Are you sure?");
        var this_btn = this;
        if (confirm) {
          $.ajax({
            type: "POST",
            url: "php/manage_product.php",
            data: {
              delete: "delete_data",
              id: btn_id,
            },
            beforeSend: function () {
            $(this_btn).removeClass("fa fa-trash");
            $(this_btn).addClass("fa fa-spinner");
            
            },
            success: function (response) {
              
              $(this_btn).removeClass("fa fa-spinner");
              $(this_btn).addClass("fa fa-trash");
            
              let response_data = JSON.parse(response).result;
              let final_data = parseJwt(response_data);
              function parseJwt(token) {
                var base64Url = token.split(".")[1];
                var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
                var jsonPayload = decodeURIComponent(
                  atob(base64)
                    .split("")
                    .map(function (c) {
                      return (
                        "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2)
                      );
                    })
                    .join("")
                );

                return JSON.parse(jsonPayload);
              }
              
              if (final_data == "success") {
                swal("success!", "Product  Deleted ", "success");
                let parent_box = this_btn.parentElement.parentElement.remove();
              } else {
                swal("error!", "somthing went wrong , please try after sometimes ", "error");
              }
              setTimeout(function () {
                $(".alert_box").html("");
              }, 7000);
            },
          });
        }
      });
    });
  };
  function edit_product() {
    $(".pro_edit_icon").each(function () {
      $(this).click(function () {
        let btn_id = $(this).attr("data-id");
        let this_btn = this;
        $.ajax({
          type: "POST",
          url: "php/manage_product.php",
          data: {
            get_data_id: btn_id,
          },
          beforeSend: function () {
            $(this_btn).removeClass("fa fa-edit");
            $(this_btn).addClass("fa fa-spinner fa-spin");
           
          },
          success: function (response) {    
            $(this_btn).removeClass("fa fa-spinner fa-spin");
            $(this_btn).addClass("fa fa-edit");
            var data = JSON.parse(response).result;
            var base64 = parseJwt(data);
            // extract data
            function parseJwt(token) {
              var base64Url = token.split(".")[1];
              var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
              var jsonPayload = decodeURIComponent(
                atob(base64)
                  .split("")
                  .map(function (c) {
                    return (
                      "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2)
                    );
                  })
                  .join("")
              );

              return JSON.parse(JSON.parse(jsonPayload));
            };
            for (let i = 0; i < base64.length; i++) {
              let product_name = base64[i].product_name;
              let short_desc = base64[i].short_desc;
              let long_desc = base64[i].long_desc;
              let keyword = base64[i].keyword;
              let pdf_url = base64[i].pdf_url;
              let image_url = base64[i].image_url;
              let created_date = base64[i].created_date;
              let updated_date = base64[i].updated_date;
              let id = base64[i].id;
              let course = base64[i].course;
              let sems = base64[i].sems;
              let sub = base64[i].sub;
              $.ajax({
                type:"POST",
                url:"php/get_sems_by_course.php",
                data:{
                  course:course
                }, beforeSend: function () {
                  $(".edit_sems_loader").removeClass("d-none");
                },
                success: function(response){
                  $(".edit_sems_loader").addClass("d-none");
                  $(".sems").html(response);
                  $(".sems").val(sems);
                }
              });
             $.ajax({
               type:"POST",
               url:"php/get_subject_by_course.php",
               data:{
                course:course
               },
               beforeSend:function(){},
               success:function(response){
                $(".subject_select").html(response);
                $(".subject_select").val(sub);
               }
             });
              $(".course_select_sub").val(course);
              $(".created_date").html(created_date);
              $(".updated_date").html(updated_date);
              $("#product_name").val(product_name);
              $("#up_short_description").html(short_desc);
              $("#long_description").val(long_desc);
              $("#keyword").val(keyword);
              $("#hidden_id").val(id);
              $(".modal").modal("show");
              update_produts_details();
            }
          },
        });
      });
    });
  };
  function reload_product_list() {
    $(".reload_btn").click(function () {
      get_all_data();
    });
  };
  function update_produts_details() {
    $(".update_produts_form").submit(function (event) {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: "php/update_product_details.php",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () {},
        success: function (response) {
          let response_data = JSON.parse(response).result;
         let data =  parseJwt(response_data);
          function parseJwt(token) {
            var base64Url = token.split(".")[1];
            var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
            var jsonPayload = decodeURIComponent(
              atob(base64)
                .split("")
                .map(function (c) {
                  return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
                })
                .join("")
            );

            return JSON.parse(jsonPayload);
          };
          // 
          get_all_data();
          if (data == "success") {
            swal("success!", "Product  Updated ", "success");
            $(".modal").modal("hide");
            } else {
              swal("Error!", "Somthing went wrong, please try again later", "error");
            }
        }
      });
    });
  };
  reload_product_list();
  function get_all_course() { 
    var sel_tag = $(".course_select_sub");
    $.ajax({
      type: "POST",
      url: "php/load_all_courses.php",
      beforeSend: function () {
        $(".course_loader").removeClass("d-none");
        $(sel_tag).prop("disabled",true);
      },
      success: function (response) {
        $(".course_loader").addClass("d-none");
        $(sel_tag).prop("disabled",false);
         $(sel_tag).html("");
        var data = JSON.parse(atob(JSON.parse(response).result.split(".")[1]));
        // after data processed
        $(sel_tag).append(
            '<option value="none"> None </option>'
          );
        for (let i = 0; i < data.length; i++) {
          var id = data[i].id;
          let name = data[i].table_name;
          $(sel_tag).append(
            '<option value="' +name + '"> ' + name + "  </option>"
          );
        }
      }
    });

  };
  get_all_course();
  function get_sub_by_course() { 
    $(".course_select_sub").on("change", function () {
      let course = $(this).val();
      $.ajax({
        type: "POST",
        url: "php/get_subject_by_course.php",
        data: {
          course:course
        }, beforeSend: function () {
          $(".sub_loader").removeClass("d-none");
          $(".subject_select").prop("disabled",true);
         },
        success: function (response) {
          $(".sub_loader").addClass("d-none");
          $(".subject_select").prop("disabled",false);
          $(".subject_select").html(response);
        }
      });
       $.ajax({
        type: "POST",
        url: "php/get_sems_by_course.php",
        data: {
          course: course
        },
        beforeSend: function () {
          $(".sems_loader").removeClass("d-none");
          $(".sems").prop("disabled",true);
         },
        success: function (response) {
          $(".sems_loader").addClass("d-none");
          $(".sems").prop("disabled",false);
          $(".sems").html(response);
        }
       });
      $(".sems").on("change", function () {
        let sems = $(this).val();
        $.ajax({
          type: "POST",
          url: "php/get_sub_by_sems.php",
          data: {
            sems: sems
          }, beforeSend: function () {
            $(".sub_loader").removeClass("d-none");
          $(".subject_select").prop("disabled",true);
           },
          success: function (response) {
             $(".sub_loader").addClass("d-none");
          $(".subject_select").prop("disabled",false);
             $(".subject_select").html(response);
          }
        });
      })
    });
    
  };
  get_sub_by_course();
});
