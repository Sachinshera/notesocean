$(document).ready(function () {
    function get_all_course_list() { 
    var sel_tag = $("#course_select");
    $.ajax({
      type: "POST",
      url: "php/load_all_courses.php",
      success: function (response) {
         $(sel_tag).html("");
        var data = JSON.parse(atob(JSON.parse(response).result.split(".")[1]));
        // after data processed
        var active_sems = data[0].table_name;
        sessionStorage.setItem("active_sem", active_sems);
        for (let i = 0; i < data.length; i++) {
          var id = data[i].id;
          let name = data[i].table_name;
          $(sel_tag).append(
            '<option value="' +name+ '"> ' + name + "  </option>"
          );
        }
      }
    });
    };
    get_all_course_list();
  function add_sems_input() {
    $(".add_sems").each(function () {
      $(this).click(function () {
        var dyn_box = $(".dyn_semester");
        $(dyn_box).append('<div class="input-group"><input type="text" placeholder="" class="form-control semster_selector"> <div class="input-group-append"><button class="input-group-text rm_sems" type="button"><i class="fa fa-times"> </i></button> </div></div>');
        $(".rm_sems").click(function () {
          let parent = this.parentElement.parentElement;
          parent.remove();
        });
      });
    });
   };
  add_sems_input();

  function upload_semseters() {
    $("#add_seemster").submit(function (event) {
      event.preventDefault();
      var sem_data = [];
      var course = $("#course_select").val();
      
      $(".semster_selector").each(function () {
        sem_data.push($(this).val());
      });
      $.ajax({
        type: "POST",
        url:"php/add_sems.php",
        data: {
          sems: sem_data,
          course:course
        },
        beforeSend: function () { },
        success: function (response) {
           $(".alert_box").html("");
          $(".alert_box").html(response);
          load_semster();
          setTimeout(function () {
            $(".alert_box").html("");
          }, 5000);
        }  
      });
    });
  };
  upload_semseters();
  function load_semster() {
    var course = sessionStorage.getItem("active_sem");
    $.ajax({
      type: "POST",
      url: "php/get_sem_by_course.php",
      data: {
        course: course
      },
      beforeSend: function () {
        $(".sems_list_table").html("loading....");
       },
      success: function (response) {
        var sem_box = $(".sems_list_table");
        $(".sems_list_table").html("");
        $(sem_box).html(response);
        // delet function start 
        $(".delete_icon").each(function () {
          $(this).click(function () {
            let name = $(this).attr("name");
            let course = $(this).attr("course");
            let confirm = window.confirm("Are you sure?");
            if (confirm) {
              $.ajax({
                type: "POST",
                data: {
                  course: course,name:name
                },
                url: "php/delete_sems.php",
                beforeSend: function () { },
                success: function (response) {
                  $(".alert_box").html("");
                  $(".alert_box").html(response);
                  load_semster();
                  setTimeout(function () {
            $(".alert_box").html("");
          }, 5000);
                }
              })
            }
          });
        });
        // delete function ends
      }
    });
  };
  load_semster();
  function change_sem_view() {
    $("#course_select").on("change", function () {
      var cr = $(this).val();
      sessionStorage.setItem("active_sem", cr);
      load_semster();
      
    });
  };
  change_sem_view();
  
  
});