 <aside class="sidebar" data-trigger="scrollbar">
     <div class="sidebar--nav">
         <ul>
             <li>
                 <ul>
                 <li>
                        <a href="faculty.php">
                         <i class="fa fa-university" style="color:red"></i>
                             <span>Faculty </span>
                         </a>
                         <ul>
                             <li><a href="add_faculty.php">Add faculty</a></li>
                             <li><a href="manage_faculty.php">Manage faculty</a></li>
                             
                         </ul>
                     </li>

                     <li>
                         <a href="#">
                             <i class="fa fa-graduation-cap" style="color:blue"></i>
                             <span>Courses</span>
                         </a>

                         <ul>
                             <li><a href="add_course.php"> Add Course</a></li>
                             <li><a href="manage_course.php"> Manage Course</a></li>
                            
                         </ul>
                     </li>
                     <li>
                         <a href="#">
                             <i class="fa fa-book"  style="color:pink"></i>
                             <span>Subjects</span>
                         </a>

                         <ul>
                             <li><a href="add_subject.php">Add Subject</a></li>
                             <li><a href="manage_subject.php">Manage Subject</a></li>
                         </ul>
                     </li>
                     <li>
                         <a href="#">
                             <i class="fa fa-file-pdf"  style="color:yellow"></i>
                             <span>Notes</span>
                         </a>

                         <ul>
                             <li><a href="add_notes.php"> Add New Notes</a></li>
                             <li><a href="manage_notes.php"> Manage Notes</a></li>
                         </ul>
                     </li>
                     <li>
                         <a href="#">
                             <i class="fa fa-user"  style="color:pink"></i>
                             <span>User management</span>
                         </a>
                         <ul>
                             <li><a href="payment_request.php"> Payment Request </a></li>
                             <li><a href="users.php"> Users </a></li>
                         </ul>
                     </li>
                 </ul>
             </li>
         </ul>
     </div>
 </aside>