<?php session_start();
if ($_SESSION["valid_user"] == "Admin") {
} else {
    header("Location:index.php");
} ?>
<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Add Subject</title>
    <?php require("assets/includes/cdncss.php"); ?>
    <style type="text/css">
        .title {
            color: #000;
        }

        .title h1 {
            font-weight: bold;
        }

        .public {
            color: red;
        }

        .itm {
            background-color: #fff;
            padding: 15px;
            margin: 15px 10px;
            border-radius: 10px;
        }

        .file p {
            color: red;
        }

        .file_span {
            color: #000;
        }

        .basic {
            padding: 20px 30px 100px 0px;
            margin: 15px 10px;
            color: #000;
        }

        .star {
            color: red;
        }

        .basic p {
            margin: 0;
        }

        .basic select {
            border: 1px solid red;
            border-radius: 5px;
            width: 290px;
            padding: 5px;
            margin-bottom: 20px;
            margin-right: 30px;
        }

        .btn_save {
            background-color: red;
            color: #fff;
            border: none;
            padding: 15px 25px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        .new_area {
            height: auto !important;
        }

        .status {
            width: 100%;
            border-color: #ebebea;
            padding: 8px 20px;
            border-radius: 5px;
            color: #495057;
        }
    </style>

</head>

<body>

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php require("assets/includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->


        <main class="main--container">

            <section class="main--content new_area">
                <div class="title">
                    <h1>Add New <span class="public">Subject</span></h1>
                </div>

                <form>
                    <div class="form-group">
                        <label for="addsubject">Subject Name<span class="star">*</span></label>
                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject Name">
                    </div>
                    <div class="form-group">
                        <label for="subject_description">Subject Description <span class="star">*</span></label>
                            <textarea id="editor" name="subject_description">
                </textarea>
                       
                    </div>
                    <div class="form-group">
                        <label for="subjecttag">Subject Tags<span class="star">*</span></label>
                        <input type="text" class="form-control" id="tags" name="subjecttag" placeholder="Enter Subject Tag">
                    </div>
                    <div class="form-group">
                        <label for="select_faculty">Select Faculty<span class="star">*</span></label>
                        <select class="form-select status" aria-label="Default select example" id="faculty">
                            <option selected>None</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Select Course<span class="star">*</span></label>
                        <select class="form-select status" aria-label="Default select example" id="course">
                            <option value="0">None</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Select Semester<span class="star">*</span></label>
                        <select class="form-select status" aria-label="Default select example" id="semester">
                            <option value="0">None</option>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
                            <option value="3">Semester 3</option>
                            <option value="4">Semester 4</option>
                            <option value="5">Semester 5</option>
                            <option value="6">Semester 6</option>
                            <option value="7">Semester 7</option>
                            <option value="8">Semester 8</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Status<span class="star">*</span></label>
                        <select class="form-select status" aria-label="Default select example" id="status">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                        </select>
                    </div>
                    <button class="btn_save btn btn-primary btn-sm" disabled="true">Save</button>
                </form>
    </div>
    </section>
    </main>
    <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->

    <script type="text/javascript">
        CKEDITOR.replace('editor');
    </script>
    <?php require("assets/includes/cdnjs.php"); ?>

    <!-- script -->
    <script>
        //   preloaded data start 
        $(document).ready(function() {
            function getAllFaculty() {
                let sel_box = $("#faculty");
                $.ajax({
                    type: "POST",
                    url: "php/faculty.php",
                    data: {
                        for: "get_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                        $(".btn_save").prop("disabled",true);
                    },
                    success: function(response) {
                        $(".btn_save").prop("disabled",false);
                        $(sel_box).html("");
                        $(sel_box).append("<option value='0'> None </option>");
                        let data = response.data;
                        if (response.status == "success") {
                            
                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                            getAllCourse();
                        }
                    }
                })
            };
            getAllFaculty();

            function getAllCourse() {
                let sel_box = $("#course");
                $.ajax({
                    type: "POST",
                    url: "php/course.php",
                    data: {
                        for: "load_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                        $(".btn_save").prop("disabled",true);
                    },
                    success: function(response) {
                        $(".btn_save").prop("disabled",false);
                        $(sel_box).html("");
                        $(sel_box).append("<option value='0'> None </option>");
                        let data = response.data;
                        if (response.status == "success") {
                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].table_name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                        }
                    }
                })
            };
        });
        //   preloaded data end

        $(document).ready(function() {
            function addSubject() {
                $("form").submit(function(event) {
                    event.preventDefault();
                    let subject_name = $("#subject").val();
                    let subject_tags = $("#tags").val();
                    let subject_faculty = $("#faculty").val();
                    let subject_course = $("#course").val();
                    let subject_semester = $("#semester").val();
                    let subject_status = $("#status").val();
                    var objEditor = CKEDITOR.instances["editor"];
                    var description = objEditor.getData();
                    if (subject_name.trim().length !== 0 && description.trim().length !== 0 && subject_tags.trim().length !== 0) {
                        $.ajax({
                            type: "POST",
                            url: "php/subject.php",
                            data: {
                                for: "add",
                                subject_name: subject_name,
                                subject_tags: subject_tags,
                                subject_faculty: subject_faculty,
                                subject_course: subject_course,
                                subject_semester: subject_semester,
                                subject_status: subject_status,
                                description: description
                            },
                            beforeSend: function() { 
                                swal({
                                    text: "Processing....",
                                    button: false,
                                    allowEscapeKey: false,
                                    allowOutsideClick: false,
                                    closeOnConfirm: false,
                                    closeOnClickOutside: false
                                });
                                $(".btn_save").prop("disabled",true);
                            },
                            success: function(response) {
                                $(".btn_save").prop("disabled",false);
                                swal.close();
                              if(response.status=="success"){
                                  swal("success","subject added","success");
                              }
                              else if(response.status=="exist"){
                                swal("warning","this subject is already exist","warning");
                              }
                              else{
                                swal("error","somthing went wrong,please try again","error");
                              }
                            }
                        })
                    } else {
                        swal("warning", "All field are required", "warning");
                    }
                })
            };
            addSubject();
        })
    </script>
    <!-- script -->

</body>

</html>