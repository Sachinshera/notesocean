<?php 
require "php/database.php";
session_start();
if ($_SESSION["valid_user"] == "Admin") {
} else {
    header("Location:index.php");
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Users</title>
    <?php require("assets/includes/cdncss.php"); ?>
</head>
<body>

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php require("assets/includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->


        <div class="main--container">
        <section class="main--content">
                <div class="panel">
                    <div class="panel-heading">
                      <h3>All users</h3>
                    </div>

                    <div class="panel-content">
                       <table class="table table-hover">
                           <thead class="thead">
                               <tr>
                                   <th>#</th>
                                   <th>Name</th>
                                   <th>Email</th>
                                   <th>Joining Date</th>
                                   <th>Status</th>
                               </tr>
                           </thead>
                           <tbody>
                               <?php 
                               
                            //    get all user info 

                            $get_data = $user_db->query("SELECT * FROM profiles ORDER BY id DESC");
                            if($get_data){
                                if($get_data->num_rows!==0){
                                    $counter = 1;
                                    while($row = $get_data->fetch_assoc()){
                                        $counter++;
                                        $fullname= $row["fullname"];
                                        $email = $row["email"];
                                        $registerdDate = $row["registerdDate"];
                                        $status = $row["status"];
                                        echo ' <tr>
                                        <td>'.$counter.'</td>
                                        <td>'.$fullname.'</td>
                                        <td>'.$email.'</td>
                                        <td>'.$registerdDate.'</td>
                                        <td>'.$status.'</td>
                                    </tr>';
                                    }
                                }
                            }
                               
                               
                               ?>
                               <!-- <tr>
                                   <td>1</td>
                                   <td>Sachin</td>
                                   <td>Sachinkumarshera@gmail.com</td>
                                   <td>234325</td>
                                   <td>Active</td>
                               </tr> -->
                           </tbody>
                       </table>
                    </div>
                </div>
            </section>

      

        </div>
    <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->


    <?php require("assets/includes/cdnjs.php"); ?>
    <!-- script -->
          
    <!-- script -->

</body>
</html>