<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Payment request</title>
    <?php require("assets/includes/cdncss.php"); ?>
    <!-- css here  -->
    <style type="text/css">
        h1,
        h2,
        h4,
        h4,
        h5,
        h6,
        td {
            color: #444;
        }

        h2 {
            font-weight: bold;
            font-size: 1.3rem;
            color: #000;
        }

        table {
            width: 100%;
            font-size: 1rem;
            font-weight: 600;

        }

        table tr td {
            padding: 8px 0;
        }

        .payment_request,
        .user_profile {
            /*box-shadow: 2px 2px 3px rgba(0,0,0,0.5);*/
            box-shadow: 1px 1px 5px 1px rgba(0, 0, 0, 0.58);
            padding: 15px;
        }

        .number {
            color: red;
        }

        .user {
            line-height: 5px;
        }

        .user h3 {
            font-size: 1.2rem;
            font-weight: bold;
        }

        .points {
            line-height: 10px;
        }

        .user_notes {
            font-size: 1.2rem;
            font-weight: bold;
            margin-left: 15px;
        }

        .notes table {
            font-size: 0.9rem !important;
        }

        .tr_border {
            border: 2px solid rgba(0, 0, 0, 0.3);

        }

        .tr_border td {
            padding: 7px 10px;
        }

        .btn {
            padding: 4px 20px !important;
        }

        @media (max-width:767px) {

            table {
                padding: 0;
                font-size: .7rem;
                line-height: 15px;
            }

            h4 {
                font-size: 0.9rem;
                line-height: 15px;
            }

            .btn {
                padding: 1px 10px !important;
                font-size: 8px;
            }

            .panel-content,
            .main--content {
                padding: 0;
            }

            .payment_request,
            .user_profile {
                padding: 10px;
            }

            .user {
                text-align: center;
            }
        }
    </style>

    <!--css here  -->
</head>

<body>

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php require("assets/includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->

        <!-- Main Container Start -->
        <main class="main--container bg-white">

            <section class="main--content">
                <div class="panel">
                    <div class="panel-content p-0 border-0 shadow-0">
                        <!--  code here -->

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 mt-2 mb-4">
                                    <div class="payment_request text-center">
                                        <h2> Payment Requet </h2>
                                        <hr>
                                        <table class="text-center table table-responsive-md">
                                            <thead class="text-center">
                                                <tr>
                                                    <th>#</th>
                                                    <th>User id</th>
                                                    <th>Points</th>
                                                    <th class="border-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="pending_list">
                                                <tr>
                                                    <td>1. </td>
                                                    <td>Sajib Biswas </td>
                                                    <td> 1500 Pts </td>
                                                    <td class="d-md-flex justify-content-md-end">
                                                        <button class="btn btn-success  mr-md-2">Approve</button>
                                                        <button class="btn btn-danger">Profile</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>


                                <div class="col-md-6 mt-2">
                                    <div class="loading_profile text-center d-none">
                                        <h1>

                                            <i class="fa fa-spinner fa-spin"></i> Loading...
                                        </h1>

                                    </div>
                                    <div class="user_profile d-none">
                                        <h2 class="text-center"> User Profile </h2>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4 text-center">
                                                <img src="assets/img/avatars/01_80x80.png" class="rounded-circle user_pic">
                                            </div>
                                            <div class="col-md-8 user">
                                                <h3 class="fullname"> Sajib Biswas </h3>
                                                <p class="email">sajib@gmail.com</p>

                                            </div>

                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6 points mt-2 mb-2">
                                                <p> Total Public Notes: <span class="total_notes"> 10 </span> </p>
                                                <p> Total Dislikes: <span class="dislikes"> 10 </span> </p>
                                            </div>
                                            <div class="col-md-6 points mt-2">
                                                <p> Total Likes: <span class="likes"> 10 </span> </p>
                                                <p> Total Points: <span class="total_points"> 10 </span> </p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <h3 class="user_notes"> User Public Notes </h3>
                                            <div class="col-md-12 notes" style="height: 200px !important;overflow-y:scroll">
                                                <table class="user_notes_box table table-responsive-md">
                                                    <!-- <tr class="">
                                                        <td>1. </td>
                                                        <td> Use Bootstrap’s custom button styles </td>
                                                        <td><button class="btn btn-info">Check</button></td>
                                                    </tr>
                                                    <tr class="">
                                                        <td>2. </td>
                                                        <td> Use Bootstrap’s custom button styles </td>
                                                        <td><button type="button" class="btn btn-info">Check</button></td>
                                                    </tr> -->
                                                </table>
                                            </div>


                                        </div>
                                        <hr>
                                        <h4>Requested Payment Points:<span class="req_points">1500 Pts</span> </h4>
                                        <h4>Account ID:<span class="account_id">sajib@gmail.com</span> </h4>
                                        <h4>Payable Amount :<span class="payabale_amount text-danger">00</span> Rupees </h4>
                                        <h4>Payment Method  :<span class="payment_method text-danger">  </span></h4>
                                        <div class="row mt-lg-5 mt-sm-3 mb-5">
                                            <div class="col-6 text-center mt-2">
                                                <button class="btn btn-success profile_approve_btn"> Approve </button>
                                            </div>
                                            <div class="col-6 text-center mt-2">
                                                <button class="btn btn-danger reject_btn"> Reject </button>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!--  code here -->
                    </div>
                </div>
            </section>
        </main>
        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->

    <!-- Scripts -->
    <?php require("assets/includes/cdnjs.php"); ?>

    <!-- js here  -->

    <script>
        $(document).ready(() => {
            function get_all_pending_payment() {
                var pending_list_box = $(".pending_list");
                $(pending_list_box).html("");
                $.ajax({
                    type: "POST",
                    url: "php/payment.php",
                    data: {
                        for: "get_all_pending_payment"
                    },
                    beforeSend: () => {
                        $(pending_list_box).html('<div class="alert alert-info"> <i class="fa fa-spinner fa-spin"> </i> Loading... </div>');
                    },
                    success: (result) => {
                        // result = JSON.parse(result);
                        $(pending_list_box).html("");
                        if (result.status == "nodata") {
                            $(pending_list_box).html('<div class="alert alert-warning"> No pending payment </div>');
                        } else if (result.status == "success") {
                            let pending_data = result.data;
                            let counter = 0;
                            for (let i = 0; i < pending_data.length; i++) {
                                counter++;
                                let id = pending_data[i].id;
                                let user_id = pending_data[i].user_id;
                                let points = pending_data[i].points;
                                let date_time = pending_data[i].date_time;
                                let method = pending_data[i].method;
                                let status = pending_data[i].status;
                                let account_id = pending_data[i].account_id;
                                $(pending_list_box).append('<tr><td> ' + counter + ' </td><td> ' + user_id + '</td><td> ' + points + ' Pts </td><td class="d-md-flex justify-content-md-end"><button class="btn btn-sm btn-danger profile_btn" data-id="' + user_id + '" data-method= "'+method+'" account-id="' + account_id + '" data-points="' + points + '" data-pending-id="' + id + '">Profile</button></td></tr>');
                            }
                            show_profile();
                            approve_payment();
                        }
                    }
                })
            };

            function show_profile(account_id, points,method) {

                $(".profile_btn").each(function() {
                    $(this).click(() => {
                        let data_id = $(this).attr("data-id");
                        var account_id = $(this).attr("account-id");
                        var points = $(this).attr("data-points");
                        var pending_id = $(this).attr("data-pending-id");
                        var method = $(this).attr("data-method");
                        sessionStorage.setItem("pend", pending_id);
                        $(".user_profile").addClass("d-none");
                        $.ajax({
                            type: "POST",
                            url: "php/payment.php",
                            data: {
                                for: "user_pofile_data",
                                user_id: data_id
                            },
                            beforeSend: () => {
                                $(".loading_profile").removeClass("d-none");
                            },
                            success: (result) => {
                                if (result.status == "success") {
                                    $(".user_profile").removeClass("d-none");
                                    $(".loading_profile").addClass("d-none");
                                    //    user data 
                                    let user_data = result.profile_data;
                                    let fullname = user_data.fullname;
                                    let email = user_data.email;
                                    let picture = user_data.picture;
                                    $(".user_pic").attr("src", picture);
                                    $(".fullname").html(fullname);
                                    $(".email").html(email);

                                    //    user data  end 

                                    //    points data 
                                    let reaction_views_data = result.reaction_views_data;
                                    let total_views = Number(reaction_views_data.total_views);
                                    let total_notes = Number(reaction_views_data.total_notes);
                                    let total_likes = Number(reaction_views_data.total_likes);
                                    let total_dislike = Number(reaction_views_data.total_dislike);
                                    let total_points = Number((total_views * 5) + (total_likes * 10) - (total_dislike * 2));
                                    $(".total_notes").html(total_notes);
                                    $(".dislikes").html(total_dislike);
                                    $(".likes").html(total_likes);
                                    $(".total_points").html(total_points);
                                    $(".payabale_amount").html(points/100);
                                    $(".payment_method").html(method);
                                    //    points data end 
                                    // user notes data 
                                    let notes_data = result.notes_data;
                                    let user_notes_list_box = $(".user_notes_box");
                                    $(".user_notes_box").html("");
                                    let counter = 0;
                                    for (let i = 0; i < notes_data.length; i++) {
                                        counter++;
                                        let title = notes_data[i].title;
                                        let url = notes_data[i].url;
                                        $(user_notes_list_box).append('<tr class=""><td> ' + counter + ' </td><td> ' + title + ' </td><td> <a href="' + url + '" target="_blank"> <button class="btn btn-info">Check</button> </a> </td></tr>');
                                    }
                                    $(".req_points").html(points);
                                    $(".account_id").html(account_id);
                                }
                            }
                        });
                    })
                })
            }
            get_all_pending_payment();

            function reject() {

                $(".reject_btn").click(() => {
                    let pending_id = sessionStorage.getItem("pend");
                    swal({
                        icon: "info",
                        title: "Are you sure?",
                        text: "please reject when you have valid reason",
                        buttons: ["Cancel", "confirm"],
                        dangerMode: true
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "php/payment.php",
                                data: {
                                    for: "reject_process",
                                    pending_id: pending_id
                                },
                                beforeSend: () => {
                                    swal({
                                        text: "Processing....",
                                        button: false,
                                        allowEscapeKey: false,
                                        allowOutsideClick: false,
                                        closeOnConfirm: false,
                                        closeOnClickOutside: false
                                    });
                                },
                                success: (result) => {
                                    swal.close();
                                    if (result.status == "success") {
                                        swal("success", "Action performed", "success");
                                        get_all_pending_payment();
                                        $(".user_profile").addClass("d-none");
                                    } else {
                                        swal("error", "somthing went wrong , please try after sometimes", "error");
                                    }
                                }
                            })
                        }
                    });
                });
            };
            reject();

            $(".profile_approve_btn").click(()=>{
                let pending_id = sessionStorage.getItem("pend");
                send_approve_peyment(pending_id);
            })
            function approve_payment() {
                $(".approve_btn").each(function() {
                    $(this).click(function() {
                        let pending_id = $(this).attr("data-id");
                        send_approve_peyment(pending_id);
                    });
                })
            };
            function send_approve_peyment(pending_id){
                swal({
                        icon: "info",
                        title: "Are you sure?",
                        text: "Please approve when you have made the payment",
                        buttons: ["Cancel", "confirm"],
                        dangerMode: false
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url: "php/payment.php",
                                data: {
                                    for: "approve_process",
                                    pending_id: pending_id
                                },
                                beforeSend: () => {
                                    swal({
                                        text: "Processing....",
                                        button: false,
                                        allowEscapeKey: false,
                                        allowOutsideClick: false,
                                        closeOnConfirm: false,
                                        closeOnClickOutside: false
                                    });
                                },
                                success: (result) => {
                                   
                                    swal.close();
                                    if (result.status =="success") {
                                        swal("success", "Action performed", "success");
                                        get_all_pending_payment();
                                        $(".user_profile").addClass("d-none");
                                    } else {
                                        swal("error", "somthing went wrong , please try after sometimes", "error");
                                    }
                                }
                            })
                        }
                    });
            };

        });
    </script>
    <!-- js here -->

</body>

</html>