<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Add faculty</title>
    <?php require("assets/includes/cdncss.php"); ?>
    <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>
    <style type="text/css">
        .title {
            color: #000;
        }

        .title h1 {
            font-weight: bold;
        }

        .public {
            color: red;
        }

        .itm {
            background-color: #fff;
            padding: 15px;
            margin: 15px 10px;
            border-radius: 10px;
        }

        .file p {
            color: red;
        }

        .file_span {
            color: #000;
        }

        .basic {
            padding: 20px 30px 100px 0px;
            margin: 15px 10px;
            color: #000;
        }

        .star {
            color: red;
        }

        .basic p {
            margin: 0;
        }

        .basic select {
            border: 1px solid red;
            border-radius: 5px;
            width: 290px;
            padding: 5px;
            margin-bottom: 20px;
            margin-right: 30px;
        }

        .btn_save {
            background-color: red;
            color: #fff;
            border: none;
            padding: 15px 25px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        .new_area {
            height: auto !important;
        }

        .status {
            width: 100%;
            border-color: #ebebea;
            padding: 8px 20px;
            border-radius: 5px;
            color: #495057;
        }
    </style>

</head>

<body>

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php require("assets/includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->


        <main class="main--container">

            <section class="main--content new_area bg-white">
                <div class="title">
                    <h1>Add New <span class="public">Faculty</span></h1>
                </div>

                <form>
                    <div class="form-group">
                        <label for="addfaculty">Add Faculty Name<span class="star">*</span></label>
                        <input type="text" class="form-control" id="faculty" name="faculty" placeholder="Enter Faculty Name">
                    </div>
                    <div class="form-group">
                        <label for="faculty_description">Facult Description <span class="star">*</span></label>
                        <form>
                            <textarea id="editor" name="faculty_description"> </textarea>
                        </form>
                    </div>
                    <div class="form-group">
                        <label for="facultytag">Faculty Tags<span class="star">*</span></label>
                        <input type="text" class="form-control" id="tag" name="tag" placeholder="Enter Faculty Tag">
                    </div>
                    <div class="form-group">
                        <label for="facultystatus">Faculty Status<span class="star">*</span></label>
                        <select class="form-select status" aria-label="Default select example">
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                        </select>
                    </div>


                    <button class="btn_save">Save</button>
                </form>

    </div>

    <script type="text/javascript">
        CKEDITOR.replace('editor');
    </script>


    <script>
        var acc = document.getElementsByClassName("toggle");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    </script>



    </section>
    <?php include("assets/includes/footer.php"); ?>
    </main>
    <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->


    <?php require("assets/includes/cdnjs.php"); ?>
    <!-- script -->
    <script>
        $(document).ready(function() {
            // add new faculty
            function addFaculty() {
                $(".btn_save").click(function() {
                    var name = $("#faculty").val();
                    var tags = $("#tag").val();
                    var status = $(".status").val();
                    var objEditor =CKEDITOR.instances["editor"];
                    var description = objEditor.getData();
                    if (name == "" || description == "" || tags == "") {
                        swal("warning", "All field are required", "warning");
                    } else {
                        $.ajax({
                            type:"POST",
                            url:"php/faculty.php",
                            data:{
                                for:"add",
                                faculty_name:name,
                                faculty_desc:description,
                                faculty_status:status,
                                tags:tags
                            },
                            beforeSend:function(){
                                swal({
                                    text:"Processing....",
                                    button:false,
                                    allowEscapeKey: false,
                                    allowOutsideClick:false,
                                    closeOnConfirm:false,
                                    closeOnClickOutside: false
                                });
                            },
                            success:function(data){
                              swal.close();
                              console.log(data);
                              if(data.status=="success"){
                                  swal("success","Faculty added","success");
                              }
                              else if(data.status=="exist"){
                                swal("warning","this faculty is already exist","warning");
                              }
                              else{
                                swal("error","somthing went wrong,please try again","error");
                              }
                            }
                        })
                    }

                });

            };
            addFaculty();
        });
    </script>
    <!-- script -->

</body>

</html>