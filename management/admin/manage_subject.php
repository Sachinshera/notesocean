<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Manage Subject</title>
    <?php require("assets/includes/cdncss.php"); ?>
    <style type="text/css">
        .action_btn:hover {
            cursor: pointer;
        }

        .title {
            color: #000;
        }

        .title h1 {
            font-weight: bold;
        }

        .star {
            color: red;
        }

        .basic p {
            margin: 0;
        }

        .basic input {
            border: 1px solid red;
            border-radius: 5px;
            width: 350px;
            padding: 5px;
            margin-bottom: 20px;
        }

        .basic select {
            border: 1px solid red;
            border-radius: 5px;
            width: 350px;
            padding: 5px;
            margin-bottom: 20px;
        }

        .btn_save {
            background-color: red;
            color: #fff;
            border: none;
            padding: 15px 25px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        .new_area {
            height: auto !important;
        }

        .main--content {
            padding: 10px 10px !important;
        }

        form.example input[type=text] {
            padding: 10px;
            font-size: 17px;
            border: 1px solid red;
            float: left;
            width: 80%;
            background: #fff;
            border-radius: 10px 0 0 10px;
        }

        form.example button {
            float: left;
            width: 20%;
            padding: 10px;
            background: red;
            color: white;
            font-size: 17px;
            border: 1px solid grey;
            border-left: none;
            cursor: pointer;
            border-radius: 0 10px 10px 0;
        }

        form.example button:hover {
            background: #0b7dda;
        }

        form.example::after {
            content: "";
            clear: both;
            display: table;
        }

        .table {
            color: #000;
            text-align: center;
        }

        .modal.fade .modal-bottom,
        .modal.fade .modal-left,
        .modal.fade .modal-right,
        .modal.fade .modal-top {
            position: fixed;
            z-index: 1055;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: 0;
            max-width: 100%
        }

        .modal.fade .modal-right {
            left: auto !important;
            transform: translate3d(100%, 0, 0);
            transition: transform .3s cubic-bezier(.25, .8, .25, 1)
        }

        .modal.fade.show .modal-bottom,
        .modal.fade.show .modal-left,
        .modal.fade.show .modal-right,
        .modal.fade.show .modal-top {
            transform: translate3d(0, 0, 0)
        }

        .w-xl {
            width: 320px
        }

        .modal-content,
        .modal-footer,
        .modal-header {
            border: none
        }

        .list-group.no-radius .list-group-item {
            border-radius: 0 !important
        }

        .btn-light {
            color: #212529;
            background-color: #f5f5f6;
            border-color: #f5f5f6
        }

        .btn-light:hover {
            color: #212529;
            background-color: #e1e1e4;
            border-color: #dadade
        }

        .modal-footer {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            padding: 1rem;
            border-top: 1px solid rgba(160, 175, 185, .15);
            border-bottom-right-radius: .3rem;
            border-bottom-left-radius: .3rem
        }
    </style>
    <style>
        .context_btn:hover {
            cursor: pointer;
        }

        .context_buttons {
            padding: 10px 10px;
        }

        .context_buttons .row {
            padding: 5px 0px;
        }

        .context_buttons .row:hover {
            cursor: pointer;
            box-shadow: 0px 0px 3px #ccc;
            border-radius: 10px;
            color: red;

        }

        #contxt {
            position: absolute;
            z-index: 20 !important;
            background-color: white;
            top: 0;
            left: 0;
        }

        .context_show {
            display: block !important;
        }

        .context_hide {
            display: none !important;
        }

        .context_blur {
            width: 100% !important;
            height: 100vh !important;
            background: rgba(0, 0, 0, 0.2);
            position: absolute;
            top: 0;
            left: 0;
            z-index: 10;
        }
    </style>
</head>

<body>
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php require("assets/includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->


        <main class="main--container bg-white">
            <!-- main content -->
            <section class="main--content new_area">
                <div class="title table-hover table-responsive">
                    <h1>Manage <span class="public">Subject</span></h1>
                </div>
                <form class="example">
                    <input type="text" placeholder="Search Subject ..." name="search" id="search_box">
                    <button type="submit"><i class="fa fa-search" id="option"></i></button>
                </form>
                <div class="container" style="height:70vh;overflow-y:scroll">
                    <table class="table table-hover table-responsive-lg">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Faculty</th>
                                <th>Course</th>
                                <th>Status</th>
                                <th>Published Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="subject_show_box">
                        
                        </tbody>
                        
                    </table>
                    <div class="laoding_screen text-center">
                        <h1> <i class="fa fa-spinner fa-spin"> </i> loading.....</h1>
                        </div>
                </div>
            </section>
            <div id="contxt" class="context_hide" style="box-shadow:0px 0px 8px -1px #ccc;border-radius:10px;padding:10px  10px;text-align:center;width:180px">
                <small>Options</small>
                <div class="context_buttons mt-1 w-100 text-left">


                    <div class="row  my-2 edit_subject_btn">
                        <div class="col-2">
                            <i class="fa fa-pen-square"></i>
                        </div>
                        <div class="col-8"> Edit </div>
                    </div>

                    <div class="row  my-2 subject_delete_btn">
                        <div class="col-2">
                            <i class="fa fa-trash-alt"></i>
                        </div>
                        <div class="col-8"> Delete </div>
                    </div>
<!-- 
                    <div class="row  my-2">
                        <div class="col-2">
                            <i class="fas fa-eye-slash"></i>
                        </div>
                        <div class="col-8"> Inactive </div>
                    </div> -->

                </div>
            </div>

            <div class="context_blur context_hide"> </div>

            <!-- main content -->
            <!--  edit subject modal  start  -->
            <div id="modal-top" class="modal fade" data-backdrop="true">
                <div class="modal-dialog modal-top">
                    <div class="modal-content no-radius">
                        <div class="modal-header">
                            <div class="modal-title text-md">Edit <span class="subject_name_header text-danger"> </span> </div> <button class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="height:80vh;overflow-y:scroll">
                            <form>
                                <div class="form-group">
                                    <label for="addsubject">Add Subject Name<span class="star">*</span></label>
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject Name">
                                </div>
                                <div class="form-group">
                                    <label for="subject_description">Subject Description <span class="star">*</span></label>
                                   
                                        <textarea id="editor" name="subject_description"> </textarea>
                                    
                                    <label for="faculty">Faculty<span class="star">*</span></label>
                                    <select class="form-select faculty form-control" aria-label="Default select example" id="faculty">
                                        <option value="0">None</option>
                                    </select>
                                </div>



                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <div class="form-group">
                                            <label for="status">Select Course<span class="star">*</span></label>
                                            <select class="form-select status form-control" aria-label="Default select example" id="course">
                                                <option selected value="0">None</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="form-group">
                                            <label for="status">Select Semester<span class="star">*</span></label>
                                            <select class="form-select status form-control" aria-label="Default select example" id="semester">
                                                <option value="0">None</option>
                                                <option value="1">Semester 1</option>
                                                <option value="2">Semester 2</option>
                                                <option value="3">Semester 3</option>
                                                <option value="4">Semester 4</option>
                                                <option value="5">Semester 5</option>
                                                <option value="6">Semester 6</option>
                                                <option value="7">Semester 7</option>
                                                <option value="8">Semester 8</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <div class="form-group">
                                            <label for="subjecttag">Subject Tags<span class="star">*</span></label>
                                            <input type="text" class="form-control" id="tag" name="tag" placeholder="Enter Subject Tag">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <div class="form-group">
                                            <label for="subjectstatus">Subject Status<span class="star">*</span></label>
                                            <select class="form-select status form-control" aria-label="Default select example" id="status">
                                                <option value="active">Active</option>
                                                <option value="inactive">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn_save btn btn-primary">Save changes</button>
                            </form>
                        </div>
                        <div class="modal-footer"><button type="button" class="btn btn-light" data-dismiss="modal">Close</button></div>
                    </div>
                </div>
            </div>
            <!-- edit subject modal end -->

        </main>
        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->


    <?php require("assets/includes/cdnjs.php"); ?>
    <!-- script -->
    <script>
        function show() {
            $("#contxt").removeClass("context_hide");
            $("#contxt").addClass("context_show");
            $(".context_blur").addClass("context_show");
            $(".context_blur").removeClass("context_hide");
        }

        function hide() {
            $("#contxt").addClass("context_hide");
            $("#contxt").removeClass("context_show");
            $(".context_blur").removeClass("context_show");
            $(".context_blur").addClass("context_hide");
        }

        function context() {

            $(".action_btn").each(function() {
                $(this).click(function(event) {
                    var parent = this.parentElement.parentElement;
                    var left = event.pageX - 150 + "px";
                    var top = event.pageY - 50 + "px";
                    $("#contxt").css({
                        left: left,
                        top: top
                    });

                    let data_id = $(parent).attr("data-id");
                    sessionStorage.setItem("active_subject_id", data_id);
                    show();
                });
            });

            $(".subject_row").each(function() {
                $(this).on("contextmenu", function(event) {
                    var parent = this;
                    var left = event.pageX - 150 + "px";
                    var top = event.pageY - 50 + "px";
                    $("#contxt").css({
                        left: left,
                        top: top
                    });
                    show();
                    let data_id = $(parent).attr("data-id");
                    sessionStorage.setItem("active_subject_id", data_id);
                    return false;
                });
            });

            $(".context_blur").click(function() {
                hide();
            });

        }
        context();
    </script>
    <script type="text/javascript">
        CKEDITOR.replace('editor');
    </script>
    <script>
        $(document).ready(function() {
            function getAllFaculty() {
                let sel_box = $("#faculty");
                $.ajax({
                    type: "POST",
                    url: "php/faculty.php",
                    data: {
                        for: "get_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            $(sel_box).html("");
                            $(sel_box).append("<option value='0'> None </option>");
                            let data = response.data;
                            sessionStorage.setItem("faculty_list", JSON.stringify(response));
                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                            // load courses
                            getAllCourse();
                        }
                    }
                })
            };
            getAllFaculty();

            function getAllCourse() {
                let sel_box = $("#course");
                $.ajax({
                    type: "POST",
                    url: "php/course.php",
                    data: {
                        for: "load_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            sessionStorage.setItem("course_list", JSON.stringify(response));
                            let data = response.data;
                            $(sel_box).html("");
                            $(sel_box).append("<option value='0'> None </option>");
                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].table_name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                            load_all();
                        }
                    }
                })
            };

            // get all subject 
            function load_all() {
                $.ajax({
                    type: "POST",
                    url: "php/subject.php",
                    data: {
                        for: "load_all",
                    },
                    beforSend: function() {
                        swal({
                            text: "Processing....",
                            button: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            closeOnConfirm: false,
                            closeOnClickOutside: false
                        });
                    },
                    success: function(data) {
                        $(".laoding_screen").addClass("d-none");
                        if (data.status == "success") {
                            var subject_data = JSON.stringify(data);
                            sessionStorage.setItem("loaded_subject", subject_data);
                            show_all(data);
                        } else if (data.status == "not found") {
                            swal("warning", "No data found", "warning");
                        }
                    }
                })
            };

            function show_all(data) {
                let course_list = JSON.parse(sessionStorage.getItem('course_list'));
                let faculty_list = JSON.parse(sessionStorage.getItem('faculty_list'));
                course_list = course_list.data;
                faculty_list = faculty_list.data;
                let list = data.data;
                var table_tr = $(".subject_show_box");
                $(table_tr).html("");
                let counter = 0;
                for (let i = 0; i < list.length; i++) {
                    counter++;
                    let name = list[i].table_name;
                    let id = list[i].id;
                    let updated_at = list[i].updated_date;
                    let status = list[i].status;
                    let faculty_id = list[i].faculty;
                    let course_id = list[i].course;
                    // find name of faculty and course

                    let course_name = course_list.filter(data => data.id == course_id);
                    if (course_id == 0) {
                        course_name = "none";
                    } else {
                        course_name = course_name[0].table_name;
                    }


                    let faculty_name = faculty_list.filter(data => data.id == faculty_id);
                    if (faculty_id == 0) {
                        faculty_name = "none";
                    } else {
                        faculty_name = faculty_name[0].name;
                    }
                    $(table_tr).append('<tr class="subject_row" data-id="' + id + '"><td> ' + counter + '</td><td> ' + name + ' </td>  <td> ' + faculty_name + ' </td>  <td> ' + course_name + ' </td> <td> ' + status + ' </td><td> ' + updated_at + ' </td><td><i class="fa fa-ellipsis-v action_btn" aria-hidden="true"/></td></tr>');

                }
                context();
                search_subject(list);
            };

            function search_subject(list) {

                $("#search_box").on("input", function() {
                    let course_list = JSON.parse(sessionStorage.getItem('course_list'));
                    let faculty_list = JSON.parse(sessionStorage.getItem('faculty_list'));
                    course_list = course_list.data;
                    faculty_list = faculty_list.data;
                    let keyword = this.value;
                    let counter = 0;
                    var table_tr = $(".subject_show_box");
                    $(table_tr).html("");
                    for (let i = 0; i < list.length; i++) {
                        let name = list[i].table_name;
                        let id = list[i].id;
                        let updated_at = list[i].updated_date;
                        let status = list[i].status;
                        let description = list[i].subject_desc;
                        let faculty_id = list[i].faculty;
                        let course_id = list[i].course;
                        // find name of faculty and course

                        let course_name = course_list.filter(data => data.id == course_id);
                        if (course_id == 0) {
                            course_name = "none";
                        } else {
                            course_name = course_name[0].table_name;
                        }


                        let faculty_name = faculty_list.filter(data => data.id == faculty_id);
                        if (faculty_id == 0) {
                            faculty_name = "none";
                        } else {
                            faculty_name = faculty_name[0].name;
                        }

                        var rgxp = new RegExp(keyword, "gi");
                        if (name.match(rgxp)) {
                            counter++;
                            $(table_tr).append('<tr class="subject_row" data-id="' + id + '"><td> ' + counter + '</td><td> ' + name + ' </td>  <td> ' + faculty_name + ' </td>  <td> ' + course_name + ' </td> <td> ' + status + ' </td><td> ' + updated_at + ' </td><td><i class="fa fa-ellipsis-v action_btn" aria-hidden="true"/></td></tr>');
                        }

                    }
                    context();
                });
            };

            function delete_subject() {
                $(".subject_delete_btn").click(function() {
                    let active_id = sessionStorage.getItem("active_subject_id");
                    if (active_id !== null) {
                        swal({
                            title: "Are you sure?",
                            text: "Once deleted, you will not be able to recover this subject !",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    type: "POST",
                                    url: "php/subject.php",
                                    data: {
                                        for: "delete_subject",
                                        id: active_id
                                    },
                                    beforeSend: function() {
                                        swal({
                                            text: "Processing....",
                                            button: false,
                                            allowEscapeKey: false,
                                            allowOutsideClick: false,
                                            closeOnConfirm: false,
                                            closeOnClickOutside: false
                                        });
                                    },
                                    success: function(response) {
                                        swal.close();
                                        if (response.status == "success") {
                                            remove_array(active_id);
                                            swal("Poof! subject has been deleted!", {
                                                icon: "success",
                                            });
                                            hide();
                                        } else {
                                            swal("Opps!", "we cannot process at the moment, please try again later!", "error");
                                        }
                                    }
                                })
                            }
                        });
                    }else{
                        window.location = location.href;
                    }
                })
            };
            delete_subject();

            function remove_array(active_id) {
                let data_list = JSON.parse(sessionStorage.getItem("loaded_subject"));
                list = data_list.data;
                remainingArr = list.filter(data => data.id !== active_id);
                data_list.data = remainingArr;
                sessionStorage.setItem("loaded_subject", JSON.stringify(data_list));
                show_all(data_list);
            }

            function edit_subject() {

                $(".edit_subject_btn").click(function() {
                    let active_id = sessionStorage.getItem("active_subject_id");
                    if (active_id !== null) {
                        let data = JSON.parse(sessionStorage.getItem("loaded_subject"));
                        let list = data.data;
                        remainingArr = list.filter(data => data.id == active_id);
                        let name = remainingArr[0].table_name;
                        let tags = remainingArr[0].tags;
                        let status = remainingArr[0].status;
                        let description = remainingArr[0].sub_desc;
                        let faculty = remainingArr[0].faculty;
                        let course = remainingArr[0].course;
                        let semester = remainingArr[0].sems;
                        $(".subject_name_header").html(name);
                        $("#subject").val(name);
                        var objEditor = CKEDITOR.instances["editor"];
                        var dec = objEditor.setData(description);
                        $("#tag").val(tags);
                        $(".status").val(status);
                        $("#faculty").val(faculty);
                        $("#course").val(course);
                        $("#semester").val(semester);
                        $("#modal-top").modal("show");
                        updateSubject();
                    }else{
                        window.location = location.href;
                    }
                });

            }
            edit_subject();

            function updateSubject() {
                $("form").submit(function(event) {
                    event.preventDefault();
                    let subject_name = $("#subject").val();
                    let subject_tags = $("#tag").val();
                    let subject_faculty = $("#faculty").val();
                    let subject_course = $("#course").val();
                    let subject_semester = $("#semester").val();
                    let subject_status = $("#status").val();
                    var objEditor = CKEDITOR.instances["editor"];
                    var description = objEditor.getData();
                    if (subject_name.trim().length == 0 &&  description.trim().length == 0 &&  subject_tags.trim().length == 0) {
                        swal("warning", "All field are required", "warning");
                    } else {
                        let active_id = sessionStorage.getItem("active_subject_id");

                        $.ajax({
                            type: "POST",
                            url: "php/subject.php",
                            data: {
                                for: "update",
                                update_id: active_id,
                                subject_name: subject_name,
                                subject_tags: subject_tags,
                                subject_faculty: subject_faculty,
                                subject_course: subject_course,
                                subject_semester: subject_semester,
                                subject_status: subject_status,
                                description: description
                            },
                            beforeSend: function() {
                                swal({
                                    text: "Processing....",
                                    button: false,
                                    allowEscapeKey: false,
                                    allowOutsideClick: false,
                                    closeOnConfirm: false,
                                    closeOnClickOutside: false
                                });
                            },
                            success: function(data) {
                                swal.close();
                                if (data.status == "success") {
                                    swal("success", "Subject updated successfully", "success");
                                    $("form").trigger("reset");
                                    let data = JSON.parse(sessionStorage.getItem("loaded_subject"));
                                    let list = data.data;
                                    remainingArr = list.filter(data => data.id == active_id);
                                    remainingArr[0].table_name = subject_name;
                                    remainingArr[0].sub_desc = description;
                                    remainingArr[0].status = subject_status;
                                    remainingArr[0].tags = subject_tags;
                                    remainingArr[0].faculty = subject_faculty;
                                    remainingArr[0].course = subject_course;
                                    remainingArr[0].sems = subject_semester;
                                    sessionStorage.setItem("loaded_subject", JSON.stringify(data));
                                    show_all(data);
                                    hide();
                                    $("#modal-top").modal("hide");
                                } else if (data.status == "exist") {
                                    swal("warning", "This subject is already exist", "warning");
                                } else {
                                    swal("error", "somthing went wrong,please try again", "error");
                                }
                            }
                        })
                    }

                });

            };
        });
    </script>
    <!-- script -->

</body>

</html>