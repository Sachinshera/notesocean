<?php
require_once("check_valid_user.php");
require_once("database.php");
require_once("JWT.php");

use Firebase\JWT\JWT;

if (isset($_POST["get"])) {

    $get_products =  get_all_products($db);
    result($get_products);
}
if (isset($_POST["delete"])) {
    $delete_result = delete_products($db);
    result($delete_result);
}
if (isset($_POST["get_data_id"])) {
    $get_id_data = get_product_by_id($db);
    result($get_id_data);
}
// get all produts
function get_all_products($db)
{
    $get_all_products = $db->query("SELECT id,product_name FROM products ORDER BY id ASC");
    if ($get_all_products->num_rows  !== 0) {
        $data_array = [];
        while ($data = $get_all_products->fetch_assoc()) {
            array_push($data_array, $data);
        };
        $result =  json_encode($data_array);
    } else {
        $result =  "data not found";
    }
    return $result;
};
// get all produts
// delete products

function delete_products($db)
{
    $delete_id = $_POST["id"];
    $get_all_roduct_info  = $db->query("SELECT pdf_url,image_url FROM products WHERE id = '$delete_id'");
    $get_all_products = $get_all_roduct_info->fetch_assoc();
    $pdf_url = $get_all_products["pdf_url"];
    $image_url = $get_all_products["image_url"];
    $delete_data = $db->query("DELETE FROM products WHERE id = '$delete_id'");
    if ($delete_data) {
        $result = "success";
    } else {
        $result = "error";
    }
    return $result;
};
function get_product_by_id($db)
{
    $data_id =  $_POST["get_data_id"];
    $get_all_data = $db->query("SELECT * FROM products WHERE id = '$data_id'");
    $data_array = [];
    while ($data = $get_all_data->fetch_assoc()) {
        array_push($data_array, $data);
    }
    $result =  json_encode($data_array);
    return $result;
};
function result($data)
{
    $jwt = JWT::encode($data, $_SESSION["valid_user"]);
    echo json_encode(array("result" => $jwt));
};
