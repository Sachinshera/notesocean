<?php 
require("database.php");
header('Content-Type: application/json'); 
if($_SERVER["REQUEST_METHOD"] == "POST"){
// db code end here 
$today = date("d-m-Y h:i:sa");

// function control 
if(isset($_POST["for"])){
    if($_POST["for"]=="add"){
        $faculty_name = mysqli_real_escape_string($db, strip_tags(trim($_POST["faculty_name"])));
        $faculty_desc =$_POST["faculty_desc"];
        $faculty_status = mysqli_real_escape_string($db, strip_tags(trim($_POST["faculty_status"])));
        $faculty_tag = mysqli_real_escape_string($db, strip_tags(trim($_POST["tags"])));
        add_faculty($db,$faculty_name,$faculty_desc,$faculty_status,$faculty_tag);
    }
    if($_POST["for"] == "load_all"){
        retrive_faculty_data($db);
    }
    if($_POST["for"]=="delete_faculty"){
        $delete_id = $_POST["id"];
        delete_faculty($db,$delete_id);
    }
    if($_POST["for"]=="update"){
        $faculty_name = mysqli_real_escape_string($db, strip_tags(trim($_POST["faculty_name"])));
        $faculty_desc = mysqli_real_escape_string($db, strip_tags($_POST["faculty_desc"]));
        $faculty_status = mysqli_real_escape_string($db, strip_tags(trim($_POST["faculty_status"])));
        $faculty_id = mysqli_real_escape_string($db, strip_tags(trim($_POST["update_id"])));
        update_faculty($db,$faculty_name,$faculty_desc,$faculty_status,$faculty_id);
    }
    if($_POST["for"]=="get_all"){
        fetch_all_list($db); 
    }
}
}
// add faculty function start 
function add_faculty($db,$faculty_name,$faculty_desc,$faculty_status,$faculty_tag){
    function check_exist($db, $faculty_name, $faculty_desc)
    {
        $query = $db->query("SELECT name,description FROM faculties WHERE name = '$faculty_name' AND description = '$faculty_desc'");
        if ($query) {
            if ($query->num_rows !== 0) {
                $message = "ok";
            } else {
                $message = "not";
            }
            return $message;
        }
    };
$exist = check_exist($db,$faculty_name,$faculty_desc);

if($exist == "not"){
   
       function insert_data($db,$faculty_name,$faculty_desc,$faculty_status,$faculty_tag){
            $today = date("d-m-Y h:i:sa");
            $query = $db->query("INSERT INTO faculties(name,description,status,created_at,updated_at,tags) VALUES('$faculty_name','$faculty_desc','$faculty_status','$today','$today','$faculty_tag')");
            if($query){
                $message = array("status"=> "success", "messese"=>"faculty added");
            }
            else{
                $message = array("status"=> "error", "messese"=>"faculty not added");
            }
            return $message;
        };
    $insert_data = insert_data($db,$faculty_name,$faculty_desc,$faculty_status,$faculty_tag);
    $final_message = $insert_data;
}
else{
    $final_message = array("status"=> "exist", "messese"=>"faculty exist");
}
echo json_encode($final_message);
};
// add faculty function end 

// retrive all faculty data  to dashboard 
function retrive_faculty_data($db){
    $query = $db->query("SELECT * FROM faculties ORDER BY id ASC");
    if($query->num_rows !== 0){
        $data_array = [];
        while($row = $query->fetch_assoc()){
            array_push($data_array, $row);
        }
        $result = array("status"=> "success", "data"=>$data_array);
        echo json_encode($result);
    }
    else{
        $result = array("status"=> "not found", "messese"=>"faculty  not found");
        echo json_encode($result);
    }
};
// delete faculty function
    function delete_faculty($db,$id){
        $query  = $db->query("DELETE FROM faculties WHERE id = '$id'");
        if($query){
            $result = array("status"=> "success", "messese"=>"faculty  deleted");
            echo json_encode($result);
        }
        else{
            $result = array("status"=> "error", "messese"=>"faculty not deleted");
            echo json_encode($result);
        }
    };
    // change status 
    function chnage_status($db,$status,$id){
        $query = $db->query("UPDATE faculties SET status = '$status' WHERE id = '$id'");
        if($query){
            $result = array("status"=> "success", "messese"=>"status changed");
            echo json_encode($result);
        }
        else{
            $result = array("status"=> "error", "messese"=>"status not changed");
            echo json_encode($result);
        }
    }
/// update faculty
function  update_faculty($db,$faculty_name,$faculty_desc,$faculty_status,$faculty_id){
    $update = $db->query("UPDATE faculties SET name = '$faculty_name',description = '$faculty_desc',status = '$faculty_status' WHERE id = '$faculty_id'");
        if($update){
            $result = array("status"=> "success", "messese"=>"faculty updated");
            echo json_encode($result);
        }
        else{
            $result = array("status"=> "error", "messese"=>"faculty not added");
            echo json_encode($result);
        }
}
// fetch all list of faculties

function fetch_all_list($db){
    $sql = $db->query("SELECT name,id FROM faculties ORDER BY id DESC");
    if($sql){
        if($sql->num_rows!==0){
            $data_array = [];
            while($row = $sql->fetch_assoc()){
            array_push($data_array, $row);
            }
            $result = array("status"=> "success", "data"=>$data_array);
            echo json_encode($result);
        }
        else{
            $result = array("status"=> "not found", "messese"=>"faculty not found");
            echo json_encode($result);
        }
    }
    else{
            $result = array("status"=> "error", "messese"=>"somthing went wrong");
            echo json_encode($result);
    }
}
