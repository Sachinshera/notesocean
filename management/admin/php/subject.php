<?php
require("database.php");
header('Content-Type: application/json');
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // db code end here 
    $today = date("d-m-Y h:i:sa");
    // function control

    if (isset($_POST["for"])) {
        if ($_POST["for"] == "add") {
            $subject_name = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_name"])));
            $subject_tags = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_tags"])));
            $subject_faculty = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_faculty"])));
            $subject_course = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_course"])));
            $subject_semester = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_semester"])));
            $subject_status = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_status"])));
            $description = $_POST["description"];
            add_subject($db, $subject_name, $subject_tags, $subject_course, $subject_faculty, $subject_semester, $subject_status, $description, $today);
        } else if ($_POST["for"] == "load_all") {
            load_all_subject($db);
        } else if ($_POST["for"] == "delete_subject") {
            $id = $_POST["id"];
            delete_subject($db, $id);
        }
        else if($_POST["for"] == "update"){
            $subject_name = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_name"])));
            $subject_tags = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_tags"])));
            $subject_faculty = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_faculty"])));
            $subject_course = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_course"])));
            $subject_semester = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_semester"])));
            $subject_status = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject_status"])));
            $description = $_POST["description"];
            $update_id = $_POST["update_id"];
            update_subject($db, $subject_name, $subject_tags, $subject_course, $subject_faculty, $subject_semester, $subject_status, $description, $today,$update_id);
        }
         else {
            $result = array("status" => "error", "messese" => "invalid reason");
            echo json_encode($result);
        }
    } else {
        $result = array("status" => "error", "messese" => "reason not found");
        echo json_encode($result);
    }
} else {
    $result = array("status" => "error", "messese" => "invaid method");
    echo json_encode($result);
}

function add_subject($db, $subject_name, $subject_tags, $subject_course, $subject_faculty, $subject_semester, $subject_status, $description, $today)
{
    // check existing subject
    $check = $db->query("SELECT * FROM subject_table WHERE table_name = '$subject_name' AND sub_desc = '$description' AND faculty = '$subject_faculty' AND sems = '$subject_semester'");
    if($check){
        if ($check->num_rows == 0) {
            $insert_data = $db->query("INSERT INTO subject_table(table_name,sub_desc,faculty,course,sems,created_date,updated_date,status,tags) VALUES('$subject_name','$description','$subject_faculty','$subject_course','$subject_semester','$today','$today','$subject_status','$subject_tags')");
            if ($insert_data) {
                $result = array("status" => "success", "messese" => "subject  added");
                echo json_encode($result);
            } else {
                $result = array("status" => "error", "messese" => "subject could not added " . $db->error . " ");
                echo json_encode($result);
            }
        } else {
            $result = array("status" => "exist", "messese" => "subject already exists");
            echo json_encode($result);
        }
    }else{
        $result = array("status" => "error", "messese" => "somthing went wrong");
        echo json_encode($result);
    }
    
}
function load_all_subject($db)
{
    $sql = $db->query("SELECT * FROM subject_table ORDER BY id ASC");
    if ($sql->num_rows !== 0) {
        $data_array = [];
        while ($row = $sql->fetch_assoc()) {
            array_push($data_array, $row);
        }
        $result = array("status" => "success", "data" => $data_array);
        echo json_encode($result);
    } else {
        $result = array("status" => "nodata", "messese" => "No subject in database");
        echo json_encode($result);
    }
}
function delete_subject($db, $id)
{
    $sql = $db->query("DELETE FROM subject_table WHERE id = '$id'");
    if ($sql) {
        $result = array("status" => "success", "messese" => "subject  deleted");
        echo json_encode($result);
    } else {
        $result = array("status" => "error", "messese" => "failed ot delete");
        echo json_encode($result);
    }
};
function  update_subject($db, $subject_name, $subject_tags, $subject_course, $subject_faculty, $subject_semester, $subject_status, $description, $today,$update_id){
    $sql = "UPDATE subject_table SET table_name = '$subject_name',sub_desc = '$description',faculty= '$subject_faculty', course = '$subject_course',sems = '$subject_semester',updated_date = '$today',status= '$subject_status',tags = '$subject_tags' WHERE id = '$update_id'";
    if($db->query($sql)){
        $result = array("status" => "success", "messese" => "subject  updated");
        echo json_encode($result);
    }else{
        $result = array("status" => "error", "messese" => "failed ot update ".$db->error." ");
        echo json_encode($result);
    }

}
