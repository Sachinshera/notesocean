<?php
require("database.php");
header('Content-Type: application/json');
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // db code end here 
    $today = date("d-m-Y h:i:sa");
    // function control

    if (isset($_POST["for"])) {
        if ($_POST["for"] == "check_exist") {
            $title = mysqli_real_escape_string($db, strip_tags(trim($_POST["title"])));
            $short_desc = mysqli_real_escape_string($db, strip_tags(trim($_POST["short_desc"])));
            $description = $_POST["description"];
            check_exist($db, $title, $short_desc, $description);
        } else if ($_POST["for"] == "add_new") {
            $title = mysqli_real_escape_string($db, strip_tags(trim($_POST["title"])));
            $short_desc = mysqli_real_escape_string($db, strip_tags(trim($_POST["short_desc"])));
            $description = $_POST["description"];
            $faculty = mysqli_real_escape_string($db, strip_tags(trim($_POST["faculty"])));
            $course = mysqli_real_escape_string($db, strip_tags(trim($_POST["course"])));
            $subject = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject"])));
            $status = mysqli_real_escape_string($db, strip_tags(trim($_POST["status"])));
            $semester = mysqli_real_escape_string($db, strip_tags($_POST["semester"]));
            $tags = mysqli_real_escape_string($db, strip_tags($_POST["tags"]));
            $url = mysqli_real_escape_string($db, strip_tags(trim($_POST["pdf_url"])));
            $size = mysqli_real_escape_string($db, strip_tags(trim($_POST["size"])));
            $file_key = mysqli_real_escape_string($db, strip_tags($_POST["file_key"]));
            add_new($db, $title, $short_desc, $description, $faculty, $course, $subject, $status, $semester, $tags, $url, $size, $file_key, $today);
        } else if ($_POST["for"] == "load_all") {
            load_all($db);
        } 
        else if($_POST["for"] == "delete_notes"){
            delete_notes($db,$_POST["id"]);
        }
        else if ($_POST["for"] == "update"){
            $id = mysqli_real_escape_string($db, strip_tags(trim($_POST["id"])));
            $title = mysqli_real_escape_string($db, strip_tags(trim($_POST["title"])));
            $short_desc = mysqli_real_escape_string($db, strip_tags(trim($_POST["short_desc"])));
            $description = $_POST["description"];
            $faculty = mysqli_real_escape_string($db, strip_tags(trim($_POST["faculty"])));
            $course = mysqli_real_escape_string($db, strip_tags(trim($_POST["course"])));
            $subject = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject"])));
            $status = mysqli_real_escape_string($db, strip_tags(trim($_POST["status"])));
            $semester = mysqli_real_escape_string($db, strip_tags($_POST["semester"]));
            $tags = mysqli_real_escape_string($db, strip_tags($_POST["tags"]));
            $url = mysqli_real_escape_string($db, strip_tags(trim($_POST["pdf_url"])));
            $size = mysqli_real_escape_string($db, strip_tags(trim($_POST["size"])));
            $file_key = mysqli_real_escape_string($db, strip_tags($_POST["file_key"]));
            update($db, $title, $short_desc, $description, $faculty, $course, $subject, $status, $semester, $tags, $url, $size, $file_key, $today,$id);
        }
        else {
            $result = array("status" => "error", "messese" => "invalid reason");
            echo json_encode($result);
        }
    } else {
        $result = array("status" => "error", "messese" => "reason not found");
        $file = $_FILES["file"];
        echo json_encode($result);
    }
} else {
    $result = array("status" => "error", "messese" => "invaid request");
    echo json_encode($result);
}

function  check_exist($db, $title, $short_desc, $description)
{
    $check_exist = "SELECT * FROM products WHERE product_name = '$title' AND short_desc = '$short_desc' AND long_desc = '$description'";
    if ($db->query($check_exist)->num_rows == 0) {
        $result = array("status" => "success", "messese" => "not exist");
        echo json_encode($result);
    } else {
        $result = array("status" => "exist", "messese" => "already exist");
        echo json_encode($result);
    }
};

function add_new($db, $title, $short_desc, $description, $faculty, $course, $subject, $status, $semester, $tags, $url, $size, $file_key, $today)
{
    $sql = "INSERT INTO products(product_name,short_desc,long_desc,keyword,faculty,course,sems,sub,pdf_url,size,file_key,created_date,updated_date,status) VALUES('$title','$short_desc','$description','$tags','$faculty','$course','$semester','$subject','$url','$size','$file_key','$today','$today','$status')";
    if ($db->query($sql)) {
        $result = array("status" => "success", "messese" => "data saved successfully");
        echo json_encode($result);
    } else {
        $result = array("status" => "error", "messese" => "cannot be insert" . $db->error);
        echo json_encode($result);
    }
}
function load_all($db)
{
    $sql = "SELECT * FROM products ORDER BY id DESC";
    if ($sql = $db->query($sql)) {
        if ($sql->num_rows !== 0) {
            $data_array = [];
            while ($row = $sql->fetch_assoc()) {
                array_push($data_array, $row);
            }
            $result = array("status" => "success", "messese" => "data loaded successfully", "data" => $data_array);
            echo json_encode($result);
        } else {
            $result = array("status" => "nodata", "messese" => "somthing went wrong");
            echo json_encode($result);
        }
    } else {
        $result = array("status" => "error", "messese" => "somthing went wrong");
        echo json_encode($result);
    }
}
function  delete_notes($db,$id){
    $sql = "DELETE FROM products WHERE id = '$id'";
    if($db->query($sql)){
        $result = array("status" => "success", "messese" => "notes deleted");
        echo json_encode($result);
    }else{
        $result = array("status" => "error", "messese" => "somthing went wrong");
        echo json_encode($result);
    }
}

function update($db, $title, $short_desc, $description, $faculty, $course, $subject, $status, $semester, $tags, $url, $size, $file_key, $today,$id){
    $sql = "UPDATE products SET product_name = '$title',short_desc = '$short_desc',long_desc = '$description',keyword = '$tags',faculty = '$faculty',course = '$course',sub = '$subject',sems = '$semester',pdf_url = '$url',size = '$size',file_key= '$file_key', status = '$status',updated_date = '$today' WHERE id =$id";
    if($db->query($sql)){
        $result = array("status" => "success", "messese" => "notes updated");
        echo json_encode($result);
    }else{
        $result = array("status" => "error", "messese" => "cannnot update data->".$db->error);
        echo json_encode($result);
    }
    
}