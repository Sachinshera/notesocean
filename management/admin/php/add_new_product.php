<?php
require_once("check_valid_user.php");
require_once("database.php");
require_once("JWT.php");

use Firebase\JWT\JWT;

$result = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $product_name = mysqli_real_escape_string($db, strip_tags(trim($_POST["product_name"])));
    $short_description = mysqli_real_escape_string($db, strip_tags(trim($_POST["short_description"])));
    $long_description = mysqli_real_escape_string($db, strip_tags(trim($_POST["long_description"])));
    $keyword = mysqli_real_escape_string($db, strip_tags(trim($_POST["keyword"])));
    $course = mysqli_real_escape_string($db, strip_tags(trim($_POST["course"])));
    $semster = mysqli_real_escape_string($db, strip_tags(trim($_POST["semster"])));
    $subject = mysqli_real_escape_string($db, strip_tags(trim($_POST["subject"])));
    if ($_FILES["product_pdf"]) {
        $product_pdf = $_FILES["product_pdf"];
    }
    $today = date("d-m-Y h:i:sa");
    $check_title_exist = $db->query("SELECT product_name FROM products  WHERE product_name = '$product_name'");
    $pdf_path_for_db = "";
    if ($check_title_exist->num_rows == 0) {
        $alias  = str_replace(" ", "-", $product_name);
        // upload image and pdf if avliable 
        if ($_FILES["product_pdf"]) {
            if ($product_pdf["tmp_name"] !== "") {
                $pdf_path_for_folder = "../../assest/pdfs/product_pdf/" . $alias . ".pdf";
                $pdf_path_for_db = "assest/pdfs/product_pdf/" . $alias . ".pdf";
                move_uploaded_file($product_pdf["tmp_name"], $pdf_path_for_folder);
            }
        }
        $insert_data = "INSERT INTO products(product_name,short_desc,long_desc,keyword,pdf_url,alias,created_date,updated_date,course,sems,sub) VALUES('$product_name','$short_description','$long_description','$keyword','$pdf_path_for_db','$alias','$today','$today','$course','$semster','$subject')";
        if ($db->query($insert_data)) {
            $result = "success";
        } else {
            $result = "failed" . $db->error;
        }
    } else {
        $result = "exist";
    }
}
echo $result;
