<?php
require("database.php");
header('Content-Type: application/json');
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // db code end here 
    $today = date("d-m-Y h:i:sa");
    // function control

    if (isset($_POST["for"])) {
        if ($_POST["for"] == "add_course") {
            $course_name = mysqli_real_escape_string($db, strip_tags(trim($_POST["course_name"])));
            $course_desc = $_POST["course_desc"];
            $course_status = mysqli_real_escape_string($db, strip_tags(trim($_POST["course_status"])));
            $course_tag = mysqli_real_escape_string($db, strip_tags(trim($_POST["tags"])));
            $course_faculty = mysqli_real_escape_string($db, strip_tags(trim($_POST["course_faculty"])));
            add_course($db, $course_name, $course_tag, $course_faculty, $course_status, $course_desc, $today);
        } else if ($_POST["for"] == "load_all") {
            load_all($db);
        } 
        else if ($_POST["for"] == "delete_course"){
            $course_id = $_POST["id"];
            delete_course($db,$course_id);
        }
        else if ($_POST["for"] == "update"){
            $update_id = $_POST["update_id"];
            $course_name = mysqli_real_escape_string($db, strip_tags(trim($_POST["course_name"])));
            $course_desc = $_POST["course_desc"];
            $course_status = mysqli_real_escape_string($db, strip_tags(trim($_POST["course_status"])));
            $course_tag = mysqli_real_escape_string($db, strip_tags(trim($_POST["tags"])));
            $course_faculty = mysqli_real_escape_string($db, strip_tags(trim($_POST["course_faculty"])));
            update_course($db,$update_id,$course_name,$course_desc,$course_status,$course_tag,$course_faculty,$today);
        }
        
        else {
            $result = array("status" => "error", "messese" => "invaid reason");
            echo json_encode($result);
        }
    } else {
        $result = array("status" => "error", "messese" => "reason not found");
        echo json_encode($result);
    }
} else {
    $result = array("status" => "error", "messese" => "invalid method");
    echo json_encode($result);
}

function add_course($db, $course_name, $course_tag, $course_faculty, $course_status, $course_desc, $today)
{
    // check existing course
    function check_exist($db, $course_name, $course_faculty, $course_desc)
    {
        $sql = $db->query("SELECT * FROM course_table WHERE table_name = '$course_name' AND course_desc = '$course_desc' AND faculty_id = '$course_faculty'");
        if ($sql->num_rows == 0) {
            return true;
        } else {
            return false;
        }
    }
    $check =  check_exist($db, $course_name, $course_faculty, $course_desc);
    if ($check) {
        $sql = "INSERT INTO course_table(table_name,course_desc,faculty_id,status,created_date,updated_date,tags) VALUES('$course_name','$course_desc','$course_faculty','$course_status','$today','$today','$course_tag')";
        if ($db->query($sql)) {
            $result = array("status" => "success", "messese" => "course added");
            echo json_encode($result);
        } else {
            $result = array("status" => "error", "messese" => "somthing went wrong");
            echo json_encode($result);
        }
    } else {
        $result = array("status" => "exist", "messese" => "course already exists");
        echo json_encode($result);
    }
};

function load_all($db)
{
    $sql = $db->query("SELECT * FROM course_table ORDER BY id ASC");
    if ($sql->num_rows !== 0) {
        $data_array = [];
        while ($data = $sql->fetch_assoc()) {
            array_push($data_array, $data);
        }
        $result = array("status" => "success", "data" => $data_array);
        echo json_encode($result);
    } else {
        $result = array("status" => "nodata", "messese" => "invalid method");
        echo json_encode($result);
    }
};
function delete_course($db,$course_id){
    $sql =$db->query( "DELETE FROM course_table WHERE id = '$course_id'");
    if($sql){
        $result = array("status" => "success", "messege" => "course deleted");
        echo json_encode($result);
    }else{
        $result = array("status" => "error", "messese" => "failed to delete");
        echo json_encode($result);
    }
}
function update_course($db,$update_id,$course_name,$course_desc,$course_status,$course_tag,$course_faculty,$today){
    $sql = "UPDATE course_table SET table_name = '$course_name', course_desc = '$course_desc', faculty_id = '$course_faculty',tags = '$course_tag',updated_date = '$today',status = '$course_status' WHERE id = '$update_id' ";
    if($db->query($sql)){
        $result = array("status" => "success", "messege" => "course updated");
        echo json_encode($result);
    }else{
        $result = array("status" => "error", "messese" => "failed to update ".$db->error." ");
        echo json_encode($result);
    }
}
