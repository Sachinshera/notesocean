<?php
session_start();
if (empty($_SESSION["valid_user"])) {
    http_response_code(401);
    $error_data = array("Status" => "Error", "Cause" => "User Not Authorized", "ResponseCose" => "401");
    die(json_encode($error_data));
};
