<?php
require_once("database.php");
header('Content-Type: application/json');
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["for"])) {
        //function control 
        if ($_POST["for"] == "get_all_pending_payment") {
            $response =  get_all_pending_payment($pay_db);
        } else if ($_POST["for"] == "user_pofile_data") {
            if (isset($_POST["user_id"])) {
                $user_id = $_POST["user_id"];
                $response = user_pofile_data($db, $user_db, $user_id,$home_page);
            } else {
                $response = array("status" => "error", "messege" => "data missing");
            }
        } 
        else if($_POST["for"]=="reject_process"){
            if(isset($_POST["pending_id"])){
                $pending_id = $_POST["pending_id"];
                $response = reject_pending_process($pay_db,$pending_id);
            }else{
                $response = array("status" => "error", "messege" => "missing reason");
            }
        } else if($_POST["for"]=="approve_process"){
            if(isset($_POST["pending_id"])){
                $pending_id = $_POST["pending_id"];
                $response = approve_pending_process($pay_db,$pending_id);
            }else{
                $response = array("status" => "error", "messege" => "missing reason");
            }
        }
        else {
            $response = array("status" => "error", "messege" => "invalid reason");
        }
        //function control end
    } else {
        $response = array("status" => "error", "messege" => "missing reason");
    }
} else {
    $response = array("status" => "error", "messege" => "invalid request");
}


function get_all_pending_payment($pay_db)
{
    $sql = $pay_db->query("SELECT * FROM process WHERE status = 'pending' ORDER BY id ASC");
    if ($sql) {
        if ($sql->num_rows !== 0) {
            $data_array = [];
            while ($row = $sql->fetch_assoc()) {
                array_push($data_array, $row);
            }
            $response = array("status" => "success", "data" => $data_array);
        } else {
            $response = array("status" => "nodata", "messege" => "no pending payment");
        }
    } else {
        $response = array("status" => "error", "messege" => "error from server");
    }
    return $response;
};
function user_pofile_data($db, $user_db, $user_id,$home_page)
{
    // get user profile data 

    $profile_data = $user_db->query("SELECT * FROM profiles WHERE user_id = '$user_id'");
    $profile_data = $profile_data->fetch_assoc();
    $fullname = $profile_data["fullname"];
    $email = $profile_data["email"];
    $picture = $profile_data["picture"];

    $pro_data = array("fullname"=>$fullname,"email"=>$email,"picture"=>$picture);
    // get public notes 

    $data_notes = [];
    $public_notes = $db->query("SELECT  * FROM  products WHERE notes_by = '$user_id'");
    if ($public_notes) {
        include("clean_string.php");
        if ($public_notes->num_rows !== 0) {
            while($public_note = $public_notes->fetch_assoc()){
                $title = $public_note["product_name"];
                $notes_id = $public_note["id"];
                
                $title = just_clean($title);
                $url = $home_page . "/" . $notes_id . "/" . $title . ".html";
                $temp_array = array("title"=>$title,"url"=>$url);
                array_push($data_notes,$temp_array);
            }
        }
    }
    // get total pubished notes 
    $publish_notes_total = 0;
    $total_like = 0;
    $total_dislike = 0;
    $total_views = 0;
    $get_publish_notes = $db->query("SELECT * FROM products WHERE notes_by = '$user_id'");
    if ($get_publish_notes->num_rows !== 0) {
        while ($publish_notes = $get_publish_notes->fetch_assoc()) {
            $publish_notes_total++;
            $notes_id = $publish_notes["id"];
            // get like  
            $get_like = $db->query("SELECT * FROM  reaction WHERE notes_id = '$notes_id' AND react = 1");
            if ($get_like->num_rows !== 0) {
                $get_total_like = $get_like->fetch_assoc();
                $total_like += $get_like->num_rows;
            }
            //  get dislike 
            $get_dislike = $db->query("SELECT * FROM  reaction WHERE notes_id = '$notes_id' AND react = 2");
            if ($get_dislike->num_rows !== 0) {
                $get_total_dislike = $get_dislike->fetch_assoc();
                $total_dislike += $get_dislike->num_rows;
            }

            // get views
            $get_views = $db->query("SELECT * FROM views WHERE notes_id = '$notes_id'");
            if ($get_views->num_rows !== 0) {
                $total_views += $get_views->num_rows;
            }
        }
    }

    $views_react_data = array("total_notes"=>$publish_notes_total,"total_views"=>$total_views,"total_likes"=>$total_like,"total_dislike"=>$total_dislike);
    $response = array("status"=>"success","notes_data"=>$data_notes,"profile_data"=>$pro_data,"reaction_views_data"=>$views_react_data);
    return $response;

};

function reject_pending_process($pay_db,$pending_id){
    $today = date("d-m-Y h:i:sa");
    $sql = $pay_db->query("UPDATE  process SET status = 'rejected', date_time = '$today' WHERE id = '$pending_id'  ");
    if($sql){
        $response = array("status" => "success", "messege" => "updated");
    }else{
        $response = array("status" => "error", "messege" => "not updated".$pay_db->error);
    }
    return $response;
};
function approve_pending_process($pay_db,$pending_id){
    $today = date("d-m-Y h:i:sa");
// get pending data 

$pending_data = $pay_db->query("SELECT * FROM process WHERE id = '$pending_id'");
if($pending_data){
    if($pending_data->num_rows!==0){
        $pending_data = $pending_data->fetch_assoc();
        $user_id = $pending_data["user_id"];
        $points = $pending_data["points"];
        $method = $pending_data["method"];
        $status = $pending_data["status"];
        $account_id = $pending_data["account_id"];
        // check status 
        if($status=="pending"){
            // add into paid

            $insert = $pay_db->query("INSERT INTO paid(user_id,points,account_id,date_time,method,status) VALUES('$user_id','$points','$account_id','$today','$method','success')");
            if($insert){
                // deleted pending data 
                $delete = $pay_db->query("DELETE FROM process WHERE id = '$pending_id'");
                if($delete){
                    $response = array("status" => "success", "messege" => "payment paid and everything is okay");
                }else{
                    $response = array("status" => "error", "messege" => "payment  paid but processing".$pay_db->error);
                }
            }else{
                $response = array("status" => "error", "messege" => "payment not paid".$pay_db->error);
            }

        }else{
            $response = array("status" => "error", "messege" => "data already rejected");
        }

    }else{
        $response = array("status" => "error", "messege" => "no pending data found for this id");
    }
    
}else{
    $response = array("status" => "error", "messege" => "query not passed");
}

 return $response;
};

echo json_encode($response);
