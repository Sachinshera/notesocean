<?php session_start();
if ($_SESSION["valid_user"] == "Admin") {
} else {
    header("Location:index.php");
} ?>
<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Add Notes</title>
    <?php require("assets/includes/cdncss.php"); ?>
    <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>
    <style type="text/css">
        .title {
            color: #000;
        }

        .title h1 {
            font-weight: bold;
        }

        .public {
            color: red;
        }

        .itm {
            background-color: #fff;
            padding: 15px;
            margin: 15px 10px;
            border-radius: 10px;
        }

        .file p {
            color: red;
        }

        .file_span {
            color: #000;
        }

        .basic {
            padding: 20px 30px 100px 0px;
            margin: 15px 10px;
            color: #000;
        }

        .star {
            color: red;
        }

        .basic p {
            margin: 0;
        }

        .basic select {
            border: 1px solid red;
            border-radius: 5px;
            width: 290px;
            padding: 5px;
            margin-bottom: 20px;
            margin-right: 30px;
        }

        .btn_save {
            background-color: red;
            color: #fff;
            border: none;
            padding: 15px 25px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        .new_area {
            height: auto !important;
        }

        .status {
            width: 100%;
            border-color: #ebebea;
            padding: 8px 20px;
            border-radius: 5px;
            color: #495057;
        }
    </style>

</head>

<body>

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php require("assets/includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->
        <main class="main--container">
            <!-- Page Header Start -->
            <section class="page--header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- Page Title Start -->
                            <h2 class="page--title h5">Dashboard</h2>
                            <!-- Page Title End -->

                            <ul class="breadcrumb">
                                <li class="breadcrumb-item active"><span>Dashboard</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Page Header End -->



            <section class="main--content new_area">
                <div class="title">
                    <h1>Add <span class="public">Notes</span></h1>
                </div>

                <form>
                    <div class="form-group">
                        <label for="addnote">Title<span class="star">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Note Title">
                    </div>
                    <div class="form-group">
                        <label for="addnote_des">Short Description <small> (meta) </small> <span class="star">*</span></label>
                        <textarea class="form-control" name="short_desc" placeholder="Enter a short description" id="short_desc">

    </textarea>
                    </div>
                    <div class="form-group">
                        <label for="course_description">Full Content <span class="star">*</span></label>

                        <textarea id="editor" name="course_description">

        </textarea>

                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="choose_faculty">Choose Faculty <span class="star">*</span>
                                </label>
                                <select class="form-select status" aria-label="Default select example" name="faculty" id="faculty">
                                    <option value="0">None</option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label for="choose_course">Choose Course <span class="star">*</span></label>
                                <select class="form-select status" aria-label="Default select example" name="course" id="course">
                                    <option value="0">None</option>
                                </select>
                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="choose_semester">Choose Semester <span class="star">*</span>
                                </label>
                                <select class="form-select status" aria-label="Default select example" name="semester" id="semester">
                                    <option value="0">None</option>
                                    <option value="1">Semester 1</option>
                                    <option value="2">Semester 2</option>
                                    <option value="3">Semester 3</option>
                                    <option value="4">Semester 4</option>
                                    <option value="5">Semester 5</option>
                                    <option value="6">Semester 6</option>
                                    <option value="7">Semester 7</option>
                                    <option value="8">Semester 8</option>
                                </select>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label for="tags">Tags</label>
                                    <input type="text" id="tags" name="tags" class="form-control" placeholder="bba notes,bba 6 sem notes" required>
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label for="subject">subject</label>
                                    <select id="subject" name="subject" class="form-control">
                                        <option value="0"> None </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label for="status">Status <span class="star">*</span></label>
                                <select class="form-select status" name="status" id="status">
                                    <option value="active">Active</option>
                                    <option value="inactive">Inactive</option>
                                </select>
                            </div>

                        </div>

                    </div>

                    <div> </div>

                    <div class="form-group">
                        <label>Upload PDF <span class="star">*</span> </label>
                        <input type="file" accept=".pdf" class="form-control" id="file" name="file" required>
                    </div>

                    <button class="btn_save" type="submit">Save</button>
                </form>
    </div>
    </section>
    <!-- Main Footer Start -->
    <footer class="main--footer">
        <p>Copyright &copy; <a href="#">NB</a>. All Rights Reserved.</p>
    </footer>
    <!-- Main Footer End -->
    </main>


    <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->

    <script type="text/javascript">
        CKEDITOR.replace('editor');
    </script>
    <?php require("assets/includes/cdnjs.php"); ?>
    <script src="https://sdk.amazonaws.com/js/aws-sdk-2.855.0.min.js"></script>

    <!-- script -->
    <script>
        //   preloaded data start 
        $(document).ready(function() {
            function getAllFaculty() {
                let sel_box = $("#faculty");
                $.ajax({
                    type: "POST",
                    url: "php/faculty.php",
                    data: {
                        for: "get_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                    },
                    success: function(response) {
                        $(sel_box).html("");
                        $(sel_box).append("<option value='0'> None </option>");
                        let data = response.data;
                        if (response.status == "success") {

                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                        }
                    }
                })
            };
            getAllFaculty();

            function getAllCourse() {
                let sel_box = $("#course");
                $.ajax({
                    type: "POST",
                    url: "php/course.php",
                    data: {
                        for: "load_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                    },
                    success: function(response) {
                        $(sel_box).html("");
                        $(sel_box).append("<option value='0'> None </option>");
                        let data = response.data;
                        if (response.status == "success") {
                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].table_name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                        }
                    }
                })
            };
            getAllCourse();



            function getAllSubject() {
                let sel_box = $("#subject");
                $.ajax({
                    type: "POST",
                    url: "php/subject.php",
                    data: {
                        for: "load_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                    },
                    success: function(response) {
                        $(sel_box).html("");
                        $(sel_box).append("<option value='0'> None </option>");
                        let data = response.data;
                        if (response.status == "success") {
                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].table_name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                        }
                    }
                })
            };
            getAllSubject();

        });
        $(document).ready(function() {
            $("form").submit(function(event) {
                event.preventDefault();
                var title = $("#title").val();
                var faculty = $("#faculty").val();
                var course = $("#course").val();
                var semester = $("#semester").val();
                var tags = $("#tags").val();
                var status = $("#status").val();
                var short_desc = $("#short_desc").val();
                var subject = $("#subject").val();
                var files = $("#file")[0].files;
                var file = $("#file")[0].files[0];

                var objEditor = CKEDITOR.instances["editor"];
                var description = objEditor.getData();
                if (title !== "" || tags !== "" || description !== "" || short_desc !== "") {
                    var size = file.size;
                    $.ajax({
                        type: "POST",
                        url: "php/notes.php",
                        data: {
                            for: "check_exist",
                            title: title,
                            short_desc: short_desc,
                            description: description
                        },
                        beforeSend: function() {
                            swal({
                                text: "please wait.... we are checking...",
                                button: false,
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                closeOnConfirm: false,
                                closeOnClickOutside: false
                            });
                        },
                        success: function(data) {
                            console.log(data);
                            swal.close();
                            if (data.status == "success") {
                            upload_s3();
                            } else if (data.status == "exist") {
                                swal("warning", "this notes is already exist", "warning");
                            } else {
                                swal("error", "somthing went wrong,please try again", "error");
                            }
                        }
                    });

                    function upload_s3() {
                        //   upload file to s3 and update data into database start 

                        // cofig aws s3 
                        var config = AWS.config;
                        config.update({
                            accessKeyId: "AKIASAWDBAK5WAWBA67V",
                            secretAccessKey: "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",
                        });
                        config.region = "ap-south-1";

                        var s3 = new AWS.S3();
                        
                        // upload on s3 process
                        <?php 
                            if($home_page = "https://notesocean.com"){
                                $btk_name = "www.notesocean.com";
                            }else{
                                $btk_name = "test.notesocean.com";
                            }
                            ?>
                        var params = {
                            Bucket: <?php echo $btk_name; ?>  ,
                            Key: "notesocean_" + title + ".pdf",
                            Body: file,
                            ACL: "public-read"
                        };
                        s3.upload(params).on("httpUploadProgress", function(event) {
                            var loaded = event.loaded;
                            var total = event.total;
                            var percent = Math.floor((loaded * 100) / total);
                            swal({
                                text: "Please wait..file  is uploading... " + percent+"%",
                                button: false,
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                closeOnConfirm: false,
                                closeOnClickOutside: false
                            })
                        }).send(function(error, data) {
                            console.log(data);
                            //  complte uploaded 
                            swal.close();
                            if (error == null) {
                                var pdf_url = data.Location;
                                var key = data.Key;
                                var filename = title;
                                update_data_into_db(pdf_url,key);
                                // end 
                            }
                        });

                    };

                    function update_data_into_db(pdf_url,key) {
                        //   update data on database
                        $.ajax({
                            type: "POST",
                            url: "php/notes.php",
                            data: {
                                for: "add_new",
                                title: title.toString(),
                                faculty: faculty.toString(),
                                course: course.toString(),
                                subject: subject.toString(),
                                short_desc: short_desc.toString(),
                                description: description.toString(),
                                status: status.toString(),
                                semester: semester.toString(),
                                tags: tags.toString(),
                                pdf_url: pdf_url.toString(),
                                size: size.toString(),
                                file_key: key.toString(),
                            },
                            beforeSend: function() {
                                swal({
                                    text: "please wait.... data updating.....",
                                    button: false,
                                    allowEscapeKey: false,
                                    allowOutsideClick: false,
                                    closeOnConfirm: false,
                                    closeOnClickOutside: false
                                });
                            },
                            success: function(json) {
                                console.log(json);
                                swal.close();
                                if (json) {
                                    if (json.status == "success") {
                                        swal("success", "notes added successfully ", "success");
                                        $("form").trigger("reset");
                                    } else {
                                        swal("opps!", "somthing went wrong , please try again later", "error");
                                    }
                                } else {
                                    swal("opps!", "somthing went wrong , please try again later", "error");
                                }

                            }
                        });
                    }

                } else {
                    swal("warning", "All field are required", "warning");
                }
            });
            // $(".btn_save").click(function() {
            //     $("form").trigger("submit");
            // });


        });
        //   preloaded data end
    </script>
    <!-- script -->

</body>

</html>