<!DOCTYPE html>
<html dir="ltr" lang="en" class="no-outlines">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Manage notes</title>
    <?php require("assets/includes/cdncss.php"); ?>
    <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>

    <style type="text/css">
        .action_btn:hover {
            cursor: pointer;
        }

        .title {
            color: #000;
        }

        .title h1 {
            font-weight: bold;
        }

        .star {
            color: red;
        }

        .basic p {
            margin: 0;
        }

        .basic input {
            border: 1px solid red;
            border-radius: 5px;
            width: 350px;
            padding: 5px;
            margin-bottom: 20px;
        }

        .basic select {
            border: 1px solid red;
            border-radius: 5px;
            width: 350px;
            padding: 5px;
            margin-bottom: 20px;
        }

        .btn_save {
            background-color: red;
            color: #fff;
            border: none;
            padding: 15px 25px;
            border-radius: 5px;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        .new_area {
            height: auto !important;
        }

        .main--content {
            padding: 10px 10px !important;
        }

        form.example input[type=text] {
            padding: 10px;
            font-size: 17px;
            border: 1px solid red;
            float: left;
            width: 80%;
            background: #fff;
            border-radius: 10px 0 0 10px;
        }

        form.example button {
            float: left;
            width: 20%;
            padding: 10px;
            background: red;
            color: white;
            font-size: 17px;
            border: 1px solid grey;
            border-left: none;
            cursor: pointer;
            border-radius: 0 10px 10px 0;
        }

        form.example button:hover {
            background: #0b7dda;
        }

        form.example::after {
            content: "";
            clear: both;
            display: table;
        }

        .table {
            color: #000;
            text-align: center;
        }

        .modal.fade .modal-bottom,
        .modal.fade .modal-left,
        .modal.fade .modal-right,
        .modal.fade .modal-top {
            position: fixed;
            z-index: 1055;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: 0;
            max-width: 100%
        }

        .modal.fade .modal-right {
            left: auto !important;
            transform: translate3d(100%, 0, 0);
            transition: transform .3s cubic-bezier(.25, .8, .25, 1)
        }

        .modal.fade.show .modal-bottom,
        .modal.fade.show .modal-left,
        .modal.fade.show .modal-right,
        .modal.fade.show .modal-top {
            transform: translate3d(0, 0, 0)
        }

        .w-xl {
            width: 320px
        }

        .modal-content,
        .modal-footer,
        .modal-header {
            border: none
        }

        .list-group.no-radius .list-group-item {
            border-radius: 0 !important
        }

        .btn-light {
            color: #212529;
            background-color: #f5f5f6;
            border-color: #f5f5f6
        }

        .btn-light:hover {
            color: #212529;
            background-color: #e1e1e4;
            border-color: #dadade
        }

        .modal-footer {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            padding: 1rem;
            border-top: 1px solid rgba(160, 175, 185, .15);
            border-bottom-right-radius: .3rem;
            border-bottom-left-radius: .3rem
        }
    </style>
    <style>
        .context_btn:hover {
            cursor: pointer;
        }

        .context_buttons {
            padding: 10px 10px;
        }

        .context_buttons .row {
            padding: 5px 0px;
        }

        .context_buttons .row:hover {
            cursor: pointer;
            box-shadow: 0px 0px 3px #ccc;
            border-radius: 10px;
            color: red;

        }

        #contxt {
            position: absolute;
            z-index: 20 !important;
            background-color: white;
            top: 0;
            left: 0;
        }

        .context_show {
            display: block !important;
        }

        .context_hide {
            display: none !important;
        }

        .context_blur {
            width: 100% !important;
            height: 100vh !important;
            background: rgba(0, 0, 0, 0.2);
            position: absolute;
            top: 0;
            left: 0;
            z-index: 10;
        }
    </style>
</head>

<body>
    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Navbar Start -->
        <?php require("assets/includes/header.php"); ?>
        <!-- Navbar End -->
        <!-- Sidebar Start -->
        <?php require("assets/includes/aside.php"); ?>
        <!-- Sidebar End -->


        <main class="main--container bg-white">
            <!-- main content -->
            <section class="main--content new_area">
                <div class="title table-hover table-responsive">
                    <h1>Manage <span class="public">notes</span></h1>
                </div>
                <form class="example">
                    <input type="text" placeholder="Search notes ..." name="search" id="search_box">
                    <button type="submit"><i class="fa fa-search" id="option"></i></button>
                </form>
                <div class="container" style="height:70vh;overflow-y:scroll">
                    <table class="table table-hover table-responsive-lg">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>id</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Published Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="notes_show_box">
                            <!-- <tr class="notes_row">
                                <td>1</td>
                                <td>1</td>
                                <td>Doe</td>
                                <td>Active</td>
                                <td>12 Jan 2021</td>
                                <td><i class="fa fa-ellipsis-v action_btn" aria-hidden="true"></i></td>
                            </tr> -->
                        </tbody>
                    </table>
                    <div class="loading_screen text-center">
                        <h1><i class="fa fa-spinner fa-spin"></i> loading....</h1>
                    </div>
                </div>
            </section>
            <div id="contxt" class="context_hide" style="box-shadow:0px 0px 8px -1px #ccc;border-radius:10px;padding:10px  10px;text-align:center;width:180px">
                <small>Options</small>
                <div class="context_buttons mt-1 w-100 text-left">

                    <div class="row  my-2 view_notes_btn">
                        <div class="col-2">
                            <i class="fa fa-external-link-square-alt"></i>
                        </div>
                        <div class="col-8"> View </div>
                    </div>


                    <div class="row  my-2 edit_notes_btn">
                        <div class="col-2">
                            <i class="fa fa-pen-square"></i>
                        </div>
                        <div class="col-8"> Edit </div>
                    </div>

                    <div class="row  my-2 notes_delete_btn">
                        <div class="col-2">
                            <i class="fa fa-trash-alt"></i>
                        </div>
                        <div class="col-8"> Delete </div>
                    </div>

                    <!-- <div class="row  my-2">
                        <div class="col-2">
                            <i class="fas fa-eye-slash"></i>
                        </div>
                        <div class="col-8"> Inactive </div>
                    </div> -->

                </div>
            </div>

            <div class="context_blur context_hide"> </div>

            <!-- main content -->
            <!--  edit notes modal  start  -->
            <div id="modal-top" class="modal fade" data-backdrop="true">
                <div class="modal-dialog modal-top">
                    <div class="modal-content no-radius">
                        <div class="modal-header">
                            <div class="modal-title text-md">Edit <span class="notes_name_header text-danger"> </span> </div> <button class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" style="height:80vh;overflow-y:scroll">
                            <form>
                                <div class="form-group">
                                    <label for="addnote">Title<span class="star">*</span></label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Note Title">
                                </div>
                                <div class="form-group">
                                    <label for="addnote_des">Short Description <small> (meta) </small> <span class="star">*</span></label>
                                    <textarea class="form-control" name="short_desc" placeholder="Enter a short description" id="short_desc">

                            </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="course_description">Full Content <span class="star">*</span></label>

                                    <textarea id="editor" name="course_description">

                                </textarea>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="choose_faculty">Choose Faculty <span class="star">*</span>
                                            </label>
                                            <select class="form-select status form-control" aria-label="Default select example" name="faculty" id="faculty">
                                                <option value="0">None</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="choose_course">Choose Course <span class="star">*</span></label>
                                            <select class="form-select status form-control" aria-label="Default select example" name="course" id="course">
                                                <option value="0">None</option>
                                            </select>
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="choose_semester">Choose Semester <span class="star">*</span>
                                            </label>
                                            <select class="form-select status form-control" aria-label="Default select example" name="semester" id="semester">
                                                <option value="0">None</option>
                                                <option value="1">Semester 1</option>
                                                <option value="2">Semester 2</option>
                                                <option value="3">Semester 3</option>
                                                <option value="4">Semester 4</option>
                                                <option value="5">Semester 5</option>
                                                <option value="6">Semester 6</option>
                                                <option value="7">Semester 7</option>
                                                <option value="8">Semester 8</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="tags">Tags</label>
                                                <input type="text" id="tags" name="tags" class="form-control" placeholder="bba notes,bba 6 sem notes" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="subject">subject</label>
                                                <select id="subject" name="subject" class="form-control">
                                                    <option value="0"> None </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="status">Status <span class="star">*</span></label>
                                            <select class="form-select status form-control" name="status" id="status">
                                                <option value="active">Active</option>
                                                <option value="inactive">Inactive</option>
                                            </select>
                                        </div>

                                    </div>

                                </div>

                                <div> </div>

                                <div class="form-group">
                                    <label>Upload PDF</label>
                                    <input type="file" accept=".pdf" class="form-control" id="file" name="file">
                                </div>
                                <div class="exist_pdf"></div>

                                <button class="btn_save" type="submit">Save</button>
                            </form>
                        </div>
                        <div class="modal-footer"><button type="button" class="btn btn-light" data-dismiss="modal">Close</button></div>
                    </div>
                </div>
            </div>
            <!-- edit notes modal end -->

        </main>
        <!-- Main Container End -->
    </div>
    <!-- Wrapper End -->


    <?php require("assets/includes/cdnjs.php"); ?>
    <!-- script -->
    <script>
        function show() {
            $("#contxt").removeClass("context_hide");
            $("#contxt").addClass("context_show");
            $(".context_blur").addClass("context_show");
            $(".context_blur").removeClass("context_hide");
        }

        function hide() {
            $("#contxt").addClass("context_hide");
            $("#contxt").removeClass("context_show");
            $(".context_blur").removeClass("context_show");
            $(".context_blur").addClass("context_hide");
        }

        function context() {

            $(".action_btn").each(function() {
                $(this).click(function(event) {
                    var parent = this.parentElement.parentElement;
                    var left = event.pageX - 150 + "px";
                    var top = event.pageY - 50 + "px";
                    $("#contxt").css({
                        left: left,
                        top: top
                    });

                    let data_id = $(parent).attr("data-id");
                    sessionStorage.setItem("active_notes_id", data_id);
                    show();
                });
            });

            $(".notes_row").each(function() {
                $(this).on("contextmenu", function(event) {
                    var parent = this;
                    var left = event.pageX - 150 + "px";
                    var top = event.pageY - 50 + "px";
                    $("#contxt").css({
                        left: left,
                        top: top
                    });
                    show();
                    let data_id = $(parent).attr("data-id");
                    sessionStorage.setItem("active_notes_id", data_id);
                    return false;
                });
            });

            $(".context_blur").click(function() {
                hide();
            });

        }
        context();
    </script>
    <script type="text/javascript">
        CKEDITOR.replace('editor');
    </script>
    <script src="https://sdk.amazonaws.com/js/aws-sdk-2.855.0.min.js"></script>

    <script>
        $(document).ready(function() {
            $("form").submit(function(event) {
                event.preventDefault();
            });

            function getAllFaculty() {
                let sel_box = $("#faculty");
                $.ajax({
                    type: "POST",
                    url: "php/faculty.php",
                    data: {
                        for: "get_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                    },
                    success: function(response) {
                        $(sel_box).html("");
                        $(sel_box).append("<option value='0'> None </option>");
                        let data = response.data;
                        if (response.status == "success") {

                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                        }
                    }
                })
            };
            getAllFaculty();

            function getAllCourse() {
                let sel_box = $("#course");
                $.ajax({
                    type: "POST",
                    url: "php/course.php",
                    data: {
                        for: "load_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                    },
                    success: function(response) {
                        $(sel_box).html("");
                        $(sel_box).append("<option value='0'> None </option>");
                        let data = response.data;
                        if (response.status == "success") {
                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].table_name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                        }
                    }
                })
            };
            getAllCourse();



            function getAllSubject() {
                let sel_box = $("#subject");
                $.ajax({
                    type: "POST",
                    url: "php/subject.php",
                    data: {
                        for: "load_all"
                    },
                    beforeSend: function() {
                        $(sel_box).html("<option> Loading ...</option>");
                    },
                    success: function(response) {
                        $(sel_box).html("");
                        $(sel_box).append("<option value='0'> None </option>");
                        let data = response.data;
                        if (response.status == "success") {
                            for (let i = 0; i < data.length; i++) {
                                let name = data[i].table_name;
                                let id = data[i].id;
                                $(sel_box).append("<option value='" + id + "'> " + name + " </option>");
                            }
                        }
                    }
                })
            };
            getAllSubject();

        });
        $(document).ready(function() {
            // get all notes 
            function load_all() {
                $.ajax({
                    type: "POST",
                    url: "php/notes.php",
                    data: {
                        for: "load_all",
                    },
                    beforSend: function() {
                        swal({
                            text: "Processing....",
                            button: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            closeOnConfirm: false,
                            closeOnClickOutside: false
                        });
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.status == "success") {
                            var notes_data = JSON.stringify(data);
                            sessionStorage.setItem("loaded_notes", notes_data);
                            show_all(data);
                        } else if (data.status == "not found") {
                            swal("warning", "No data found", "warning");
                        }
                    }
                })
            };
            load_all();

            function show_all(data) {
                $(".loading_screen").addClass("d-none");
                let list = data.data;
                var table_tr = $(".notes_show_box");
                $(table_tr).html("");
                let counter = 0;
                for (let i = 0; i < list.length; i++) {
                    counter++;
                    let name = list[i].product_name;
                    let id = list[i].id;
                    let updated_at = list[i].updated_date;
                    let status = list[i].status;
                    let description = list[i].description;
                    $(table_tr).append('<tr class="notes_row" data-id="' + id + '"><td> ' + counter + '</td> <td> ' + id + '</td> <td class="text-left">  ' + name + ' </td><td> ' + status + ' </td><td> ' + updated_at + ' </td><td><i class="fa fa-ellipsis-v action_btn" aria-hidden="true"/></td></tr>');

                }
                context();
                search_notes(list);
            };

            function search_notes(list) {
                $("#search_box").on("input", function() {
                    let keyword = this.value;
                    let counter = 0;
                    var table_tr = $(".notes_show_box");
                    $(table_tr).html("");
                    for (let i = 0; i < list.length; i++) {

                        let name = list[i].product_name;
                        let id = list[i].id;
                        let updated_at = list[i].updated_date;
                        let status = list[i].status;
                        let description = list[i].description;
                        var rgxp = new RegExp(keyword, "gi");
                        if (name.match(rgxp) || id.match(rgxp) || updated_at.match(rgxp) || status.match(rgxp)) {
                            counter++;
                            $(table_tr).append('<tr class="notes_row" data-id="' + id + '"><td> ' + counter + '</td> <td> ' + id + '</td> <td class="text-left"> ' + name + ' </td><td> ' + status + ' </td><td> ' + updated_at + ' </td><td><i class="fa fa-ellipsis-v action_btn" aria-hidden="true"/></td></tr>');
                        }
                    }
                    context();
                });
            };

            function delete_notes() {
                $(".notes_delete_btn").click(function() {
                    let active_id = sessionStorage.getItem("active_notes_id");
                    if (active_id !== null) {
                        swal({
                            title: "Are you sure?",
                            text: "Once deleted, you will not be able to recover this notes !",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    type: "POST",
                                    url: "php/notes.php",
                                    data: {
                                        for: "delete_notes",
                                        id: active_id
                                    },
                                    beforeSend: function() {
                                        swal({
                                            text: "Processing....",
                                            button: false,
                                            allowEscapeKey: false,
                                            allowOutsideClick: false,
                                            closeOnConfirm: false,
                                            closeOnClickOutside: false
                                        });
                                    },
                                    success: function(response) {
                                        swal.close();
                                        if (response.status == "success") {
                                            remove_array(active_id);
                                            swal("Poof! notes has been deleted!", {
                                                icon: "success",
                                            });
                                            hide();
                                        } else {
                                            swal("Opps!", "we cannot process at the moment, please try again later!", "error");
                                        }
                                    }
                                })
                            }
                        });
                    }
                })
            };
            delete_notes();

            function remove_array(active_id) {
                let data_list = JSON.parse(sessionStorage.getItem("loaded_notes"));
                list = data_list.data;
                remainingArr = list.filter(data => data.id !== active_id);
                data_list.data = remainingArr;
                sessionStorage.setItem("loaded_notes", JSON.stringify(data_list));
                show_all(data_list);
            }

            function edit_notes() {

                $(".edit_notes_btn").click(function() {
                    let active_id = sessionStorage.getItem("active_notes_id");
                    if (active_id !== null) {
                        let data = JSON.parse(sessionStorage.getItem("loaded_notes"));
                        let list = data.data;
                        remainingArr = list.filter(data => data.id == active_id);
                        let name = remainingArr[0].product_name;
                        let tags = remainingArr[0].keyword;
                        let status = remainingArr[0].status;
                        let short_desc = remainingArr[0].short_desc;
                        let faculty = remainingArr[0].faculty;
                        let course = remainingArr[0].course;
                        let sems = remainingArr[0].sems;
                        let sub = remainingArr[0].sub;
                        let pdf_url = remainingArr[0].pdf_url;
                        let description = remainingArr[0].long_desc;
                        let updated_at = remainingArr[0].updated_at;
                        let created_at = remainingArr[0].created_at;
                        $(".notes_name_header").html(name);
                        $("#notes").val(name);
                        var objEditor = CKEDITOR.instances["editor"];
                        var dec = objEditor.setData(description);
                        event.preventDefault();
                        $("#title").val(name);
                        $("#faculty").val(faculty);
                        $("#course").val(course);
                        $("#semester").val(sems);
                        $("#tags").val(tags);
                        $("#status").val(status);
                        $("#short_desc").val(short_desc);
                        $("#subject").val(sub);
                        if (pdf_url == "") {
                            $(".exist_pdf").html('<div class="alert alert-waring bg-info text-light"> this notes has no pdf , please upload a pdf file </div>');
                        } else {
                            $(".exist_pdf").html('<div class="alert alert-waring bg-info text-light"> this notes has a pdf , please review  it once <a href="' + pdf_url + '" target="_blank"> preview </a> </div>');
                        }
                        $("#modal-top").modal("show");
                    }
                });

            }
            edit_notes();

            function updatenotes() {
                $(".btn_save").click(function() {
                    let title = $("#title").val();
                    let faculty = $("#faculty").val();
                    let course = $("#course").val();
                    let semester = $("#semester").val();
                    let tags = $("#tags").val();
                    let status = $("#status").val();
                    let short_desc = $("#short_desc").val();
                    let subject = $("#subject").val();
                    let file = $("#file")[0].files;
                    var pdf_file = $("#file")[0].files[0];

                    var objEditor = CKEDITOR.instances["editor"];
                    var description = objEditor.getData();
                    if (title.trim().length == 0 && short_desc.trim().length == 0 && tags.trim().length == 0) {
                        swal("warning", "All field are required", "warning");
                    } else {
                        let active_id = sessionStorage.getItem("active_notes_id");
                        let notes_data = JSON.parse(sessionStorage.getItem("loaded_notes"));
                        let list = notes_data.data;
                        remainingArr = list.filter(data => data.id == active_id);
                        let file_key = remainingArr[0].file_key;
                        let pre_pdf_url = remainingArr[0].pdf_url;
                        var size = remainingArr[0].size;
                        if (file.length !== 0) {
                            size = pdf_file.size;
                            // aws config file
                            // cofig aws s3 
                            var config = AWS.config;
                            config.update({
                                accessKeyId: "AKIASAWDBAK5WAWBA67V",
                                secretAccessKey: "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",
                            });
                            config.region = "ap-south-1";

                            var s3 = new AWS.S3();
                            // delete preview notes auto

                            var delete_params = {
                                Bucket: 'www.notesocean.com',
                                Key: file_key,
                            };

                            upload_new_obj();

                            function upload_new_obj() {
                                var upload_param = {
                                    Bucket: 'www.notesocean.com',
                                    Key: title + ".pdf",
                                    Body: pdf_file,
                                    ACL: "public-read"
                                };
                                s3.upload(upload_param).on("httpUploadProgress", function(event) {
                                    swal({
                                        text: "Please wait.... file uploading....",
                                        button: false,
                                        allowEscapeKey: false,
                                        allowOutsideClick: false,
                                        closeOnConfirm: false,
                                        closeOnClickOutside: false
                                    });
                                }).send(function(error, data) {
                                    //  complte uploaded 
                                    swal.close();
                                    if (error == null) {
                                        var pdf_url = data.Location;
                                        var file_key = data.Key;
                                        update_into_db(pdf_url, file_key);
                                    } else {
                                        swal("error", "somthing went wrong,please try again", "error");
                                    }
                                });
                            }
                        } else {
                            update_into_db(pre_pdf_url, file_key);
                        }

                        function update_into_db(pdf_url, file_key) {
                            $.ajax({
                                type: "POST",
                                url: "php/notes.php",
                                data: {
                                    for: "update",
                                    id: active_id,
                                    title: title,
                                    faculty: faculty,
                                    course: course,
                                    subject: subject,
                                    short_desc: short_desc,
                                    description: description,
                                    status: status,
                                    semester: semester,
                                    tags: tags,
                                    pdf_url: pdf_url,
                                    size: size,
                                    file_key: file_key
                                },
                                beforeSend: function() {
                                    swal({
                                        text: "Processing....",
                                        button: false,
                                        allowEscapeKey: false,
                                        allowOutsideClick: false,
                                        closeOnConfirm: false,
                                        closeOnClickOutside: false
                                    });
                                },
                                success: function(data) {
                                    swal.close();

                                    if (data.status == "success") {
                                        swal("success", "notes updated successfully", "success");
                                        $("form").trigger("reset");
                                        remainingArr[0].product_name = title;
                                        remainingArr[0].short_desc = short_desc;
                                        remainingArr[0].long_desc = description;
                                        remainingArr[0].keyword = tags;
                                        remainingArr[0].faculty = faculty;
                                        remainingArr[0].course = course;
                                        remainingArr[0].sems = semester;
                                        remainingArr[0].sub = subject;
                                        remainingArr[0].pdf_url = pdf_url;
                                        remainingArr[0].size = size;
                                        remainingArr[0].file_key = file_key;
                                        remainingArr[0].status = status;
                                        sessionStorage.setItem("loaded_notes", JSON.stringify(notes_data));
                                        show_all(notes_data);
                                        $("#modal-top").modal("hide");
                                    } else if (data.status == "exist") {
                                        swal("warning", "This notes is already exist", "warning");
                                    } else {
                                        swal("error", "somthing went wrong,please try again", "error");
                                    }
                                }
                            })
                        }

                    }

                });

            };
            updatenotes();
            function view_notes() {
                $(".view_notes_btn").click(function() {
                    let active_id = sessionStorage.getItem("active_notes_id");
                    $.ajax({
                    type: "POST",
                    url: "php/get_pub_url.php",
                    data: {
                        notes_id: active_id,
                    },
                    beforeSend: () => {

                    },
                    success: (result) => {
                      result = JSON.parse(result);
                        // console.log(result);
                        if (result.status == "success") {
                            open(result.data, "_blank");
                        } else {
                            swal("erro", "somthing went wrong , please try after sometimes", "error");
                        }
                    }
                })
                    
                })
            };
            view_notes();

        });
    </script>
    <!-- script -->

</body>

</html>