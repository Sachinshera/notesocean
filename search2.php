<?php
$query = "";
require("includes/db.php");
if (isset($_GET["q"])) {
    $query = $_GET["q"];
} else {
     header("Location:".$home_page);
}
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php echo $query; ?> </title>
    <meta name="description" content="Search  your notes on notesocean.com">
    <meta name="robots" content="index,follow">
    <?php include("includes/cdn.php") ?>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Heebo:wght@500&display=swap');

        .card {
            width: 80%;
            border: none;
            border-radius: 20px;
            height: 90vh;
            box-shadow: 0px 0px 5px 1px #ccc;
            margin: 10px 0px !important;
        }

        .form-control {
            border-radius: 7px;
            border: 1.5px solid #E3E6ED
        }

        input.form-control:focus {
            box-shadow: none;
            border: 1.5px solid #E3E6ED;
            background-color: #F7F8FD;
            letter-spacing: 1px
        }

        .btn-primary {
            background-color: #5878FF !important;
            border-radius: 7px
        }

        .btn-primary:focus {
            box-shadow: none
        }

        .text {
            font-size: 13px;
            color: #9CA1A4
        }

        .price {
            background: #F5F8FD;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            width: 97px
        }

        .flex-row {
            border: 1px solid #F2F2F4;
            border-radius: 10px;
            margin: 0 1px 0
        }

        .flex-column p {
            font-size: 14px
        }

        span.mb-2 {
            font-size: 12px;
            color: #8896BD
        }

        h5 span {
            color: #869099
        }

        .item_row {
            height: 100% !important;
            overflow: scroll;
        }

        .item_row b {
            color: red;
        }

        @media screen and (max-width: 768px) {
            .card {
                /* display: flex;
        justify-content: center; */
                text-align: center;
                width: 100%;
                padding-top: 10px;
                height: 85vh;
                padding-bottom: 20%;

                margin-top: 10px !important;
            }

            .price {
                border: none;
                margin: 0 auto
            }
        }

        ::-webkit-scrollbar {
            display: none;
        }
    </style>
    <!-- loader css -->
    <style>
        .lds-default {
            display: block;
            width: 100px;
            height: 100px;
            position: relative;
            /* top: 40% !important; */
            margin: 0 auto;
        }

        .lds-default div {
            position: absolute;
            width: 5px;
            height: 5px;
            background-color: #E21717;
            border-radius: 50%;
            animation: lds-default 1.2s linear infinite;
        }

        .lds-default div:nth-child(1) {
            animation-delay: 0s;
            top: 37px;
            left: 66px;
        }

        .lds-default div:nth-child(2) {
            animation-delay: -0.1s;
            top: 22px;
            left: 62px;
        }

        .lds-default div:nth-child(3) {
            animation-delay: -0.2s;
            top: 11px;
            left: 52px;
        }

        .lds-default div:nth-child(4) {
            animation-delay: -0.3s;
            top: 7px;
            left: 37px;
        }

        .lds-default div:nth-child(5) {
            animation-delay: -0.4s;
            top: 11px;
            left: 22px;
        }

        .lds-default div:nth-child(6) {
            animation-delay: -0.5s;
            top: 22px;
            left: 11px;
        }

        .lds-default div:nth-child(7) {
            animation-delay: -0.6s;
            top: 37px;
            left: 7px;
        }

        .lds-default div:nth-child(8) {
            animation-delay: -0.7s;
            top: 52px;
            left: 11px;
        }

        .lds-default div:nth-child(9) {
            animation-delay: -0.8s;
            top: 62px;
            left: 22px;
        }

        .lds-default div:nth-child(10) {
            animation-delay: -0.9s;
            top: 66px;
            left: 37px;
        }

        .lds-default div:nth-child(11) {
            animation-delay: -1s;
            top: 62px;
            left: 52px;
        }

        .lds-default div:nth-child(12) {
            animation-delay: -1.1s;
            top: 52px;
            left: 62px;
        }

        @keyframes lds-default {

            0%,
            20%,
            80%,
            100% {
                transform: scale(1);
            }

            50% {
                transform: scale(1.5);
            }
        }
    </style>

</head>

<body>
    <?php include("includes/header.php") ?>
    <section>
        <div class="container d-flex justify-content-center" style="margin-top:60px">
            <div class="card mt-2 p-4">
                <form action="<?php echo $home_page ?>/search2">
                    <div class="input-group mb-3"> <input type="text" value="<?php echo $query; ?>" name="q" class="form-control" placeholder="Search notes">
                        <div class="input-group-append"><button class="btn" style="color:white;background:red" type="submit"><i class="fas fa-search"></i></button></div>
                    </div>
                </form>
                <div class="lds-default loader">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                <div class="row">
                    <div class="col-6 text-left">
                        <span class="text  total_results"></span>
                    </div>
                    <div class="col-6 text-right">
                        <span class="text total_time"></span>
                    </div>
                </div>

                <div class="item_row">

                </div>
                <!-- items  -->
                <!-- <div class="d-flex flex-row justify-content-between mb-3">
                    <div class="d-flex flex-column p-3">
                        <p class="mb-1">Logo and marketing material design for Bakery</p> <small class="text-muted">8 days remaining</small>
                    </div>
                    
                </div> -->
                <!-- <div class="d-flex flex-row justify-content-between mx-1">
                    <div class="d-flex flex-column p-3">
                        <p class="mb-1">Need to create brand guidelines for my brand</p> <small class="text-muted">12 days remaining</small>
                    </div>
                </div> -->
                <!-- items -->
            </div>

        </div>
    </section>


    <script>
        $(document).ready(function() {
            var formattedTotalResults = 0;
            var start_index = 1;
            var pre_index = 0;
            var status  = "no";

            function get_data() {
                $.ajax({
                    type: "GET",
                    url: "https://www.googleapis.com/customsearch/v1",
                    data: {
                        key: "AIzaSyCoaG8ZRARispA7DYvijknsa0PXF-e2nWo",
                        cx: "5a56d29a974ecc02f",
                        q: "<?php echo $query; ?>",
                        siteSearch: "notesocean.com/about",
                        siteSearchFilter: "e",
                        start: start_index,
                        fileType:"html"
                    },
                    beforeSend: function() {},
                    success: function(data) {
                        // console.log(data);
                        var searchInformation = data.searchInformation;
                        var formattedSearchTime = searchInformation.formattedSearchTime;
                        formattedTotalResults = searchInformation.totalResults;
                       
                        if (formattedTotalResults !== "0") {
                            status = "ok";
                            $(".loader").addClass("d-none");
                            $(".total_results").html("Total Notes : " + formattedTotalResults);
                            $(".total_time").html("Total Time : " + formattedSearchTime + " Sec");
                            var items = data.items;
                            for (var i = 0; i < items.length; i++) {
                                var title = items[i].htmlTitle;
                                var link = items[i].link;
                                var description = items[i].pagemap.metatags[0]["og:description"];
                                $(".item_row").append('<a style="text-decoration: none;color:black" href="' + link + '"> <div  class="d-flex flex-row justify-content-between mb-3 shadow-sm"><div class="d-flex flex-column p-3"><p class="mb-1 text-left"> ' + title + ' </p><small class="text-muted text-left"> ' + description + ' </small></div></div> </a>');
                            }
                        }
                        start_index = start_index + 10;
                        pre_index = start_index;
                        
                    }
                });
            }

            get_data();
            $(".item_row").scroll(function() {
                var box_height = $(".item_row").height();
                // console.log($(".item_row"));
                var scroll_height = $(".item_row")[0].scrollHeight;
                var scroll_top = $(".item_row").scrollTop();
                // for mobile devicesType
                if ($(window).width() < 760) {
                    var h_percent = (scroll_height * 95) / 100;
                } else {
                    var h_percent = (scroll_height * 50) / 100;
                }

                // console.log(h_percent);
                // console.log(scroll_top);
                if (scroll_top >= scroll_height-box_height) {
                    if (formattedTotalResults > start_index) {
                                    
                        get_data();
                        if(pre_index != start_index){
                            
                            
                           
                        // alert("a");
                        }
                       
                       
                    }
                }
                console.log("index: " + start_index);
                console.log("pre: " + pre_index);
                //  console.log("H"+h_percent);
                //         console.log("S"+scroll_top);

            });
        });
    </script>
</body>

</html>