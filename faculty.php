<?php require_once("includes/db.php"); ?>
<!DOCTYPE html>
<html lang=" en-US" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="utf-8">
    <title>faculty - Notesocean</title>
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and faculty. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <meta name="keyword" content="basic computer notes for students,ms word notes pdf,note to pdf,lecture notes in,,mechanical engineering,engineering mechanics notes,lecture notes in electrical engineering,total quality management notes,basic electrical engineering notes,handwritten notes to text,highway engineering notes,made easy handwritten notes,irrigation engineering notes,business management notes,lacture notes,handwritten notes,pdf notes,engineering notes,management notes,notesocean, notes ocean,notes ocean bba notes">
    <meta name="robots" content="index,follow">
    <!-- og start  -->
    <meta property="og:site_name" content="Notes Ocean" />
    <meta property="og:title" content="faculty - Notesocean " />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://notesocean.com/faculty" />
    <meta name="og:description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and faculty. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- meta end  -->
    <?php include("includes/cdn.php"); ?>

    <!-- styles start here -->
    <style type="text/css">
        .mt-100{
            margin-top: 100px;
        }
        .mb-100{
            margin-bottom: 100px;
        }
        
        .banner_main{
            height: 50vh;
            background-image:linear-gradient(rgba(0,0,0,0.3),
                rgba(0,0,0,0.3)), url('assest/img/Course.jpg');
            background-size: cover;
            display:flex;
            justify-content: center;
            align-items: center;

        }
        .banner_child h1{
            color: #fff;
            font-weight: 900;
            font-size: 4rem;
        }
        .rs-categories{
            background: #F3F8F9;
            padding: 100px 0;
        }
        .rs-categories a{
            text-decoration: none;
        }
        .rs-categories.style1 .categories-item {
            border: 1px solid #dfe9eb;
            background: #ffffff;
            overflow: hidden;
            padding: 30px;
            display: block;
            color: #505050;
            border-radius: 5px;
            display: flex;
            align-items: center;
        }

        .rs-categories.style1 .categories-item .icon-part {
            float: left;
            margin-right: 25px;
            width: 70px;
            height: 70px;
            line-height: 70px;
            border-radius: 100%;
            background: rgba(22, 170, 202, 0.2);
            text-align: center;
            transition: all 0.3s ease;
        }

        .rs-categories.style1 .categories-item .icon-part img {
            -webkit-transition: all 0.4s ease;
            transform: scale(1);
        }

        .rs-categories.style1 .categories-item .content-part .title {
            color: #111111;
            margin-bottom: 5px;
            font-size: 22px;
            transition: all 0.3s ease;
        }

        .rs-categories.style1 .categories-item:hover {
            background: #FF0000;
            color: #ffffff;
            border-color: #FF0000;
        }

        .rs-categories.style1 .categories-item:hover .icon-part {
            background: #ffffff;
        }

        .rs-categories.style1 .categories-item:hover .icon-part img {
            transform: scale(0.9);
        }

        .rs-categories.style1 .categories-item:hover .content-part .title {
            color: #ffffff;
        }

        .rs-categories.main-home .categories-items {
            position: relative;
            transition: all 0.3s ease;
        }


        .readon{
            color: #000;
            text-transform: uppercase;
            font-size: 1.2rem;
            font-weight: 700;
            padding: 10px 30px;
            border: 2px solid #FF0000;
            border-radius: 50px;
        }
        .title{
            font-size: 3rem;
            font-weight: 700;
            color: #000;
        }
        .sub-title{
            color: #FF0000;
            text-transform: uppercase;
            font-size: 1.2rem;
            font-weight: 700;
        }

    </style>
</head>

<body>
    <!-- Header -->

    <?php include("includes/header.php") ?>
   <!-- header end  -->

   <!-- content start here -->

   <!-- <div class="banner_main">
        <div class="banner_child">
            <h1> faculty</h1>
        </div>
   </div> -->


<!-- Categories Section Start -->
        <div id="rs-categories " class="rs-categories style1 bg-white pt-5">
            <div class="container">
                <div class="row mb-5 md-mb-3">
                    <div class="col-md-6 sm-mb-30">
                        <div class="sec-title">
                            <div class="sub-title primary">faculty</div>
                            <h2 class="title mb-0">Our  faculty </h2>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="btn-part text-right sm-text-left">
                            <a href="#" class="readon">View All Course</a>
                        </div>
                    </div> -->
                </div>
                <div class="row">
                <!-- show all faculties start -->

                <?php 
                include("includes/clean.php");
                $get_fac = $db->query("SELECT * FROM faculties");
                if($get_fac){
                    if($get_fac->num_rows!==0){
                        while($fac_row = $get_fac->fetch_assoc()){
                        $fac_name = $fac_row["name"];
                        $fac_desc = $fac_row["description"];
                        $fac_id = $fac_row["id"];
                        $get_faculty_num = $db->query("SELECT * FROM course_table WHERE faculty_id = '$fac_id' AND status = 'active'");
                        if($get_faculty_num){
                            $num_of_course = $get_faculty_num->num_rows;
                        }

                    echo '  <div class="col-lg-4 col-md-6 mb-3" data-wow-delay="300ms" data-wow-duration="2000ms">
                        <a class="categories-item" href="'.$home_page.'/faculty/'.just_clean($fac_name).'.html">
                            <div class="content-part">
                                <h4 class="title"> '.$fac_name.' </h4>
                                <span class="faculty"> '.$num_of_course.' Courses</span>
                                <p>   <p> '.substr($fac_desc, 0,50).'  </p> </p>
                            </div>
                        </a>
                    </div>';

                        }
                    }
                }
                
                
                ?>
                </div>
            </div>
        </div>
        <!-- Categories Section End -->
   <!-- content end  here -->

<!-- footer start here -->
    <?php include("includes/footer.php"); ?>
</body>

</html>