<?php
require_once("includes/db.php");
if (isset($_GET["notes_id"]) && isset($_GET["filename"])) {
    $filename = $_GET["filename"];
    $notes_id = htmlentities(mysqli_real_escape_string($db, strip_tags(trim($_GET["notes_id"]))));
    $check_exist = $db->query("SELECT pdf_url FROM products WHERE id = $notes_id");
    if ($check_exist) {
        if ($check_exist->num_rows !== 0) {
            $notes_data = $check_exist->fetch_assoc();
            $pdf_url = $notes_data["pdf_url"];
            if ($pdf_url !== "") {
                // echo $pdf_url;
                // echo $filename;
                // Header content type
                header('Content-type: application/pdf');

                header('Content-Disposition: inline; filename="' . $filename . '"');

                header('Content-Transfer-Encoding: binary');

                header('Accept-Ranges: bytes');

                // Read the file
                @readfile($pdf_url);
            }else{
                header("Location:".$home_page."/notfound");
            }
        }else{
            header("Location:".$home_page."/notfound");
        }
    }else{
        header("Location:".$home_page."/notfound");
    }
}else{
    header("Location:".$home_page."/notfound");
}
