<?php 
require("includes/db.php");
// include("includes/clean.php");
function cleanURL($string)
{
    $url = str_replace("'", '', $string);
    $url = str_replace('%20', ' ', $url);
    $url = str_replace('_', ' ', $url);
    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url); // substitutes anything but letters, numbers and '_' with separator
    $url = trim($url, "-");
    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);  // you may opt for your own custom character map for encoding.
    $url = strtolower($url);
    $url = preg_replace('~[^-a-z0-9_]+~', '', $url); // keep only letters, numbers, '_' and separator
    return $url;
}
header("Content-type:application/xml");
echo '<?xml version="1.0" encoding="UTF-8"?>';
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


$get_notes =  $db->query("SELECT * FROM `products` ORDER BY id DESC");
if($get_notes){
    if($get_notes->num_rows!==0){
        while($row = $get_notes->fetch_assoc()){
            $id = $row["id"];
            $name = $row["product_name"];
            $date = $row["updated_date"];
            $date  =  date_create($date);
            $date  =  date_format($date,"Y-m-d");
        
            $crear_name =  cleanURL($name).".html";
            $url = $home_page."/".$id."/".$crear_name; 
    
            echo '<url>

            <loc> '.$url.' </loc>
      
            <lastmod> '.$date.' </lastmod>
      
            <changefreq>weekly</changefreq>
      
            <priority>0.9</priority>
      
         </url>';
        }
    }
}

echo '</urlset>';


