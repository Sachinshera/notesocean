<?php require_once("includes/db.php"); ?>
<!DOCTYPE html>
<html lang=" en-US" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="utf-8">
    <title>Feedback - Notesocean</title>
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <meta name="keyword" content="basic computer notes for students,ms word notes pdf,note to pdf,lecture notes in,,mechanical engineering,engineering mechanics notes,lecture notes in electrical engineering,total quality management notes,basic electrical engineering notes,handwritten notes to text,highway engineering notes,made easy handwritten notes,irrigation engineering notes,business management notes,lacture notes,handwritten notes,pdf notes,engineering notes,management notes,notesocean, notes ocean,notes ocean bba notes">
    <meta name="robots" content="index,follow">
    <!-- og start  -->
    <meta property="og:site_name" content="Notes Ocean" />
    <meta property="og:title" content="Contact us - Notes Ocean " />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://notesocean.com/contact-us" />
    <meta name="og:description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- meta end  -->
    <?php include("includes/cdn.php"); ?>

    <!-- styles start here -->
    <style type="text/css">
    .margin_top_bottom{
        margin: 100px auto;
    }
    .feedback{
    width: 100%;
    max-width: 780px;
    background: #fff;
    
    padding: 25px;
    box-shadow: 1px 1px 16px rgba(0, 0, 0, 0.3);
}
.survey-hr{
  margin:10px 0;
  border: .5px solid #ddd;
}
.feedback form input,
.feedback form textarea{
    width: 100%;
    border: 1px solid #ddd;
}
.star-rating {
   margin: 25px 0 0px;
  font-size: 0;
  white-space: nowrap;
  display: inline-block;
  width: 175px;
  height: 35px;
  overflow: hidden;
  position: relative;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating i {
  opacity: 0;
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 20%;
  z-index: 1;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating input {
  -moz-appearance: none;
  -webkit-appearance: none;
  opacity: 0;
  display: inline-block;
  width: 20%;
  height: 100%;
  margin: 0;
  padding: 0;
  z-index: 2;
  position: relative;
}
.star-rating input:hover + i,
.star-rating input:checked + i {
  opacity: 1;
}
.star-rating i ~ i {
  width: 40%;
}
.star-rating i ~ i ~ i {
  width: 60%;
}
.star-rating i ~ i ~ i ~ i {
  width: 80%;
}
.star-rating i ~ i ~ i ~ i ~ i {
  width: 100%;
}
.choice {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 20px;
  display: block;
}
span.scale-rating{
margin: 5px 0 15px;
    display: inline-block;
   
    width: 100%;
   
}
span.scale-rating>label {
  position:relative;
    -webkit-appearance: none;
  outline:0 !important;
    border: 1px solid grey;
    height:33px;
    margin: 0 5px 0 0;
  width: calc(10% - 7px);
    float: left;
  cursor:pointer;
}
span.scale-rating label {
  position:relative;
    -webkit-appearance: none;
  outline:0 !important;
    height:33px;
      
    margin: 0 5px 0 0;
  width: calc(10% - 7px);
    float: left;
  cursor:pointer;
}
span.scale-rating input[type=radio] {
  position:absolute;
    -webkit-appearance: none;
  opacity:0;
  outline:0 !important;
    /*border-right: 1px solid grey;*/
    height:33px;
 
    margin: 0 5px 0 0;
  
  width: 100%;
    float: left;
  cursor:pointer;
  z-index:3;
}
span.scale-rating label:hover{
background:#fddf8d;
}
span.scale-rating input[type=radio]:last-child{
border-right:0;
}
span.scale-rating label input[type=radio]:checked ~ label{
    -webkit-appearance: none;
 
    margin: 0;
  background:#fddf8d;
}
span.scale-rating label:before
{
  content:attr(value);
    top: 7px;
    width: 100%;
    position: absolute;
    left: 0;
    right: 0;
    text-align: center;
    vertical-align: middle;
  z-index:2;
}
    </style>
</head>

<body>
    <!-- Header -->

    <?php include("includes/header.php") ?>
   <!-- header end  -->

   <!-- content start here -->

            <div class="feedback margin_top_bottom">
            <p>Dear User,<br>
            Thank you for using <strong>Notesocean</strong>. We would like to know how we performed. Please spare some moments to give us your valuable feedback as it will help us in improving our service.</p>
             
            <h4>Please rate your service experience for the following parameters</h4>
             
            <form method="post" action="#action-url">
            <label>1. Your overall experience with us ?</label><br>
              
            <span class="star-rating">
              <input type="radio" name="rating1" value="1"><i></i>
              <input type="radio" name="rating2" value="2"><i></i>
              <input type="radio" name="rating3" value="3"><i></i>
              <input type="radio" name="rating4" value="4"><i></i>
              <input type="radio" name="rating5" value="5"><i></i>
            </span>
             
              <div class="clear"></div> 
              <hr class="survey-hr">
            <label>2. Friendliness and courtesy shown to you while recieving your vehicle</label><br>
            <span class="star-rating">
              <input type="radio" name="ratings1" value="1"><i></i>
              <input type="radio" name="ratings2" value="2"><i></i>
              <input type="radio" name="ratings3" value="3"><i></i>
              <input type="radio" name="ratings4" value="4"><i></i>
              <input type="radio" name="ratings5" value="5"><i></i>
            </span>
             
             
              <div class="clear"></div> 
              <hr class="survey-hr">
            <label>3. Friendliness and courtesy shown to you while delivery of your vehicle</label><br><br/>
              <div style="color:grey">
                <span style="float:left">
                 POOR
                </span>
                <span style="float:right">
                  BEST
                </span>
                
              </div>
            <span class="scale-rating">
              <label value="1">
              <input type="radio" name="rating" >
              <label style="width:100%;"></label>
              </label>
              <label value="2">
              <input type="radio" name="rating" >
              <label style="width:100%;"></label>
              </label>
              <label value="3">
              <input type="radio" name="rating">
              <label style="width:100%;"></label>
              </label>
              <label value="4">
              <input type="radio" name="rating">
              <label style="width:100%;"></label>
              </label>
              <label value="5">
              <input type="radio" name="rating">
              <label style="width:100%;"></label>
              </label>
              <label value="6">
              <input type="radio" name="rating">
              <label style="width:100%;"></label>
              </label>
              <label value="7">
              <input type="radio" name="rating">
              <label style="width:100%;"></label>
              </label>
              <label value="8">
              <input type="radio" name="rating">
              <label style="width:100%;"></label>
              </label>
              <label value="9">
              <input type="radio" name="rating">
              <label style="width:100%;"></label>
              </label>
              <label value="10">
              <input type="radio" name="rating" value="10">
              <label style="width:100%;"></label>
              </label>
            </span>
             
              <div class="clear"></div> 
              <hr class="survey-hr"> 
            <label for="m_3189847521540640526commentText">4. Any Other suggestions:</label><br/><br/>
            <textarea cols="75" name="commentText" rows="5"></textarea><br>
            <br>
              <div class="clear"></div> 
            <input style="background:#43a7d5;color:#fff;padding:12px;border:0" type="submit" value="Submit your review"> 
            </form>
            </div>




   <!-- content end  here -->

<!-- footer start here -->
    <?php include("includes/footer.php"); ?>
</body>

</html>