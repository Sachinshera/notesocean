<?php require_once("includes/db.php"); ?>
<!DOCTYPE html>
<html lang=" en-US" prefix="og: https://ogp.me/ns#">

<head>
  <meta charset="utf-8">
  <title>Error | Page Not Found - Notesocean</title>
  <!-- meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
  <meta name="keyword" content="basic computer notes for students,ms word notes pdf,note to pdf,lecture notes in,,mechanical engineering,engineering mechanics notes,lecture notes in electrical engineering,total quality management notes,basic electrical engineering notes,handwritten notes to text,highway engineering notes,made easy handwritten notes,irrigation engineering notes,business management notes,lacture notes,handwritten notes,pdf notes,engineering notes,management notes,notesocean, notes ocean,notes ocean bba notes">
  <meta name="robots" content="index,follow">
  <!-- og start  -->
  <meta property="og:site_name" content="Notes Ocean" />
  <meta property="og:title" content="About us - Notes Ocean " />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://notesocean.com/about-us" />
  <meta name="og:description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- meta end  -->
  <?php include("includes/cdn.php"); ?>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
  <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />
  <!-- styles start here -->
  <style type="text/css">
    * {
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
    }

    body {
      padding: 0;
      margin: 0;
    }

    #notfound {
      position: relative;
      height: 90vh;
    }

    #notfound .notfound {
      position: absolute;
      left: 50%;
      top: 50%;
      -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
    }

    .notfound {
      max-width: 520px;
      width: 100%;
      text-align: center;
      line-height: 1.4;
    }

    .notfound .notfound-404 {
      height: 190px;
    }

    .notfound .notfound-404 h1 {
      font-family: 'Montserrat', sans-serif;
      font-size: 146px;
      font-weight: 700;
      margin: 0px;
      color: #232323;
    }

    .notfound .notfound-404 h1>span {
      display: inline-block;
      width: 120px;
      height: 120px;
      background-image: url('assest/img/emoji.png');
      background-size: cover;
      -webkit-transform: scale(1.4);
      -ms-transform: scale(1.4);
      transform: scale(1.4);
      z-index: -1;
    }

    .notfound h2 {
      font-family: 'Montserrat', sans-serif;
      font-size: 22px;
      font-weight: 700;
      margin: 0;
      text-transform: uppercase;
      color: #232323;
    }

    .notfound p {
      font-family: 'Montserrat', sans-serif;
      color: #787878;
      font-weight: 300;
    }

    #input_box  {
      font-family: 'Montserrat', sans-serif;
      display: inline-block;
      padding: 12px 30px;
      font-weight: 700;
      background-color: #ffff;
      text-align: center;
      color: #323232;
      border-radius: 40px;
      text-decoration: none;
      -webkit-transition: 0.2s all;
      transition: 0.2s all;
      margin-top:20px;
    }

    .notfound a:hover {
      opacity: 0.8;
    }

    @media only screen and (max-width: 767px) {
      .notfound .notfound-404 {
        height: 115px;
      }
      #input_box{
        width: 90%;
      }

      .notfound .notfound-404 h1 {
        font-size: 86px;
      }

      .notfound .notfound-404 h1>span {
        width: 86px;
        height: 86px;
      }
    }
  </style>
  <!-- <script type="text/javascript">
function PopUp(hideOrshow) {
    if (hideOrshow == 'hide') document.getElementById('ac-wrapper').style.display = "none";
    else document.getElementById('ac-wrapper').removeAttribute('style');
}
window.onload = function () {
    setTimeout(function () {
        PopUp('show');
    }, 5000);
}
</script> -->
</head>

<body>
  <!-- Header -->

  <?php include("includes/header.php"); include("includes/ago_time.php"); include("includes/clean.php") ?>

  <!-- header end  -->

  <!-- content start here -->

  <div id="notfound">
    <div class="notfound">
      <div class="notfound-404">
        <h1>4<span></span>4</h1>
      </div>
      <h2>Oops! Page Not Be Found</h2>
      <p>The page you are looking for might have been removed had its name changed or is temporarily unavailable.</p>
        <form action="<?php echo $home_page; ?>/search.php">
      <input type="text" name="q" id="input_box" class="form-control" placeholder="Search your notes">
      </form>
    </div>
  </div>  
  <!-- footer start here -->
  <?php include("includes/footer.php"); ?>
</body>

</html>