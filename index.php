 <?php 
 require_once("includes/db.php"); 
date_default_timezone_set("Asia/Kolkata");
include("includes/clean.php");


function httpPost($url, $data)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

?>
<!DOCTYPE html>
<html lang=" en-US" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="utf-8">
    <title>Find the perfect notes</title>
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">
    <meta name="keyword" content="engineering notes,business management notes,lacture notes,handwritten notes,pdf notes,engineering notes,management notes,notesocean, notes ocean,notes ocean bba notes">
    <!-- og start  -->
    <meta property="og:site_name" content="Notes Ocean" />
    <meta property="og:title" content="Find the perfect notes" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://notesocean.com/" />
    <meta name="og:description" content="Notes Ocean is the best destination for students to find perfect notes for every subject and courses. We provide students with perfect study materials for their education, unlimited data storage, platform to earn money.">

    <!-- meta end  -->
    <?php include("includes/cdn.php"); ?>
    <?php echo $ads_script; ?>
    <!-- styles start here -->

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <style>
        .card-img {
            width: 100% !important;
            height: 150px !important;
            object-fit: cover;
        }

        .categories-slide-mobile {
            display: none;
        }

        .card img {
            height: 140px;
        }

        .card-body h4 {
            font-size: 16px;
        }

        .card-body p {
            font-size: 12px;
        }

        .categories-slide {
            margin: 10px 0 50px 0;
            display: block;
        }

        .categories-slide h3 {
            font-size: 24px;
        }
        .categories-slide a{
            font-size: 15px;
        }

        .notes-slide {
            margin: 10px 0;
        }

        .notes-slide h3 {
            font-size: 24px;
        }

        .user-image img {
            width: 30px;
            height: 30px;
            border-radius: 50%;
        }

        .categories {
            padding-top: 10px;
            padding-bottom: 10px;
            border-radius: 25px;
            background-color: #f8f8f8;
        }

        .control-button {
            padding: 10px;
            color: #dedede;
        }

        .control-button:hover {
            text-decoration: none;
        }

        @media (max-width: 767px) {
            .categories-slide {
                display: none;
            }


            .categories-slide-mobile {
                display: block;
                margin: 10px 0 50px 0;
                
            }
            .categories-slide-mobile a{
                font-size: 13px;
            }

            .notes-slide h3 {
                font-size: 20px;
            }

            .categories-slide-mobile h3 {
                font-size: 20px;
            }

            .card-body {
                padding: 0.7rem 1rem 0.3rem 1rem;
            }

            .card-body h4 {
                font-size: 16px;
            }

            .card-body p {
                font-size: 10px;
            }
        }
    </style>

<style type="text/css">
        .mt-100 {
            margin-top: 100px;
        }

        .mb-100 {
            margin-bottom: 100px;
        }

        .banner-search {
            background-image: url('assest/img/notebook.svg');
            height: 55vh;
            background-repeat: no-repeat;
            background-size: cover;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        form.example input[type=text] {
            padding: 10px;
            font-size: 17px;
            border: 1px solid grey;
            border-radius: 10px 0px 0px 10px;
            float: left;
            width: 90%;
            background: #ffff;
            border-right: none;
            text-align: center;
        }

        form.example input[type=text]:focus {
            outline: none;
        }

        form.example button {
            float: left;
            width: 10%;
            padding: 10px;
            background: #ffff;
            color: red;
            font-size: 17px;
            border: 1px solid grey;

            border-radius: 0px 10px 10px 0;
            border-left: none;
            cursor: pointer;
        }

        form.example button:hover {
            background: red;
            color: #ffff;
        }

        form.example::after {
            content: "";
            clear: both;
            display: table;
        }

        .notes-section {
            margin-top: -130px;

        }

        .notes {

            border-radius: 10px;
            /* box-shadow: 0 0 2px 0.5px #ccc; */
            border: 1px solid #ccc;
            background-color: #fff;
            width: 80%;
            margin-left: 10%;
            height: 100%;
            margin-bottom: 20px !important;
        }

        .notes h3,
        h2,
        h4,
        h5 {
            text-align: center;
            margin-bottom: 20px;

        }

        .notes ul {
            list-style: none;
            text-align: left;
        }

        .notes ul li {
            font-size: 12 px;
            line-height: 35px;

            padding: 2px 5px;

        }

        .notes ul li a {
            text-decoration: none;
            color: #323232;

        }

        .btn-danger {
            border-radius: 25px;
            padding: 10px 30px;
        }

        .headings {
            text-align: center;
            color: whitesmoke;

        }

        .headings h1 {
            font-weight: bold;
            padding-top: 25px;
            color: white;
        }

        .public {
            color: red;
        }

        .headings {
            width: 50% !important;
        }

        @media (max-width:768px) {
            .banner-search {
                padding: 5%;
                background-size: cover;
                min-height: 25vh !important;
                margin-top: 18%;
                /* display: inline-block */
                display: block;
                justify-content: top;
                align-items: top;

            }

            .headings h2 {
                font-size: 27px;
                text-align: center;
            }

            .headings p {
                font-size: 13px;
            }

            .headings {
                width: 100% !important;
            }

            .notes {
                /* padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 2px 0.5px #ccc;
            background-color: #fff; */
                width: 100%;
                margin-left: 0%;

            }

            form.example button {

                padding: 10px 0px;
            }

        }
    </style>
</head>

<body>
    <!-- Header -->
    <?php include("includes/header.php") ?>
    <!-- header end  -->
    <!-- content for Deskktop -->
    

    <section class="banner-search">
        <div class="headings text-center ">
            <h1>Find the Perfect <span class="public">Notes</span></h1>
            <p>Notesocean is the best destination for students<br> to find perfect notes for every subject & course.</p>
            <br>

            <form class="example" action="<?php echo $home_page; ?>/search.php">
                <input type="text" placeholder="search your notes" name="q">
                <button type="submit"><i class="fa fa-search"></i></button>

            </form>
            <br><br>
            <br>

        </div>

    </section>

    <section class="categories-slide">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <h3 class="mb-3">All Categories </h3>
                </div>
                <div class="col-6 text-right">
                    <a class="control-button mb-3 mr-1" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </a>
                    <a class="control-button mb-3 " href="#carouselExampleIndicators2" role="button" data-slide="next">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-12">
                    <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php include("php/desktop-tag.php"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="categories-slide-mobile">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <h3 class="mb-3">All Categories </h3>
                </div>
                <div class="col-6 text-right">
                    <a class="control-button mb-3 mr-1" href="#carouselExampleIndicators3" role="button" data-slide="prev">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </a>
                    <a class="control-button mb-3 " href="#carouselExampleIndicators3" role="button" data-slide="next">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-12">
                    <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php  include("php/mobile-tag.php"); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="notes-slide">
        <div class="container-fluid">
            <h3 class="mb-3"> Latest Notes </h3>
            <div class="row">
                <?php
                // get lates notes of page 0 
                $api_key = "1234";
                $today = date("i:s");
                $today = explode(":", $today);
                $minute = $today["0"];
                $second = $today["1"];
                $final_api_key = base64_encode(base64_encode($minute . "-" . $api_key . "-" . $second));
                $url = "https://notesocean.com/api.notesocean.com/v2/" . $final_api_key . "/Z2V0X25vdGVz";
                $data = array("count" => 100);
                $response = json_decode(httpPost($url, $data), true);
                if ($response["status"] == "success") {
                    $notes_data = $response["data"];
                    for ($i = 0; $i < count($notes_data); $i++) {
                        $title =  $notes_data[$i]["title"];
                        $notes_id = $notes_data[$i]["notes_id"];
                        $by = $notes_data[$i]["by"];
                        $before_time = $notes_data[$i]["before_time"];
                        $views = $notes_data[$i]["views"];
                        $thumbnails = $notes_data[$i]["thumbnails"];
                        $profile  = $notes_data[$i]["profile"];
                        $fullname = $profile["fullname"];
                        $picture = $profile["picture"];
                        $user_id = $profile["user_id"];

                        // explode thumbnail 

                        $thumb = explode(",", $thumbnails);
                        $thumb1 = $thumb["0"];
                        echo ' <div class="col-md-3 mb-3">
               <a href="'.$home_page."/".$notes_id."/".just_clean($title).'.html" style="text-decoration:none;color:black">  <div class="card">
                    <img class="card-img" alt="' . $title . '" src="' . $thumb1 . '">
                    <div class="card-body">
                        <div class="row">
                            
                            <div class="col-md-12 col-12 pl-4">
                                <h4> ' . $title . ' </h4>
                                <div class="row">
                                <div class="col user-image">
                                <img src="' . $picture . '" alt="' . $fullname . '">
                            </div>
                                    <div class="col">
                                        <p> ' . $fullname . '  </p>
                                    </div>
                                    <div class="col">
                                        <p> ' . $views . ' views </p>
                                    </div>
                                    <div class="col">
                                        <p> ' . $before_time . ' </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </a>
            </div>';
                    }
                }else{
                    print_r($response);
                }
                ?>
        </div>

    </section>




    <!-- content for Mobile -->

    <!-- content end  here -->
    <!-- footer start here -->
    <?php include("includes/footer.php"); ?>
    <script>
        $(document).ready(function(){
            $( $("#carouselExampleIndicators2 .carousel-item")[0] ).addClass("active");
            $( $("#carouselExampleIndicators3 .carousel-item")[0] ).addClass("active");
        })
    </script>
</body>

</html>