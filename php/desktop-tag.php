<?php
require("includes/db.php");
$keyword_array = [];
$get_all_keyword = $db->query("SELECT * FROM products");
while ($row = $get_all_keyword->fetch_assoc()) {

    $keyword = explode(",",trim(strtolower($row["keyword"])));
    for ($i = 0; $i < count($keyword); $i++) {
        array_push($keyword_array, str_replace("pdf", " ", strtolower($keyword[$i])));
    }  
}
// remove example keyword
if (($key = array_search("example1", $keyword_array)) !== false) {
    unset($keyword_array[$key]);
}
if (($key = array_search("example2", $keyword_array)) !== false) {
    unset($keyword_array[$key]);
}
if (($key = array_search("example3", $keyword_array)) !== false) {
    unset($keyword_array[$key]);
}
$keyword_array = array_count_values($keyword_array);
arsort($keyword_array);
// $keyword_array = array_unique($keyword_array,SORT_STRING);
$popular = array_slice(array_keys($keyword_array), 0, count($keyword_array), true);
// category list 
$list = ["engineering","bba","bds","book","class 10","class 11","class 12 ","mba"];
foreach($list as $cate){
    $arr = $popular;
    $results = array();
    foreach ($arr as $value) {
       if (strpos($value,$cate) !== false) { $results[] = $value; }
    }
    echo '
        <div class="carousel-item">
        <div class="row">
        ';
    $count = 0;
   
   foreach($results as $tags){        
$count++;
if($count<8){
    echo '
        <div class="col">
              <a href="'.$home_page.'/notes/'.just_clean($tags).'.html" style="text-decoration:none;color:black">   <div class="categories text-center">
                '.$tags.'
              </div>
              </a>
          </div> ';
} 
   }
    echo '
    </div>
    </div>
';

}
