<?php
require("../includes/db.php");
header('Content-Type: application/json');
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["for"])) {
        // function control
        if ($_POST["for"] == "get_reaction") {
            if (isset($_POST["notes_id"])) {
                // check user login or not 
                $user_id = check_login($user_db);
                if ($user_id !== null) {
                  $response=   get_reaction_for_user($db,$_POST["notes_id"],$user_id);
                } else {
                   $response =  get_reaction($db, $_POST["notes_id"]);
                }
                
            } else {
                $response = array("status" => "error", "messege" => "data missing");
            }
        }else if($_POST["for"]=="add_react"){
            if(isset($_POST["react"])&&isset($_POST["notes_id"])){
                $react = $_POST["react"];
                $notes_id = $_POST["notes_id"];
                $user_id = check_login($user_db);
                if($user_db!==null){
                    $response =  add_react($db,$react,$user_id,$notes_id);
                }

            }else{
                $response = array("status" => "error", "messege" => "react missing");
            }
        }
    } else {
        $response = array("status" => "error", "messege" => "reason missing");
    }
}
function check_login($user_db)
{
    if ($_COOKIE["url"] != "") {
        // get user id 
        $cookiekey = $_COOKIE["url"];
        $get_user_id = $user_db->query("SELECT userid FROM login_session WHERE cookie_key = '$cookiekey' ");
        if ($get_user_id->num_rows !== 0) {
            $userid = $get_user_id->fetch_assoc();
            $userid = $userid["userid"];
        } else {
            $userid = null;
        }
    } else {
        $userid = null;
    }
    return $userid;
};
function get_reaction($db, $notes_id)
{
    $total_like = 0;
    $total_dislike = 0;
    $likeCount = $db->query("SELECT COUNT(react) FROM reaction WHERE notes_id = '$notes_id' AND react = '1' ");
    if($likeCount->num_rows!==0){
        $likeCount = $likeCount->fetch_assoc();
        $likeCount = $likeCount["COUNT(react)"];
        $total_like = $likeCount;
    }
    $dislikeCount = $db->query("SELECT COUNT(react) FROM reaction WHERE notes_id = '$notes_id' AND react = 2 "); 
    if($dislikeCount->num_rows!==0){
        $dislikeCount = $dislikeCount->fetch_assoc();
        $dislikeCount = $dislikeCount["COUNT(react)"];
        $total_dislike = $dislikeCount;
    }
    $react = array("status"=>"success","like"=>$total_like,"dislike"=>$total_dislike);
    return $react;
}
function get_reaction_for_user($db,$notes_id,$user_id){
    $total_like = 0;
    $total_dislike = 0;
    $likeCount = $db->query("SELECT COUNT(react) FROM reaction WHERE notes_id = '$notes_id' AND react = '1' ");
    if($likeCount->num_rows!==0){
        $likeCount = $likeCount->fetch_assoc();
        $likeCount = $likeCount["COUNT(react)"];
        $total_like = $likeCount;
    }
    $dislikeCount = $db->query("SELECT COUNT(react) FROM reaction WHERE notes_id = '$notes_id' AND react = 2 "); 
    if($dislikeCount->num_rows!==0){
        $dislikeCount = $dislikeCount->fetch_assoc();
        $dislikeCount = $dislikeCount["COUNT(react)"];
        $total_dislike = $dislikeCount;
    }
    $check_react = $db->query("SELECT * FROM reaction WHERE notes_id = '$notes_id' AND  user_id = '$user_id'");
    $user_react = "0";
    if($check_react){
        if($check_react->num_rows!==0){
            $user_react  = $check_react->fetch_assoc();
            $user_react = $user_react["react"];
        }
    }
    $react = array("status"=>"success","like"=>$total_like,"dislike"=>$total_dislike,"react"=>$user_react);
    return $react;
}
function  add_react($db,$react,$user_id,$notes_id){
$today = date("d-m-Y h:i:sa");
$check = $db->query("SELECT * FROM reaction WHERE notes_id = '$notes_id' AND user_id = '$user_id'");
if($check->num_rows!==0){
    // update data 
    $update = $db->query("UPDATE reaction SET react = '$react' WHERE notes_id = '$notes_id'AND user_id = '$user_id'");
    if($update){
        $response = array("status"=>"success","messege "=>"reaction updated");
    }else{
        $response = array("status"=>"error","messege "=>"reaction not updated");
    }
}else{
    // insert data 
    $insert = $db->query("INSERT INTO reaction(notes_id,user_id,date_time,react) VALUES('$notes_id','$user_id','$today','$react')");
    if($insert){
        $response = array("status"=>"success","messege "=>"reaction added");
    }else{
        $response = array("status"=>"error","messege "=>"reaction not added");
    }
}
 return $response;
}
echo json_encode($response);